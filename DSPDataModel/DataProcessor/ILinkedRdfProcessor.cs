﻿using System;
using System.Collections.Generic;
using DspDataModel.Storages;

namespace DspDataModel.DataProcessor
{
    public interface ILinkedRdfProcessor
    {
        event EventHandler<bool> OnStateChanged;
        event EventHandler<IReadOnlyList<IRadioSource>> SignalsProcessed;

        void Initialize();

        void Put(IReadOnlyList<IRadioSource> signals, int stationId);
        void Stop();
    } 
}
