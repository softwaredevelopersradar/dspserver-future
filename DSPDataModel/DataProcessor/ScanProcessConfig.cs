﻿using System.Collections.Generic;
using System.Linq;
using Settings;

namespace DspDataModel.DataProcessor
{
    public struct ScanProcessConfig
    {
        public float Threshold { get; private set; }
        public int ScanAveragingCount { get; private set; }
        public int DirectionAveragingCount { get; private set; }
        public bool UseDirectionFinding { get; private set; }
        public bool CalculateHighQualityPhases { get; private set; }
        
        /// <summary>
        /// could be null, this means that all samples should be processed
        /// </summary>
        public IReadOnlyList<FrequencyRange> FrequencyRanges { get; private set; }

        private ScanProcessConfig(float threshold, int directionAveragingCount, int scanAveragingCount, bool useDirectionFinding, IEnumerable<FrequencyRange> frequencyRanges = null, bool calculateHighQualityPhases = false)
        {
            if (useDirectionFinding)
            {
                Contract.Assert(scanAveragingCount > 0);
                Contract.Assert(directionAveragingCount > 0);
            }

            FrequencyRanges = frequencyRanges?.ToArray();
            Threshold = threshold;
            ScanAveragingCount = scanAveragingCount;
            DirectionAveragingCount = directionAveragingCount;
            UseDirectionFinding = useDirectionFinding;
            CalculateHighQualityPhases = calculateHighQualityPhases;
        }

        /// <summary>
        /// Creates config that uses direction finding
        /// </summary>
        public static ScanProcessConfig CreateDfConfig(float threshold, int directionAveragingCount, int scanAveragingCount, IEnumerable<FrequencyRange> frequencyRanges = null, bool calculateHighQualityPhases = false)
        {
            return new ScanProcessConfig(threshold, directionAveragingCount, scanAveragingCount, true, frequencyRanges, calculateHighQualityPhases);
        }

        /// <summary>
        /// Creates config that doesn't use direction finding
        /// </summary>
        public static ScanProcessConfig CreateConfigWithoutDf(float threshold, IEnumerable<FrequencyRange> frequencyRanges = null, bool calculateHighQualityPhases = false)
        {
            return new ScanProcessConfig(threshold, 0, 0, false, frequencyRanges, calculateHighQualityPhases);
        }
    }
}