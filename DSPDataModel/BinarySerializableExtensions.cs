﻿using System.IO;
using System.IO.Compression;
using llcss;

namespace DspDataModel
{
    public static class BinarySerializableExtensions
    {
        public static void LoadFromFile<T>(this T self, string filename) where T : IBinarySerializable
        {
            using (var fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read))
            using (var compressor = new GZipStream(fileStream, CompressionMode.Decompress))
            using (var resultStream = new MemoryStream())
            {
                compressor.CopyTo(resultStream);
                self.Decode(resultStream.ToArray(), 0);
            }
        }

        public static void SaveToFile<T>(this T self, string filename) where T : IBinarySerializable
        {
            using (var fileStream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.Write))
            using (var compressor = new GZipStream(fileStream, CompressionLevel.Optimal))
            {
                var data = self.GetBytes();
                compressor.Write(data, 0, data.Length);
            }
        }
    }
}
