﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.Data;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace DspDataModel.LinkedStation
{
    public interface ILinkedStationBase
    {
        int OwnAddress { get; }
        int LinkedAddress { get; }

        bool IsWorking { get; }
        bool Connected { get; }
        ConnectionTypes ConnectionType { get; }

        bool SendText(string text);
        bool SendSyncTime(DateTime time);
        bool SendCoordinates(double latitude, double longitude, int altitude, TargetStation station);
        Task<float?> PerformExecutiveDf(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount);

        /// <summary>
        /// this method is for RdfMode only, do not use it!
        /// </summary>
        /// <returns></returns>
        Task<IReadOnlyCollection<ISignal>> WaitForSignals();

        event EventHandler<string> TextReceivedEvent;
        event EventHandler<(IPosition, TargetStation)> CoordinatesReceivedEvent;
        event EventHandler DisconnectEvent;
        event EventHandler ConnectEvent;
        event EventHandler<DspServerMode> SetModeEvent;
        event EventHandler<(RangeType, IReadOnlyCollection<Filter>)> SetRangeSectorsEvent;
        event EventHandler<IReadOnlyCollection<ISignal>> SignalsReceivedEvent;
        event EventHandler<(FrequencyType, IReadOnlyCollection<FrequencyRange>)> SetSpecialRangesEvent;
        event EventHandler<IReadOnlyCollection<IRadioJamTarget>> SetFrsJammingTargetsEvent;
        event EventHandler<IReadOnlyCollection<IRadioJamFhssTarget>> SetFhssJammingTargetsEvent;
        event EventHandler RequestFrsTargetsEvent;
        event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;
        event EventHandler<DateTime> SetSyncTimeEvent;

        bool SendSignals(IEnumerable<ISignal> signals);
        void Stop();
    }
}