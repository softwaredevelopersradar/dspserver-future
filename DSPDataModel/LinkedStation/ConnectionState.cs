﻿namespace DspDataModel.LinkedStation
{
    public enum ConnectionState
    {
        NoConnection = 0,
        Hosted = 1,
        Connected = 2
    }

    public enum ConnectionTypes
    {
        NoConnection = 0,
        TcpIp = 1,
        Rs232 = 2
    }
}