using System;
using System.Collections.Generic;
using DspDataModel.Data;
using DspDataModel.Storages;
using Settings;

namespace DspDataModel
{
    public class FilterManagerMock : IFilterManager
    {
        public IReadOnlyList<Band> Bands { get; }
        public IReadOnlyList<Filter> Filters { get; }
        public IReadOnlyList<FrequencyRange> ForbiddenFrequencies { get; }
        public IReadOnlyList<FrequencyRange> ImportantFrequencies { get; }
        public IReadOnlyList<FrequencyRange> KnownFrequencies { get; }
        public IReadOnlyList<float> NoiseLevels { get; }
        public IReadOnlyList<float> Thresholds { get; }
        public IReadOnlyList<int> CalibrationBands { get; }
        public IReadOnlyList<Filter> IntelligenceFilters { get; }
        public IReadOnlyList<Filter> RadioJamFilters { get; }
        public RangeType WorkingRangeType { get; set; }
        public event EventHandler<IReadOnlyList<Band>> BandsUpdatedEvent;
        public event EventHandler<IReadOnlyList<Filter>> FiltersUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenFrequenciesUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ImportantFrequenciesUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> KnownFrequenciesUpdatedEvent;
        public event EventHandler<IReadOnlyList<float>> NoiseLevelUpdatedEvent;
        public event EventHandler<IReadOnlyList<float>> ThresholdsUpdatedEvent;

        public float GetAdaptiveThreshold(int bandNumber) => Constants.DefaultThresholdValue;

        public RadioSourceType GetRadioSourceType(float frequencyKhz) => RadioSourceType.Normal;
        public RadioSourceType GetRadioSourceType(ISignal signal) => RadioSourceType.Normal;

        public float GetThreshold(int bandNumber) => Constants.DefaultThresholdValue;

        public IReadOnlyList<Filter> GetFilters(RangeType rangeType)
        {
            return Filters;
        }

        public bool IsInFilter(float frequencyKhz, float direction) => true;
        public bool IsInFilter(ISignal signal) => true;
        public bool IsInFilter(IFhssNetwork network) => true;
        public bool SetFilters(RangeType rangeType, IEnumerable<Filter> filters)
        {
            return true;
        }

        public bool SetForbiddenFrequencies(IEnumerable<FrequencyRange> forbiddenFrequencies) => true;

        public void SetGlobalThreshold(float value)
        { }

        public bool SetImportantFrequencies(IEnumerable<FrequencyRange> importantFrequencies) => true;

        public bool SetKnownFrequencies(IEnumerable<FrequencyRange> knownFrequencies) => true;

        public void SetThreshold(int bandNumber, float value)
        { }

        public void UpdateNoiseLevel(IAmplitudeScan scan)
        { }

        public void UpdateNoiseLevel(int bandNumber, float noiseLevel)
        { }

        public void ClearCalibrationBands()
        { }

        public void SetCalibrationBands(List<int> bands)
        { }
    }
}