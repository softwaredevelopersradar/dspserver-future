using System.Collections.Generic;
using DspDataModel.Database;

namespace DspDataModel.Storages
{
    public interface IFhssNetworkStorage
    {
        IDatabaseController DatabaseController { get; }

        void Put(IEnumerable<IFhssNetwork> networks);
        void Clear();
        IEnumerable<IFhssNetwork> GetFhssNetworks();

        IReadOnlyCollection<IFhssNetwork> HidedFhssNetworks { get; }
        void PerformAction(SignalAction action, int[] signalsId);
    }
}