﻿using DspDataModel.Data;
using Settings;

namespace DspDataModel.Storages
{
    public interface ISpectrumStorage
    {
        void Put(IAmplitudeScan scan);
        IAmplitudeBand GetSpectrum(int bandNumber);
        IAmplitudeBand GetSpectrum(float startFrequencyKhz, float endFrequencyKhz, int pointCount);
    }

    public static class SpectrumStorageExtensions
    {
        public static IAmplitudeBand GetSpectrum(this ISpectrumStorage storage, float startFrequencyKhz, float endFrequencyKhz)
        {
            return storage.GetSpectrum(startFrequencyKhz, endFrequencyKhz, Utilities.GetSamplesCount(startFrequencyKhz, endFrequencyKhz));
        }
    }
}
