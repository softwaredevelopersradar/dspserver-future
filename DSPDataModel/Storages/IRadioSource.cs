﻿using System;
using System.Collections.Generic;

namespace DspDataModel.Storages
{
    public enum RadioSourceType
    {
        Normal, Important, Forbidden, Known
    }

    public interface IRadioSource : ISignal
    {
        int Id { get; }
        bool IsNew { get; }
        bool IsActive { get; }
        DateTime BroadcastStartTime { get; }
        DateTime FirstBroadcastStartTime { get; }
        RadioSourceType SourceType { get; }
        
        /// <summary>
        /// Direction and reliability from linked stations if there are any.
        /// Key is station Id.
        /// </summary>
        Dictionary<int, (float direction, float reliability)> LinkedInfo { get; }
        void UpdateLinkedStationDirection(int stationId, float direction, float reliability);

        float GetIntersectionBandwidth();

        double Latitude { get; }
        double Longitude { get; }
        double Altitude { get; }

        bool IsSameSource(ISignal signal);
        bool IsSameLinkedStationSource(ISignal signal);
        void Update(ISignal signal);
        void Update();
        void UpdateLocation(double latitude, double longitude, double altitude);
    }
}
