﻿using System;
using System.Collections.Generic;

namespace DspDataModel
{
    public interface IFhssNetwork
    {
        float MinFrequencyKhz { get; }
        float MaxFrequencyKhz { get; }
        float BandwidthKhz { get; }
        float StepKhz { get; }
        int FrequenciesCount { get; }
        float ImpulseDurationMs { get; }
        int Id { get; }
        bool IsActive { get; }
        bool IsDurationMeasured { get; }
        DateTime LastUpdateTime { get; }
        IReadOnlyList<FhssUserInfo> UserInfo { get; }
        /// <summary>
        /// Linked stations user info, if there are any
        /// Key is station Id
        /// </summary>
        IReadOnlyDictionary<int, LinkedFhssUserInfo> LinkedUserInfo { get; }
        IReadOnlyList<FixedRadioSource> FixedRadioSources { get; }

        void Update(IFhssNetwork network);
        void RemeasureDuration();
    }

    //todo : to separate files
    /// <summary>
    /// structure holds data for Fhss module working fixed radio source
    /// </summary>
    public struct FixedRadioSource
    {
        public float FrequencyKhz { get; }
        public float Amplitude { get; }
        public float Direction { get; }
        public float Bandwidth { get; }

        public FixedRadioSource(float frequencyKhz, float amplitude, float direction, float bandwidth) : this()
        {
            FrequencyKhz = frequencyKhz;
            Amplitude = amplitude;
            Direction = direction;
            Bandwidth = bandwidth;
        }
    }

    /// <summary>
    /// Information about possible user of fhss network
    /// </summary>
    public struct FhssUserInfo
    {
        public float Amplitude { get; }
        public float Direction { get; }
        public float StandardDeviation { get; }

        public float Altitude { get; }
        public float Latitude { get; }
        public float Longitude { get; }

        public FhssUserInfo(float amplitude, float direction, float standardDeviation)
        {
            Amplitude = amplitude;
            Direction = direction;
            StandardDeviation = standardDeviation;
            Altitude = -1;
            Latitude = -1;
            Longitude = -1;
        }

        public FhssUserInfo(float amplitude, float direction, float standardDeviation, float altitude, float latitude, float longitude)
        {
            Amplitude = amplitude;
            Direction = direction;
            StandardDeviation = standardDeviation;
            Altitude = altitude;
            Latitude = latitude;
            Longitude = longitude;
        }
    }


    /// <summary>
    /// Information about possible user of fhss network from linked station
    /// </summary>
    public struct LinkedFhssUserInfo
    {
        public float Amplitude { get; }
        public float Direction { get; }
        public float StandardDeviation { get; }

        public LinkedFhssUserInfo(float amplitude, float direction, float standardDeviation)
        {
            Amplitude = amplitude;
            Direction = direction;
            StandardDeviation = standardDeviation;
        }
    }
}
