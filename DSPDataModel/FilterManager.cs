﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel.Data;
using DspDataModel.Storages;
using Settings;

namespace DspDataModel
{
    public sealed class FilterManager : IFilterManager
    {
        private Filter[] _radioJamFilters;
        private Filter[] _intelligenceFilters;

        public IReadOnlyList<Filter> IntelligenceFilters => _intelligenceFilters;
        public IReadOnlyList<Filter> RadioJamFilters => _radioJamFilters;
        public RangeType WorkingRangeType { get; set; }

        public IReadOnlyList<Filter> Filters
        {
            get
            {
                switch (WorkingRangeType)
                {
                    case RangeType.Intelligence:
                        return IntelligenceFilters;
                    case RangeType.RadioJamming:
                        return RadioJamFilters;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public event EventHandler<IReadOnlyList<float>> ThresholdsUpdatedEvent;
        public event EventHandler<IReadOnlyList<float>> NoiseLevelUpdatedEvent;
        public event EventHandler<IReadOnlyList<Filter>> FiltersUpdatedEvent;
        
        public event EventHandler<IReadOnlyList<Band>> BandsUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ImportantFrequenciesUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> KnownFrequenciesUpdatedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenFrequenciesUpdatedEvent;

        private readonly float[] _thresholds;
        public IReadOnlyList<float> Thresholds => _thresholds;

        private readonly float[] _noiseLevels;
        public IReadOnlyList<float> NoiseLevels => _noiseLevels;

        private Band[] _radioJamBands;
        private Band[] _intelligenceBands;

        public IReadOnlyList<Band> Bands
        {
            get
            {
                switch (WorkingRangeType)
                {
                    case RangeType.Intelligence:
                        return _intelligenceBands;
                    case RangeType.RadioJamming:
                        return _radioJamBands;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        private FrequencyRange[] _knownFrequencies;
        public IReadOnlyList<FrequencyRange> KnownFrequencies => _knownFrequencies;

        private FrequencyRange[] _importantFrequencies;
        public IReadOnlyList<FrequencyRange> ImportantFrequencies => _importantFrequencies;

        private FrequencyRange[] _forbiddenFrequencies;
        public IReadOnlyList<FrequencyRange> ForbiddenFrequencies => _forbiddenFrequencies;

        private List<int> _calibrationBands;
        public IReadOnlyList<int> CalibrationBands => _calibrationBands;

        private readonly object _setFiltersLockObject = new object();

        public FilterManager()
        {
            _radioJamFilters = new Filter[0];
            _intelligenceFilters = new Filter[0];
            _thresholds = Enumerable.Repeat(Constants.DefaultThresholdValue, Config.Instance.BandSettings.BandCount).ToArray();
            _noiseLevels = Enumerable.Repeat(Constants.ReceiverMinAmplitude, Config.Instance.BandSettings.BandCount).ToArray();

            _radioJamBands = new Band[0];
            _intelligenceBands = new Band[0];

            _knownFrequencies = new FrequencyRange[0];
            _importantFrequencies = new FrequencyRange[0];
            _forbiddenFrequencies = new FrequencyRange[0];
            WorkingRangeType = RangeType.Intelligence;

            _calibrationBands = new List<int>();
        }

        public bool SetImportantFrequencies(IEnumerable<FrequencyRange> importantFrequencies)
        {
            var ranges = importantFrequencies.OrderBy(r => r.StartFrequencyKhz).ToArray();
            if (!AreListsEqual(ranges, _importantFrequencies))
            {
                _importantFrequencies = ranges;
                ImportantFrequenciesUpdatedEvent?.Invoke(this, ImportantFrequencies);
                return true;
            }
            return false;
        }

        public bool SetKnownFrequencies(IEnumerable<FrequencyRange> knownFrequencies)
        {
            var ranges = knownFrequencies.OrderBy(r => r.StartFrequencyKhz).ToArray();
            if (!AreListsEqual(ranges, _knownFrequencies))
            {
                _knownFrequencies = ranges;
                KnownFrequenciesUpdatedEvent?.Invoke(this, KnownFrequencies);
                return true;
            }
            return false;
        }

        public bool SetForbiddenFrequencies(IEnumerable<FrequencyRange> forbiddenFrequencies)
        {
            var ranges = forbiddenFrequencies.OrderBy(r => r.StartFrequencyKhz).ToArray();
            if (!AreListsEqual(ranges, _forbiddenFrequencies))
            {
                _forbiddenFrequencies = ranges;
                ForbiddenFrequenciesUpdatedEvent?.Invoke(this, ForbiddenFrequencies);
                return true;
            }
            return false;
        }

        public float GetAdaptiveThreshold(int bandNumber)
        {
            return NoiseLevels[bandNumber] + Config.Instance.DirectionFindingSettings.AdditionalAdaptiveLevel;
        }

        public void SetGlobalThreshold(float value)
        {
            for (var i = 0; i < _thresholds.Length; i++)
            {
                _thresholds[i] = value;
            }
            ThresholdsUpdatedEvent?.Invoke(this, _thresholds);
        }

        public void SetThreshold(int bandNumber, float value)
        {
            _thresholds[bandNumber] = value;
            ThresholdsUpdatedEvent?.Invoke(this, _thresholds);
        }

        public void UpdateNoiseLevel(int bandNumber, float noiseLevel)
        {
            _noiseLevels[bandNumber] = noiseLevel;
            NoiseLevelUpdatedEvent?.Invoke(this, _noiseLevels);
        }

        public void UpdateNoiseLevel(IAmplitudeScan scan)
        {
            UpdateNoiseLevel(scan.BandNumber, scan.Amplitudes.GetNoiseLevel());
        }

        public float GetThreshold(int bandNumber)
        {
            return Math.Max(_thresholds[bandNumber], GetAdaptiveThreshold(bandNumber));
        }

        public IReadOnlyList<Filter> GetFilters(RangeType rangeType)
        {
            switch (rangeType)
            {
                case RangeType.Intelligence:
                    return IntelligenceFilters;
                case RangeType.RadioJamming:
                    return RadioJamFilters;
                default:
                    throw new ArgumentOutOfRangeException(nameof(rangeType), rangeType, null);
            }
        }

        public bool SetFilters(RangeType rangeType, IEnumerable<Filter> filters)
        {
            switch (rangeType)
            {
                case RangeType.Intelligence:
                    return SetFilters(filters, ref _intelligenceFilters, ref _intelligenceBands);
                case RangeType.RadioJamming:
                    return SetFilters(filters, ref _radioJamFilters, ref _radioJamBands);
                default:
                    throw new ArgumentOutOfRangeException(nameof(rangeType), rangeType, null);
            }
        }

        private bool SetFilters(IEnumerable<Filter> filters, ref Filter[] currentFilters, ref Band[] currentBands)
        {
            lock (_setFiltersLockObject)
            {
                var newFilters = filters.OrderBy(f => f.MinFrequencyKhz).ToArray();

                if (AreListsEqual(currentFilters, newFilters))
                {
                    return false;
                }
                currentFilters = newFilters;
                currentBands = SplitFiltersInBands(currentFilters);

                FiltersUpdatedEvent?.Invoke(this, Filters);
                BandsUpdatedEvent?.Invoke(this, Bands);

                return true;
            }
        }

        // hope lists are sorted in same way
        private static bool AreListsEqual<T>(IReadOnlyList<T> list1, IReadOnlyList<T> list2) where T : IEquatable<T>
        {
            if (list1.Count != list2.Count)
            {
                return false;
            }
            for (var i = 0; i < list1.Count; ++i)
            {
                if (!list1[i].Equals(list2[i]))
                {
                    return false;
                }
            }
            return true;
        }

        public RadioSourceType GetRadioSourceType(ISignal signal)
        {
            return GetRadioSourceType(signal.FrequencyKhz);
        }

        public RadioSourceType GetRadioSourceType(float frequencyKhz)
        {
            if (ImportantFrequencies.Any(range => range.Contains(frequencyKhz)))
            {
                return RadioSourceType.Important;
            }
            if (ForbiddenFrequencies.Any(range => range.Contains(frequencyKhz)))
            {
                return RadioSourceType.Forbidden;
            }
            if (KnownFrequencies.Any(range => range.Contains(frequencyKhz)))
            {
                return RadioSourceType.Known;
            }
            return RadioSourceType.Normal;
        }

        public bool IsInFilter(ISignal signal) => IsInFilter(signal.FrequencyKhz, signal.Direction);

        public bool IsInFilter(float frequencyKhz, float direction) => Filters.Any(f => f.IsInFilter(frequencyKhz, direction));

        public bool IsInFilter(IFhssNetwork network) =>
            Filters.Any(f => f.IsInFilter(network.MinFrequencyKhz) || f.IsInFilter(network.MaxFrequencyKhz));

        private Band[] SplitFiltersInBands(Filter[] filters)
        {
            return filters
                .SelectMany(f => f.SplitInBands())
                .Distinct()
                .ToArray();

        }

        public void ClearCalibrationBands() => _calibrationBands.Clear();

        public void SetCalibrationBands(List<int> bands)
        {
            _calibrationBands = bands;
        }
    }
}
