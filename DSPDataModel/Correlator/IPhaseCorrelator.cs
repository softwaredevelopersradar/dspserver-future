﻿using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Correlator
{
    public interface IPhaseCorrelator
    {
        float AmplitudeRelativeThreshold { get; }
        float CorrelationThreshold { get; }
        float StandardDeviationWeigth { get; }
        float CorrelationWeigth { get; }

        void ReloadRadioPathTable(string filename);
        void SaveTheoreticalTable(string filename);

        DirectionInfo CalculateDirection(float threshold, IDataScan scan, int startIndex, int endIndex);
        DirectionInfo CalculateDirection(float threshold, IDataScan scan, int index);

        float[] GetCorrelationCurve(float frequencyKhz, IReadOnlyList<float> phases);
        float[] GetCorrelationCurve(ISignal signal);
    }
}
