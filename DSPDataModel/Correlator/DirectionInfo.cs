﻿namespace DspDataModel.Correlator
{
    public struct DirectionInfo
    {
        public float Angle { get; set; }
        public float StandardDeviation { get; set; }
        public float Correlation { get; set; }
        public float DiscardedPhasesPart { get; }
        public float PhasesDeviation { get; }

        public float[] Phases { get; set; }

        public DirectionInfo(float angle, float standardDeviation, float discardedPhasesPart, float correlation, float[] phases, float phasesDeviation) : this()
        {
            Angle = angle;
            StandardDeviation = standardDeviation;
            DiscardedPhasesPart = discardedPhasesPart;
            Correlation = correlation;
            Phases = phases;
            PhasesDeviation = phasesDeviation;
        }
    }
}
