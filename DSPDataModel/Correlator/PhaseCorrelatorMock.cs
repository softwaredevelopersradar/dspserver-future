﻿using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Correlator
{
    public class PhaseCorrelatorMock : IPhaseCorrelator
    {
        public float AmplitudeRelativeThreshold => 0;
        public float CorrelationThreshold => 0;
        public float StandardDeviationWeigth => 0.5f;
        public float CorrelationWeigth => 0.5f;

        public void ReloadRadioPathTable(string filename)
        { }

        public void SetPreciseCalibrationPhases(float frequencyKhz, int direction, float[] phases)
        {
        }

        public DirectionInfo CalculateDirection(float threshold, IDataScan scan, int startIndex, int endIndex)
        {
            return new DirectionInfo(0, 0, 0, 0, new float[360], 0);
        }

        public DirectionInfo CalculateDirection(float threshold, IDataScan scan, int index)
        {
            return new DirectionInfo(0, 0, 0, 0, new float[360], 0);
        }

        public float[] GetCorrelationCurve(float frequencyKhz, IReadOnlyList<float> phases)
        {
            return new float[360];
        }

        public float[] GetCorrelationCurve(ISignal signal)
        {
            return new float[360];
        }

        public void SaveTheoreticalTable(string filename)
        { }
    }
}