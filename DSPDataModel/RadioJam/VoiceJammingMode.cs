﻿namespace DspDataModel.RadioJam
{
    public enum VoiceJammingMode
    {
        FromFile = 0,
        RealtimeCapture = 1
    }
}
