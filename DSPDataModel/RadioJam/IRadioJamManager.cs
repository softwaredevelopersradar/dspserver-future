﻿using System.Threading.Tasks;
using DspDataModel.Database;

namespace DspDataModel.RadioJam
{
    public enum RadioJamMode
    {
        Frs, // ФРЧ
        Afrs, // АПРЧ
        FrsAuto, // ФРЧ Авто
        Fhss,
        Voice // голосовая помеха
    }

    public interface IRadioJamManager : IRadioJamTargetConfig
    {
        IRadioJamTargetStorage Storage { get; }
        IRadioJamTargetStorage LinkedStationStorage { get; }
        IDatabaseController DatabaseController { get; }
        IRadioJamShaper Shaper { get; }
        RadioJamMode JamMode { get; }
        FftResolution FhssFftResoultion { get; set; }
        bool IsJammingActive { get; }
        int EmitDuration { get; set; }

        /// <summary>
        /// For Afrs and Frs Auto modes only
        /// </summary>
        int Threshold { get; set; }

        /// <summary>
        /// For Afrs mode only
        /// </summary>
        int DirectionSearchSector { get; set; }

        /// <summary>
        /// For Afrs mode only
        /// </summary>
        int FrequencySearchBandwidthKhz { get; set; }

        /// <summary>
        /// For Frs auto mode only
        /// </summary>
        float MinSignalBandwidthKhz { get; set; }

        /// <summary>
        /// For voice jamming only
        /// </summary>
        VoiceJammingMode VoiceMode { get; set; }

        /// <summary>
        /// For voice jamming only
        /// </summary>
        string VoiceJamFilePath { get; set; }

        /// <summary>
        /// For voice jamming only
        /// </summary>
        int VoiceJamFrequencyKhz { get; set; }

        /// <summary>
        /// For voice jamming only
        /// </summary>
        byte VoiceJamDeviationCode { get; set; }

        /// jam duration in microseconds (10e-6)
        int FhssJamDuration { get; set; }

        /// <summary>
        /// To stop jamming after this amount of time
        /// </summary>
        int JammingAutoStopIntervalSec { get; set; }

        int ChannelsInLiter { get; set; }
        int CheckEmitDelayMs { get; }
        bool Connect();
        void Disconnect();
        IRadioJamTarget CreateRadioJamTarget(ISignal signal);
        void Start(RadioJamMode mode);
        Task Stop();
        Task StopWithoutStoppingJamming();

        void ChangeVoiceJamState(bool newJamState);

        int GetMinPowerLevel(int literNumber);
    }
}
