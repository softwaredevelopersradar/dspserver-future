namespace DspDataModel.RadioJam
{
    public interface IRadioJamTargetConfig
    {
        int LongWorkingSignalDurationMs { get; set; }
    }

    public class RadioJamTargetConfig : IRadioJamTargetConfig
    {
        public int LongWorkingSignalDurationMs { get; set; }

        public RadioJamTargetConfig(int longWorkingSignalDurationMs)
        {
            LongWorkingSignalDurationMs = longWorkingSignalDurationMs;
        }
    }
}