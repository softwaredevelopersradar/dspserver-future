﻿using System;
using System.Collections.Generic;

namespace DspDataModel.RadioJam
{
    public interface IRadioJamShaper
    {
        bool IsConnected { get; }

        event EventHandler<bool> ConnectionStateChangedEvent;
        event EventHandler<byte[]> ShaperConditionReceivedEvent;

        bool Connect(string host, int port, string clientHost, int clientPort);
        void Disconnect();

        // used for jamming : 
        ShaperResponse StartFrsJamming(TimeSpan duration, IEnumerable<IRadioJamTarget> targets);
        ShaperResponse StartFhssJamming(uint fftCountCode, int duration, IEnumerable<IRadioJamFhssTarget> targets);

        void StartVoiceJamming(bool emitionEnabled, int frequencyKhz, byte deviation);
        bool CheckIsVoiceJamAvailable(VoiceJammingMode jamMode, string filePath);

        ShaperResponse StopJamming();

        // used for radiojam shaper state :
        ShaperResponse<int[]> GetLiterPowers();
        ShaperResponse<int[]> GetFirstNineLiterPowers();
        bool SetLiterPower(byte liter, byte power);
        bool SetLiterTypeLoad(byte liter, byte typeLoad);
        bool GetLiterStates();
        bool GetLiterVoltages();
        bool GetLiterCurrents();
        bool GetLiterTemperatures();
    }
}
