namespace DspDataModel.RadioJam
{
    public struct ShaperResponse<T>
    {
        public bool IsOk => ErrorCode == 0;
        public T Result { get; }
        public int ErrorCode { get; }

        public ShaperResponse(T result, int errorCode)
        {
            Result = result;
            ErrorCode = errorCode;
        }
    }

    public struct ShaperResponse
    {
        public bool IsOk => ErrorCode == 0;
        public int ErrorCode { get; }

        public ShaperResponse(int errorCode)
        {
            ErrorCode = errorCode;
        }
    }
}