﻿namespace DspDataModel.RadioJam
{
    public enum ShaperCommandCodes : byte
    {
        Power = 0,
        State,
        Voltage,
        Current,
        Temperature
    }
}
