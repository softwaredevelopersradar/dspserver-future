﻿using System.Threading;
using System.Threading.Tasks;

namespace DspDataModel.RadioJam
{
    public interface IRadioJamStrategy
    {
        Task JammingTask(CancellationToken token);

        bool StopJammingAfterTaskEnd { get; set; }
    }
}
