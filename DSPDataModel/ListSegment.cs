﻿using System.Collections;
using System.Collections.Generic;

namespace DspDataModel
{
    public struct ListSegment<T> : IReadOnlyList<T>
    {
        private readonly IReadOnlyList<T> _data;

        public readonly int Offset;

        public int Count { get; }

        public ListSegment(IReadOnlyList<T> data, int offset, int count)
        {
            _data = data;
            Offset = offset;
            Count = count;
        }

        public T this[int index] => _data[Offset + index];

        public IEnumerator<T> GetEnumerator()
        {
            var to = Offset + Count;
            for (var i = Offset; i < to; ++i)
            {
                yield return _data[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
