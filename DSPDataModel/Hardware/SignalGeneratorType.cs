namespace DspDataModel.Hardware
{
    public enum SignalGeneratorType
    {
        Keysight,
        SG6000L,
        Simulation
    }
}