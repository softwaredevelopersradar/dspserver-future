﻿using System;

namespace DspDataModel.Hardware
{
    public static class UintBitExtensions
    {
        public static uint SetBit(this uint self, int index, int value)
        {
            if (index >= 32)
            {
                MessageLogger.Error($"{index} is more than 31, which is max index to set");
                return self;
            }

            if (value == 0)
            {
                return self & Get0Value(index);
            }

            if (value == 1)
            {
                return self | Get1Value(index);
            }

            return self;
        }

        /// <summary>
        /// Returns a value that is used to change bitIndex bit to 0 with & operator
        /// </summary>
        private static uint Get0Value(int bitIndex)
        {
            return 0xFFFF_FFFF - Get1Value(bitIndex);
        }

        /// <summary>
        /// Returns a value that is used to change bitIndex bit to 1 with | operator
        /// </summary>
        private static uint Get1Value(int bitIndex) => (uint)Math.Pow(2, bitIndex);
    }
}
