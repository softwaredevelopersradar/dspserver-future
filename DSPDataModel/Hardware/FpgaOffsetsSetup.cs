﻿namespace DspDataModel.Hardware
{
    public class FpgaOffsetsSetup
    {
        public byte CmdTblBar;
        public byte AxiCdmaBar;
        public byte AxiPcie0Bar;
        public byte AxiPcie1Bar;
        public byte AxiPcieBar;
        public byte Adc1Bar;
        public byte Adc2Bar;
        public uint CmdTblOff;
        public uint AxiCdmaOff;
        public uint AxiPcie0Off;
        public uint AxiPcie1Off;
        public uint AxiPcieOff;
        public uint Adc1Base;
        public uint Adc2Base;
        public uint Adc1Off;
        public uint Adc2Off;
        public uint AxiBar0D0Off;
        public uint AxiBar0D1Off;
        public uint AxiBar0D0Size;
        public uint AxiBar0D1Size;
        public uint AxiBarOff;
        public uint AxiBarSize;

        public FpgaOffsetsSetup()
        {
            CmdTblBar = AxiCdmaBar = AxiPcie0Bar = AxiPcie1Bar = AxiPcieBar = Adc1Bar = Adc2Bar = 0xFF;
            CmdTblOff = AxiCdmaOff = AxiPcie0Off = AxiPcie1Off = AxiPcieOff = Adc1Base = Adc2Base = Adc1Off = Adc2Off =
                AxiBar0D0Off = AxiBar0D1Off = AxiBar0D0Size = AxiBar0D1Size = AxiBarOff = AxiBarSize = 0xFFFFFFFF;
        }
    }
}