﻿using System.Threading.Tasks;

namespace DspDataModel.Hardware
{
    public interface ISignalGenerator
    {
        float FrequencyKhz { get; set; }
        float Level { get; set; }
        bool EmitionEnabled { get; set; }
        bool IsInitialized { get; }

        Task<bool> Initialize(string hostname);
        void Close();
        void Reset();
    }
}
