﻿using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public interface IFpgaDeviceManager
    {
        IReadOnlyList<IFpgaDevice> Devices { get; }
        FpgaMode Mode { get; }
        bool GetBlankScan { get; set; }
        double GetScanTimeMs { get; }

        bool Initialize();
        IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers);
        ITransmissionChannel GetFpgaTransmissionChannel();
        FpgaOffsetsSetup OffsetsSetup { get; }
        FpgaDeviceBandsSettingsSource BandsSettingsSource { get; }
        
        bool StartDevices();
        bool WaitData();
        bool StartFhssMode(FhssSetup setup);
        bool StartFhssDurationMeasurement(FhssSetup setup);
        bool StopFhssMode();
        void Close();
        int GetDriverVersion();
    }

    public enum FpgaMode
    {
        Intelligence, Fhss, FhssDurationMeasurement
    }
}
