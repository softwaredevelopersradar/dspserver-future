﻿using System.Collections.Generic;
using Settings;

namespace DspDataModel.Hardware
{
    public class FpgaDeviceBandsSettingsSource
    {
        private IReadOnlyList<FpgaDeviceBandSettings> _settings;

        public IReadOnlyList<FpgaDeviceBandSettings> Settings
        {
            get { return _settings; }
            set { _settings = value; }
        }

        public FpgaDeviceBandsSettingsSource()
        {
            var settings = new FpgaDeviceBandSettings[Config.Instance.BandSettings.BandCount];
            for (var i = 0; i < settings.Length; ++i)
            {
                settings[i] = new FpgaDeviceBandSettings(0, 0, false, false);
            }
            Settings = settings;
        }
    }
}