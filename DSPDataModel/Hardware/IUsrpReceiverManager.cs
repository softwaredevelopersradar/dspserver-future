﻿using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public interface IUsrpReceiverManager
    {
        bool IsConnected { get; }
        float ScanSpeed { get; }

        short FrequencyMhz { get; set; }
        int BandNumber { get; set; }
        byte Amplification { get; set; }

        bool Initialize();
        IAmplitudeScan GetScan();
    }
}