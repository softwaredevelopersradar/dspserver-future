﻿using System;
using System.Collections.Generic;

namespace DspDataModel.Hardware
{
    // interface for different data transmissions (TCP/IP, RS-232, PCI-E...)
    public interface ITransmissionChannel
    {
        bool IsConnected { get; }

        /// <summary>
        /// Connects to remote point using args (for example args are Ip, port when using tcp/ip)
        /// </summary>
        /// <returns>true if connect is successful, false otherwise</returns>
        bool Connect(params object[] args);
        
        void Disconnect();
        
        void Write(byte[] buffer, int offset, int count);
        
        int Read(byte[] buffer, int offset, int count);
        
        event EventHandler<IReadOnlyList<byte>> OnDataRead;

        /// <summary>
        /// Timeout in milliseconds
        /// </summary>
        int Timeout { get; set; }
    }
}
