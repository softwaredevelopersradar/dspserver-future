﻿namespace DspDataModel.Hardware
{
    public static class HardwareExtensions
    {
        public static void Write(this ITransmissionChannel channel, byte[] buffer)
        {
            channel.Write(buffer, 0, buffer.Length);
        }

        public static int Read(this ITransmissionChannel channel, byte[] buffer)
        {
            return channel.Read(buffer, 0, buffer.Length);
        }
    }
}
