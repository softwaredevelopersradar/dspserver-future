﻿using System;
using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public interface IFpgaDevice
    {
        IntPtr DeviceHandler { get; }
        int ChannelsCount { get; }
        bool IsOpened { get; }

        bool IsFhssModeRunning { get; }
        bool StartFhssMode(FhssSetup setup);
        bool StartFhssDurationMeasurement(FhssSetup setup);
        bool StopFhssMode();

        bool Initialize();
        void Open();
        void Close();
        bool InitFpga(ChannelMode channelMode, FftResolution fftSize);
        bool InitFpgaTacting(TactingMode mode);
        uint GetAdcFlag();
        bool StartAdc(uint adcFlag);
        bool Read(byte[] buf);
        byte[] Read();
        bool WaitData();
        int GetFhssPeakIndex(int channelIndex);
        int GetScanIndex();
        int GetDriverVersion();
        bool ReadData(IFpgaDataScan scan, IReadOnlyList<int> bandNumbers);
        void DropFhssMeasurementFlag();

        /// <summary>
        /// should be called only once per device worktime session
        /// </summary>
        bool InitializeFpgaNet();

        void WriteWord(byte bar, uint offset, uint data);
        uint ReadWord(byte bar, uint offset);
    }
}
