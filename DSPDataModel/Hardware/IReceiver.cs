﻿namespace DspDataModel.Hardware
{
    public interface IReceiver
    {
        int Frequency { get; }
        int BandNumber { get; set; }
        void SetBandNumber(int bandNumber);
        void SetFrequency(int frequencyMhz);
        int Number { get; }
        ITransmissionChannel DataChannel { get; }
    }
}
