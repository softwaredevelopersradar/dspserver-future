﻿using System.Collections.Generic;
using DspDataModel.Data;
using DspDataModel.RadioJam;

namespace DspDataModel.Server
{
    public class SectorAndRangesChangedEventArgs : IServerCommandResult
    {
        public RangeType RangesType { get; }
        public TargetStation Station { get; }
        public IReadOnlyCollection<Filter> NewFilters { get; }
        public RequestResult Result { get; }
        public int? ClientId { get; }
        public bool SendBroadcastEvent { get; }

        public SectorAndRangesChangedEventArgs(RangeType rangesType, TargetStation station, IReadOnlyCollection<Filter> newFilters, RequestResult result, int? clientId, bool sendBroadcastEvent)
        {
            RangesType = rangesType;
            Station = station;
            NewFilters = newFilters;
            Result = result;
            ClientId = clientId;
            SendBroadcastEvent = sendBroadcastEvent;
        }
    }

    public class RadioJamTargetsChangedEventArgs : IServerCommandResult
    {
        public IReadOnlyList<IRadioJamTarget> RadioJamTargets { get; }
        public TargetStation Station { get; }
        public RequestResult Result { get; }
        public int? ClientId { get; }
        public bool SendBroadcastEvent { get; }

        public RadioJamTargetsChangedEventArgs(IReadOnlyList<IRadioJamTarget> radioJamTargets, TargetStation station, RequestResult result,  int? clientId, bool sendBroadcastEvent)
        {
            RadioJamTargets = radioJamTargets;
            Station = station;
            Result = result;
            ClientId = clientId;
            SendBroadcastEvent = sendBroadcastEvent;
        }
    }

    public class SpecialRangesChangedEventArgs : IServerCommandResult
    {
        public FrequencyType RangesType { get; }
        public TargetStation Station { get; }
        public IReadOnlyCollection<FrequencyRange> Ranges { get; }
        public RequestResult Result { get; }
        public int? ClientId { get; }
        public bool SendBroadcastEvent { get; }

        public SpecialRangesChangedEventArgs(FrequencyType rangesType, TargetStation station, IReadOnlyCollection<FrequencyRange> ranges, RequestResult result, int? clientId, bool sendBroadcastEvent)
        {
            RangesType = rangesType;
            Station = station;
            Ranges = ranges;
            Result = result;
            ClientId = clientId;
            SendBroadcastEvent = sendBroadcastEvent;
        }
    }

    public class RadioJamFhssTargetsChangedEventArgs : IServerCommandResult
    {
        public IReadOnlyList<IRadioJamFhssTarget> RadioJamTargets { get; }
        public TargetStation Station { get; }
        public RequestResult Result { get; }
        public int? ClientId { get; }
        public bool SendBroadcastEvent { get; }

        public RadioJamFhssTargetsChangedEventArgs(IReadOnlyList<IRadioJamFhssTarget> radioJamFhssTargets, TargetStation station, RequestResult result, int? clientId, bool sendBroadcastEvent)
        {
            RadioJamTargets = radioJamFhssTargets;
            Station = station;
            Result = result;
            ClientId = clientId;
            SendBroadcastEvent = sendBroadcastEvent;
        }
    }

    public class StationLocationChangedEventArgs : IServerCommandResult
    {
        public IPosition Position { get; }
        public TargetStation Station { get; }
        public RequestResult Result { get; }
        public int? ClientId { get; }
        public bool SendBroadcastEvent { get; }

        public StationLocationChangedEventArgs(IPosition position, TargetStation station, RequestResult result, int? clientId, bool sendBroadcastEvent)
        {
            Position = position;
            Station = station;
            Result = result;
            ClientId = clientId;
            SendBroadcastEvent = sendBroadcastEvent;
        }
    }
}