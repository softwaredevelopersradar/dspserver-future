namespace DspDataModel.Server
{
    public interface IServerCommandResult
    {
        RequestResult Result { get; }
        int? ClientId { get; }
        bool SendBroadcastEvent { get; }
    }
}