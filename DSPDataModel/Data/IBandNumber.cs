﻿namespace DspDataModel.Data
{
    public interface IBandNumber
    {
        int BandNumber { get; }
    }
}
