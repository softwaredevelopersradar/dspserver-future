﻿namespace DspDataModel.Data
{
    public interface IBandData : IAmplitudeBand, IPhaseBand
    {
    }

    public interface IAmplitudeBand : ICountable
    {
        float[] Amplitudes { get; }
    }

    public interface IPhaseBand : ICountable
    {
        float[] Phases { get; }
    }
}
