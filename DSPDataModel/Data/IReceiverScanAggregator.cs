﻿using System.Collections.Generic;

namespace DspDataModel.Data
{
    public interface IReceiverScanAggregator
    {
        IReadOnlyList<IReceiverScan> Scans { get; }
        void AddScan(IReceiverScan scan);
        void Clear();
        IAmplitudeScan GetSpectrum();
    }
}
