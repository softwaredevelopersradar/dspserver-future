﻿using System.Collections.Generic;
using System.Threading;
using Settings;

namespace DspDataModel.Data
{
    public class FpgaDataScan : IFpgaDataScan
    {
        private readonly IReceiverScan[] _scans;
        public IReadOnlyList<IReceiverScan> Scans => _scans;
        public IReceiverScan RcScan => _scans[Constants.DfReceiversCount];
        public int BandNumber => _scans[0].BandNumber;

        public int ScanIndex { get; }

        private static int _scanCounter = 0;

        public FpgaDataScan() : this(new IReceiverScan[Constants.ReceiversCount])
        { }

        public FpgaDataScan(IReadOnlyList<IReceiverScan> dfScans, IReceiverScan rcScan)
        {
            Contract.Assert(dfScans != null && dfScans.Count == Constants.DfReceiversCount);
            _scans = new IReceiverScan[Constants.ReceiversCount];
            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                _scans[i] = dfScans[i];
            }
            _scans[Constants.DfReceiversCount] = rcScan;

            ScanIndex = Interlocked.Increment(ref _scanCounter);

        }

        public FpgaDataScan(IReceiverScan[] scans)
        {
            Contract.Assert(scans != null && scans.Length == Constants.ReceiversCount);
            _scans = scans;

            ScanIndex = Interlocked.Increment(ref _scanCounter);
        }

        public void SetScan(int receiverIndex, IReceiverScan scan)
        {
            _scans[receiverIndex] = scan;
        }
    }
}
