﻿namespace DspDataModel.Data
{
    public interface ICountable
    {
        int Count { get; }
    }
}