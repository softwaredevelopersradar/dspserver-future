﻿namespace DspDataModel.Data
{
    public class AmplitudeBand : IAmplitudeBand
    {
        public float[] Amplitudes { get; }
        public int Count => Amplitudes.Length;

        public AmplitudeBand(float[] amplitudes)
        {
            Amplitudes = amplitudes;
        }
    }
}
