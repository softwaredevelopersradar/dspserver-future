﻿namespace DspDataModel
{
    public enum ReceiverUsrpSetup
    {
        Receiver3Ghz,
        Receiver3GhzUsrp6Ghz,
        Receiver6Ghz,
    }
}
