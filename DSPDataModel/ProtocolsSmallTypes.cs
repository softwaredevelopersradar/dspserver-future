﻿namespace DspDataModel
{
    public enum FftResolution
    {
        N16384 = 2,
        N8192 = 3,
        N4096 = 4,
    }

    public enum RangeType
    {
        Intelligence = 0,
        RadioJamming = 1
    }

    public enum TargetStation
    {
        Current = 0,
        Linked = 1
    }

    public enum StorageType
    {
        Frs = 0,
        Fhss = 1
    }

    public enum SignalAction
    {
        Hide = 0,
        Restore = 1
    }

    public enum FrequencyType
    {
        Forbidden,
        Known,
        Important
    }
}