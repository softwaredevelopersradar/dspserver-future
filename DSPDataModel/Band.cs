﻿using System;
using Settings;

namespace DspDataModel
{
    public struct Band : IEquatable<Band>
    {
        /// <summary>
        /// Band number from 0 to Config.Instance.BandSettings.BandCount
        /// </summary>
        public int Number { get; }
        public int FrequencyFromKhz { get; }
        public int FrequencyToKhz { get; private set; }

        public Band(int number) : this()
        {
            Number = number;
            FrequencyFromKhz = Constants.FirstBandMinKhz + number * Constants.BandwidthKhz;
            FrequencyToKhz = FrequencyFromKhz + Constants.BandwidthKhz;
        }

        public bool Equals(Band other)
        {
            return other.Number == Number;
        }

        public override bool Equals(object other)
        {
            if (other == null)
                return false;
            return other is Band band && Equals(this, band);
        }

        public override int GetHashCode()
        {
            return Number.GetHashCode();
        }

        public override string ToString() => $"Band {Number}";
    }
}
