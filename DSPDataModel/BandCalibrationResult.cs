﻿using System.Collections.Generic;

namespace DspDataModel
{
    public struct BandCalibrationResult
    {
        public IReadOnlyList<float[]> Phases { get; }
        public IReadOnlyList<float[]> Amplitudes { get; }
        public float[] PhaseDeviations { get; }

        public BandCalibrationResult(IReadOnlyList<float[]> phases, IReadOnlyList<float[]> amplitudes, float[] phaseDeviations) : this()
        {
            Phases = phases;
            Amplitudes = amplitudes;
            PhaseDeviations = phaseDeviations;
        }
    }
}
