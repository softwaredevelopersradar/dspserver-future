﻿using System.Collections.Generic;

namespace DspDataModel.Tasks
{
    public class CalibrationTaskResult
    {
        public List<BandRadioPathCalibration> RadioPathBandCalibrations { get; }

        public CalibrationTaskResult()
        {
            RadioPathBandCalibrations = new List<BandRadioPathCalibration>(Config.Instance.BandSettings.BandCount);
        }

        public void AddBandRadioPathCalibration(BandRadioPathCalibration bandCalibration)
        {
            RadioPathBandCalibrations.Add(bandCalibration);
        }
    }
}
