﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DspDataModel.Tasks
{
    public interface IModeController
    {
        ITaskManager TaskManager { get; }

        /// <summary>
        /// Scan speed In GHz per sec
        /// </summary>
        float ScanSpeed { get; }
        void UpdateScanSpeed(float getScanTimeMs);

        Task Initialize();
        void OnActivated();
        void OnStop();
        IEnumerable<IReceiverTask> CreateTasks();
    }

    public enum DspServerMode
    {
        Stop,
        RadioIntelligence,
        RadioIntelligenceWithDf,
        RadioJammingFrs,
        RadioJammingAfrs,
        RadioJammingVoice,
        RadioJammingFhss,
        Calibration,
        FhssDurationMeasuring
    }

    public static class DspServerModeExtensions
    {
        public static bool IsRadioJamMode(this DspServerMode mode)
        {
            return mode == DspServerMode.RadioJammingFrs || 
                   mode == DspServerMode.RadioJammingAfrs || 
                   mode == DspServerMode.RadioJammingVoice || 
                   mode == DspServerMode.RadioJammingFhss;
        }

        public static bool IsIntelligenceMode(this DspServerMode mode)
        {
            return mode == DspServerMode.RadioIntelligence ||
                   mode == DspServerMode.RadioIntelligenceWithDf;
        }
    }
}
