﻿using System;
using System.Collections.Generic;
using Settings;

namespace DspDataModel
{
    public struct Filter : IEquatable<Filter>
    {
        public int MinFrequencyKhz { get; }
        public int MaxFrequencyKhz { get; }

        public int DirectionFrom { get; }
        public int DirectionTo { get; }

        private readonly FrequencyRange _range;

        public Filter(int minFrequencyKhz, int maxFrequencyKhz, int directionFrom, int directionTo) : this()
        {
            Contract.Assert(minFrequencyKhz > 0);
            Contract.Assert(minFrequencyKhz < maxFrequencyKhz);
            Contract.Assert(0 <= directionFrom && directionFrom <= 360);
            Contract.Assert(0 <= directionTo && directionTo <= 360);

            MinFrequencyKhz = minFrequencyKhz;
            MaxFrequencyKhz = maxFrequencyKhz;
            DirectionFrom = directionFrom;
            DirectionTo = directionTo;

            _range = new FrequencyRange(MinFrequencyKhz, MaxFrequencyKhz);
        }

        public bool IsInFilter(ISignal signal)
        {
            return FrequencyRange.AreIntersected(signal.GetFrequencyRange(), _range) && IsAngleInSector(signal.Direction);
        }

        public bool IsInFilter(float frequencyKhz, float direction)
        {
            Contract.Assert(0 <= direction && direction < 360);
            return _range.Contains(frequencyKhz) && IsAngleInSector(direction);
        }

        public bool IsInFilter(float frequencyKhz)
        {
            return _range.Contains(frequencyKhz);
        }

        private bool IsAngleInSector(float direction) => Phases.PhaseMath.IsAngleInSector(direction, DirectionFrom, DirectionTo);

        public IEnumerable<Band> SplitInBands() => _range.SplitInBands();

        public FrequencyRange ToFrequencyRange() => new FrequencyRange(MinFrequencyKhz, MaxFrequencyKhz);

        public bool Equals(Filter other)
        {
            return MinFrequencyKhz == other.MinFrequencyKhz && MaxFrequencyKhz == other.MaxFrequencyKhz
                   && DirectionFrom == other.DirectionFrom && DirectionTo == other.DirectionTo;
        }
    }
}
