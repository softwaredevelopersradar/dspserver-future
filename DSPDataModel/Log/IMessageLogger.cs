﻿using System;

namespace DspDataModel
{
    public interface IMessageLogger
    {
        ConsoleColor ErrorColor { get; set; }
        bool IsFpgaLogEnabled { get; set; }
        bool IsReceiversLogEnabled { get; set; }
        bool IsServerLogEnabled { get; set; }
        bool IsShaperLogEnabled { get; set; }
        bool IsTraceLogEnabled { get; set; }
        ConsoleColor MessageColor { get; set; }
        ConsoleColor WarningColor { get; set; }

        void Error(Exception e);
        void Error(Exception e, string message);
        void Error(string message);
        void FpgaLog(string message);
        void Log(string message);
        void Log(string message, ConsoleColor color);
        void ReceveirsLog(string seed, byte[] data);
        void ServerLog(string message);
        void ShaperLog(string message);
        void Trace(string message, ConsoleColor color = ConsoleColor.Gray);
        void Warning(string message);
    }
}