﻿namespace DspDataModel
{
    public enum SignalModulation
    {
        Unknown = -1,
        ZeroImpulse = 0,
        Am2 = 1,
        Am101 = 2,
        Fm2 = 3,
        Fsk2 = 4,
        AmFm = 5,
        Fsk4 = 6,
        Fsk8 = 7,
        Fm = 8,
        Fsk = 9, // Частотная манипуляция Frequency-shift keying
        Wideband = 10,
    }
}
