﻿namespace DspDataModel.Database
{
    public struct JammingSettings
    {
        public FftResolution FhssFftResolution;
        public int FhssRadiationTime;
        public int LongWorkingSignalDurationMs;
        public int ChannelsInLiter;
        public int EmitionDuration;
        public int DirectionSearchSector;
        public int FrequencySearchBandwidthKhz;
        public int Threshold;
        public int JammingAutoStopIntervalSec;
    }
}
