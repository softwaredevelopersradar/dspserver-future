﻿using System;
using DspDataModel.Storages;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.Hardware;
using DspDataModel.RadioJam;
using DspDataModel.Tasks;

namespace DspDataModel.Database
{
    public interface IDatabaseController
    {
        bool IsConnected { get; }

        event EventHandler<IEnumerable<Filter>> IntelligenceFiltersReceivedEvent;
        event EventHandler<IEnumerable<Filter>> RadioJamFiltersReceivedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> KnownRangesReceivedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> ImportantRangesReceivedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesReceivedEvent;
        event EventHandler<IReadOnlyList<IRadioJamTarget>> FrsJamTargetsReceivedEvent;
        event EventHandler<IReadOnlyList<IRadioJamFhssTarget>> FhssJamTargetsReceivedEvent;
        event EventHandler<JammingSettings> JammingSettingsReceived;
        event EventHandler StopInfiniteJamming;

        void RadioSourceTableAddRange(IEnumerable<IRadioSource> radioSources);
        void ClearRadioSourceTable();
        void DeleteRadioSourceRecord(int id);

        void FhssTableAddRange(IEnumerable<IFhssNetwork> fhssNetworks);
        void FhssTableClear();

        void UpdateFrsJamTarget(IRadioJamTarget target);
        void UpdateFrsRadioJamState(IReadOnlyList<IRadioJamTarget> targets);
        void UpdateFhssRadioJamState(IReadOnlyList<(int id, bool isJammed)> targets, IReadOnlyList<float> jammedFrequencies);
        void ClearRadioJamStateTables();

        void UpdateGpsCoordinates(IGpsReceiver gpsReceiver);

        Task Connect();
        void Disconnect();

        void UpdateStationMode(DspServerMode mode);
    }
}
