﻿using System;
using System.Threading;

namespace DspDataModel
{
    /// <summary>
    /// structure that's used in the signal storage
    /// </summary>
    public struct ImpulseSignal
    {
        public int ScanIndex { get; }
        public ISignal Signal { get; }
        public DateTime Time { get; }
        public int Id { get; }

        private static int _idCounter = 0;

        public ImpulseSignal(ISignal signal, int scanIndex)
            : this (signal, scanIndex, DateTime.Now)
        { }

        public ImpulseSignal(ISignal signal, int scanIndex, DateTime time)
        {
            ScanIndex = scanIndex;
            Signal = signal;
            Time = time;

            Id = Interlocked.Increment(ref _idCounter);
        }
    }
}