namespace DspDataModel.AmplitudeCalulators
{
    public interface IAmplitudeCalculatorFactory
    {
        IAmplitudeCalculator CreateCalculator(AmplitudeCalculatorPolicy policy);
    }

    public enum AmplitudeCalculatorPolicy
    {
        Average,
        Max,
        MinMax
    }

    public static class AmplitudeCalculatorFactoryExtensions
    {
        public static IAmplitudeCalculator CreateCalculator(this IAmplitudeCalculatorFactory self)
        {
            return self.CreateCalculator(Config.Instance.DirectionFindingSettings.AmplitudeCalculatorPolicy);
        }
    }
}