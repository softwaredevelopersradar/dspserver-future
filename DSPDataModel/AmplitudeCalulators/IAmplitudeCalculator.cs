﻿namespace DspDataModel.AmplitudeCalulators
{
    public interface IAmplitudeCalculator
    {
        float[] CalculateAmplitudes(float[][] amplitudes);
    }
}
