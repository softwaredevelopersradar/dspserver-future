﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataProcessor.Fhss;
using DataStorages;
using DspDataModel;
using DspDataModel.Hardware;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Tasks;
using HardwareLibrary.FpgaDevices;
using Settings;

namespace TasksLibrary.JamStrategies
{
    /// <summary>
    /// Not a jamming strategy though, because we only measure the duration without radiation.
    /// </summary>
    public class FhssDurationMeasureStrategy
    {
        private const int MaxSimulationImpulseDuration = 11_000_000;

        private readonly int _controlTimeMs;
        private readonly IFpgaDeviceManager _deviceManager;
        private readonly ITaskManager _taskManager;
        private readonly IServerController _serverController;
        private readonly IReadOnlyList<IRadioJamFhssTarget> _fhssTargets;
        private FhssSetup _fhssSetup;
        public FhssDurationMeasureStrategy(
            ITaskManager taskManager,
            IServerController serverController)
        {
            _controlTimeMs = Config.Instance.FhssSearchSettings.FhssDurationMeasureTimeMs;
            _taskManager = taskManager;
            _deviceManager = taskManager.HardwareController.DeviceManager;
            _serverController = serverController;
            var fhssNetworks = taskManager.FhssNetworkStorage.GetFhssNetworks()
                .Where(n => n.IsDurationMeasured == false)
                .Select(n => new RadioJamFhssTarget(
                    minFrequencyKhz: n.MinFrequencyKhz,
                    maxFrequencyKhz: n.MaxFrequencyKhz,
                    threshold: taskManager.FilterManager.GetThreshold(taskManager.FilterManager.Bands[0].Number),
                    modulationCode: 0,
                    manipulationCode: 0,
                    deviationCode: 0,
                    forbiddenRanges: new List<FrequencyRange>(),
                    id: n.Id)
            ).ToList();
            if (fhssNetworks.Count < Constants.FhssChannelsCount)
                _fhssTargets = fhssNetworks;
            else
                _fhssTargets = fhssNetworks.Take(Constants.FhssChannelsCount).ToList();
        }

        private FhssSetup GenerateFhssSetup(IRadioJamFhssTarget target)
        {
            var threshold = new[] { target.Threshold };
            var bandSettings = _taskManager.HardwareController.DeviceManager.BandsSettingsSource.Settings;
            var fpgaReceiverSettings = _fhssTargets
                .Select(t => new FhssReceiverSetup(
                    (int)(t.MinFrequencyKhz / 1000) * 1000,
                    bandSettings[Utilities.GetBandNumber(t.MinFrequencyKhz + Constants.BandwidthKhz * 0.5f)].ShouldBeReversed))
                .ToArray();
            //minimum precision for duration measurement, since we do not need exact frequency, just the fact that it is changed
            return new FhssSetup(FftResolution.N4096, threshold, _fhssTargets, fpgaReceiverSettings);
        }

        private void SetupReceivers(IRadioJamFhssTarget target)
        {
            var receiverCentralFrequencyKhz = target.MinFrequencyKhz + Constants.BandwidthKhz * 0.5f;
            _taskManager.HardwareController.ReceiverManager.Receivers[0].SetFrequency((int)(receiverCentralFrequencyKhz / 1000));
        }

        private bool StartDurationMeasurement(IRadioJamFhssTarget target)
        {
            _fhssSetup = GenerateFhssSetup(target);
            SetupReceivers(target);

            if (!_taskManager.HardwareController.DeviceManager.StartFhssDurationMeasurement(_fhssSetup))
            {
                throw new Exception("Device manager can't start fhss duration mode");
            }
            return true;
        }

        public Task MeasuringTask(CancellationToken token)
        {
            return Task.Run(() => MeasuringTaskInner(token));
        }

        private void MeasuringTaskInner(CancellationToken token)
        {
            foreach (var target in _fhssTargets)
            {
                try
                {
                    token.ThrowIfCancellationRequested();//to ensure, that we are okay to use fpga devices
                    if (StartDurationMeasurement(target) == false)
                    {
                        MessageLogger.Warning($"Error occured while trying to measure the duration of " +
                                              $"{target.MinFrequencyKhz} - {target.MaxFrequencyKhz}, " +
                                              "measurement is aborted.");
                        continue;
                    }

                    MeasuringCycle(target, token);
                }
                catch (OperationCanceledException canceledException)
                {
                    MessageLogger.Warning($"Manually finished measuring taks with exception : {canceledException}");
                    return; //manual finish of 
                }
                catch (Exception exception)
                {
                    MessageLogger.Error(
                        $"Couldn't measure {target.MinFrequencyKhz} - {target.MaxFrequencyKhz} network duration.\r\n" +
                        exception.StackTrace);
                }
            }

            if (token.IsCancellationRequested)
                return;
            _serverController.SetMode(_serverController.PreDurationMeasurementMode);
        }

        private void MeasuringCycle(IRadioJamFhssTarget target, CancellationToken outerCancellationToken)
        {
            var device = _deviceManager.Devices[0];// only first fpga device is used for fhss

            // is needed for simulation only
            var isSimulation = device is FakeFpgaDevice;
            var bandNumbers = _taskManager.HardwareController.ReceiverManager.Receivers
                .Select(r => r.BandNumber)
                .ToArray();

            var previousScanIndex = -1;
            var peakIndecies = new List<int>();
            const int noPeakFoundCode = 16383;

            var cancellationTokenSource = new CancellationTokenSource();
            var controlWatch = new System.Diagnostics.Stopwatch();
            var indexTask = new Task(
                action:
                () =>
                {
                    while (cancellationTokenSource.IsCancellationRequested == false)
                    {
                        if (outerCancellationToken.IsCancellationRequested)
                            break;
                        if (!isSimulation)
                        {
                            var scanIndex = device.GetScanIndex();
                            if (scanIndex == previousScanIndex)
                            {
                                continue;
                            }
                            previousScanIndex = scanIndex;
                        }
                        else
                        {
                            Array.Copy(
                                _fhssTargets.Select(t => Utilities.GetBandNumber(t.MinFrequencyKhz)).ToArray(),
                                bandNumbers,
                                _fhssTargets.Count
                            );
                            _deviceManager.GetScan(bandNumbers);
                        }

                        for (var i = 0; i < _fhssSetup.ReceiverSettings.Count; ++i)
                        {
                            var peakIndex = device.GetFhssPeakIndex(i);
                            if (peakIndex == noPeakFoundCode)
                            {
                                continue;
                            }
                            peakIndecies.Add(peakIndex);
                        }
                    }
                },
                cancellationToken: cancellationTokenSource.Token);

            if (indexTask.Status == TaskStatus.Created)
                indexTask.Start();
            controlWatch.Start();

            while (controlWatch.Elapsed.TotalMilliseconds <= _controlTimeMs)
            {
                outerCancellationToken.ThrowIfCancellationRequested();
                Thread.Sleep(1);
                continue;
            }
            controlWatch.Stop();
            cancellationTokenSource.Cancel();
            cancellationTokenSource.Dispose();

            outerCancellationToken.ThrowIfCancellationRequested();//to ensure, that we are okay to use fpga devices
            _deviceManager.StopFhssMode();
            device.DropFhssMeasurementFlag();

            var duration = MeasureDuration(peakIndecies);
            if (isSimulation)
                duration = new Random().Next(0, MaxSimulationImpulseDuration);

            MessageLogger.Warning(
                $"Measuring result of {target.MinFrequencyKhz} - {target.MaxFrequencyKhz} : {duration}");

            if (duration < Constants.FhssMinImpulseDuration || duration > Constants.FhssMaxImpulseDuration)
            {
                MessageLogger.Warning($"Measured duration {duration} mcs is not going to be applied, since it is too small or big");
                return;
            }

            var network = _taskManager.FhssNetworkStorage.GetFhssNetworks().FirstOrDefault(n => n.Id == target.Id);
            network?.Update(
                new FhssNetwork(
                    minFrequencyKhz: network.MinFrequencyKhz,
                    maxFrequencyKhz: network.MaxFrequencyKhz,
                    bandwidthKhz: network.BandwidthKhz,
                    stepKhz: network.StepKhz,
                    fhssUserInfos: network.UserInfo,
                    impulseDurationMs: duration,
                    isDurationMeasured: true,
                    frequenciesCount: network.FrequenciesCount,
                    fixedRadioSources: network.FixedRadioSources
                    ));
        }

        private float MeasureDuration(IList<int> peakIndecies)
        {
            MessageLogger.Warning($"received {peakIndecies.Count}");
            //clear index copies, when frequency hopping didn't happened, and the same index came, ex. from 111222333 -> 123
            while (true)
            {
                var indexToRemove = -1;
                for (int i = 0; i < peakIndecies.Count - 1; i++)
                {
                    if (peakIndecies[i] == peakIndecies[i + 1])
                    {
                        indexToRemove = i;
                        break;
                    }
                }
                if (indexToRemove != -1)
                {
                    peakIndecies.RemoveAt(indexToRemove);
                    continue;
                }
                break;
            }
            var boundariesCount = 2;
            if (peakIndecies.Count <= 50) //when indecies count is small, boundaries are included to result 
                boundariesCount = 0;
            var listWithout4097 = peakIndecies.Where(i => i != 4097).ToList();//todo : remove?
            MessageLogger.Warning($"after removals : {peakIndecies.Count}");
            return (float)_controlTimeMs * 1000 / (listWithout4097.Count - boundariesCount);
        }
    }
}