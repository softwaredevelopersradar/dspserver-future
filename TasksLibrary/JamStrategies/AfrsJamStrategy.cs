﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.RadioJam;
using DspDataModel.Tasks;
using Phases;
using SharpExtensions;
using TasksLibrary.Tasks;

namespace TasksLibrary.JamStrategies
{
    // Подавление АПРЧ
    public class AfrsJamStrategy : JamStrategy
    {
        private readonly IEnumerable<float> _initialFrs;
        private readonly float _gap = Config.Instance.DirectionFindingSettings.SignalsMergeGapKhz;
        private readonly static FrequencyRange[] _afrsWorkRanges = new []
        {
            new FrequencyRange(33_000, 88_000),
            new FrequencyRange(88_000, 108_000),
            new FrequencyRange(108_000, 170_000),
            new FrequencyRange(170_000, 220_000),
            new FrequencyRange(220_000, 400_000),
            new FrequencyRange(400_000, 512_000),
            new FrequencyRange(512_000, 860_000),
            new FrequencyRange(860_000, 1215_000),
            new FrequencyRange(1215_000, 1575_000),
            new FrequencyRange(1575_000, 3000_000)
        };

        public AfrsJamStrategy(IRadioJamManager radioJamManager, ITaskManager taskManager,
            UpdateFrsJamStateDelegate updateStateEventFunction)
            : base(radioJamManager, taskManager, updateStateEventFunction)
        {
            var initialTargets = RadioJamManager.Storage.FrsTargets;
            _initialFrs = TaskManager.RadioSourceStorage.GetRadioSources()
                .Where(rs => rs.IsActive && !initialTargets.Any(
                    t => Math.Abs(t.FrequencyKhz - rs.FrequencyKhz) < _gap))
                .Select(s => s.FrequencyKhz).ToList();
        }

        protected override async Task<IReadOnlyList<TargetSignalPair>> UpdateActiveSources(IReadOnlyList<TargetSignalPair> activeSources)
        {
            var leftSources = RadioJamManager.Storage.FrsTargets
                .Where(t => activeSources.All(s => !ReferenceEquals(s.Target, t)))
                .ToArray();

            var resultSources = (await FindDfRadioSources(leftSources)).ToList();
            foreach (var source in resultSources)
            {
                source.Target.FrequencyKhz = source.Signal.FrequencyKhz;
                TaskManager.DatabaseController.UpdateFrsJamTarget(source.Target);
            }

            resultSources.AddRange(activeSources.Select(r => new TargetSignalPair(r.Target, r.Signal)));
            return resultSources;
        }

        private IEnumerable<Band> GetSignalSearchRange(IRadioJamTarget signal)
        {
            var workBands = _afrsWorkRanges.First(r => r.Contains(signal.FrequencyKhz)).SplitInBands();
            var intelligenceBands = TaskManager.FilterManager.IntelligenceFilters.SelectMany(f => f.SplitInBands());
            var jammingBands = TaskManager.FilterManager.RadioJamFilters.SelectMany(f => f.SplitInBands());
            return workBands.Intersect(intelligenceBands).Intersect(jammingBands);
        }

        /// <summary>
        /// finds new frequencies of not found targets
        /// </summary>
        private async Task<List<TargetSignalPair>> FindDfRadioSources(IReadOnlyList<IRadioJamTarget> targets)
        {
            if (targets.IsNullOrEmpty())
            {
                return new List<TargetSignalPair>();    
            }

            var bands = targets
                .SelectMany(GetSignalSearchRange)
                .Distinct()
                .ToArray();

            var taskSetup = new DirectionFindingTaskSetup
            {
                TaskManager = this.TaskManager,
                Objectives = bands,
                Priority = 1,
                ReceiversIndexes = new[] {0, 1, 2, 3, 4},
                IsEndless = false,
                AutoUpdateObjectives = false
            };

            var task = new DirectionFindingTask(taskSetup);
            TaskManager.AddTask(task);
            await task.WaitForResult();

            var result = new List<TargetSignalPair>();

            foreach (var target in targets)
            {
                var workRange = _afrsWorkRanges.First(r => r.Contains(target.FrequencyKhz));
                var intelligenceFilter = TaskManager.FilterManager.IntelligenceFilters.FirstOrDefault(
                    f => f.ToFrequencyRange().Contains(target.FrequencyKhz));
                var jammingFilter = TaskManager.FilterManager.RadioJamFilters.FirstOrDefault(
                    f => f.ToFrequencyRange().Contains(target.FrequencyKhz));

                var signal = task.TypedResult.Signals.FirstOrDefault(s =>
                {
                    if (result.Any(pair => pair.Signal == s))
                    {
                        return false;
                    }
                    
                    if (!workRange.Contains(s.FrequencyKhz) ||
                        !intelligenceFilter.ToFrequencyRange().Contains(s.FrequencyKhz) ||
                        !jammingFilter.ToFrequencyRange().Contains(s.FrequencyKhz))
                    {
                        return false;
                    }

                    if (intelligenceFilter.DirectionFrom > s.Direction ||
                        intelligenceFilter.DirectionTo < s.Direction ||
                        jammingFilter.DirectionFrom > s.Direction ||
                        jammingFilter.DirectionTo < s.Direction)
                    {
                        return false;
                    }

                    if (TaskManager.FilterManager.ForbiddenFrequencies.Any(r => r.Contains(s.FrequencyKhz)))
                        return false;

                    if (PhaseMath.Angle(target.Direction, s.Direction) > RadioJamManager.DirectionSearchSector)
                    {
                        return false;
                    }

                    if (s.Amplitude < RadioJamManager.Threshold)
                    {
                        return false;
                    }

                    var otherTargets = RadioJamManager.Storage.FrsTargets.Where(t => !ReferenceEquals(t,target));
                    if (otherTargets.Any(
                        t => Math.Abs(s.FrequencyKhz - t.FrequencyKhz) < _gap))
                    {
                        return false;
                    }

                    if (_initialFrs.Any(rs=> Math.Abs(rs - s.FrequencyKhz) < _gap))
                    {
                        return false;
                    }

                    return true;
                });
                if (signal != null)
                {
                    result.Add(new TargetSignalPair(target, signal));
                }
            }
            return result;
        }
    }
}
