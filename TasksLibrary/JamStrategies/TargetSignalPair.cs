using DspDataModel;
using DspDataModel.RadioJam;

namespace TasksLibrary.JamStrategies
{
    public class TargetSignalPair
    {
        public IRadioJamTarget Target { get; }
        public ISignal Signal { get; }

        public TargetSignalPair(IRadioJamTarget target, ISignal signal)
        {
            Target = target;
            Signal = signal;
        }
    }
}