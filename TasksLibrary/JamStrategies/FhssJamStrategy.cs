﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel;
using DspDataModel.Hardware;
using DspDataModel.RadioJam;
using DspDataModel.Tasks;
using HardwareLibrary.FpgaDevices;
using Settings;

namespace TasksLibrary.JamStrategies
{
    public class FhssJamStrategy : IRadioJamStrategy
    {
        private readonly IFpgaDeviceManager _deviceManager;
        private readonly IReadOnlyList<IRadioJamFhssTarget> _fhssTargets;
        private readonly IReadOnlyList<IRadioJamFhssTarget> _fhssChannelTargets;
        private readonly int _maxPeakCount;
        private readonly TimeSpan _peakAccumulationDuration;
        private readonly IRadioJamManager _radioJamManager;
        private readonly IRadioJamShaper _shaper;
        private readonly ITaskManager _taskManager;
        private const int ChannelBandwidth = Constants.BandwidthKhz;

        private readonly UpdateFhssJamStateDelegate _updateStateEventFunction;

        private FhssSetup _fhssSetup;
        private DateTime _lastEventTime;

        public bool StopJammingAfterTaskEnd { get; set; }

        public FhssJamStrategy(
            IRadioJamManager radioJamManager,
            ITaskManager taskManager,
            TimeSpan peakAccumulationDuration,
            int maxPeakCount,
            UpdateFhssJamStateDelegate updateStateEventFunction)
        {
            _taskManager = taskManager;
            _radioJamManager = radioJamManager;
            _deviceManager = taskManager.HardwareController.DeviceManager;
            _shaper = radioJamManager.Shaper;
            _fhssTargets = radioJamManager.Storage.FhssTargets;
            _fhssChannelTargets = CreateChannelTargets(_fhssTargets);
            _maxPeakCount = maxPeakCount;
            _updateStateEventFunction = updateStateEventFunction;
            _peakAccumulationDuration = peakAccumulationDuration;
        }

        public Task JammingTask(CancellationToken token)
        {
            return Task.Run(() => JammingTaskInner(token));
        }

        private List<IRadioJamFhssTarget> CreateChannelTargets(IReadOnlyList<IRadioJamFhssTarget> _fhssTargets)
        {
            var channelTargets = new List<IRadioJamFhssTarget>();
            for (int i = 0; i < _fhssTargets.Count; i++)
            {
                var t = SplitFhssTarget(_fhssTargets[i]);
                if (channelTargets.Count + t.Count <= Constants.FhssChannelsCount)
                {
                    channelTargets.AddRange(t);
                }
            }
            return channelTargets;

            List<IRadioJamFhssTarget> SplitFhssTarget(IRadioJamFhssTarget target)
            {
                var targetSplit = new List<IRadioJamFhssTarget>();
                var occupiedChannels = (int)(target.MaxFrequencyKhz - target.MinFrequencyKhz) / ChannelBandwidth;
                if ((target.MaxFrequencyKhz - target.MinFrequencyKhz) % ChannelBandwidth != 0)
                    occupiedChannels++;
                for (int i = 0; i < occupiedChannels; i++)
                {
                    float start = target.MinFrequencyKhz + ChannelBandwidth * i;
                    float end;
                    if (i != occupiedChannels - 1)
                        end = target.MinFrequencyKhz + ChannelBandwidth * (i + 1);
                    else
                        end = target.MaxFrequencyKhz;
                    var forbiddenRanges = new List<FrequencyRange>();
                    foreach (var range in target.ForbiddenRanges)
                    {
                        if (range.StartFrequencyKhz <= start && range.EndFrequencyKhz <= start)
                            continue; // previous channel
                        if (range.StartFrequencyKhz >= end && range.EndFrequencyKhz >= end)
                            continue; // next channel
                        var rangeStart = Math.Max(range.StartFrequencyKhz, start);
                        var rangeEnd = Math.Min(range.EndFrequencyKhz, end);
                        forbiddenRanges.Add(new FrequencyRange(rangeStart, rangeEnd));
                    }

                    var channelTarget = new RadioJamFhssTarget(
                        minFrequencyKhz: start,
                        maxFrequencyKhz: end,
                        threshold: target.Threshold,
                        modulationCode: target.ModulationCode,
                        deviationCode: target.DeviationCode,
                        manipulationCode: target.ManipulationCode,
                        forbiddenRanges: forbiddenRanges,
                        id: target.Id);
                    targetSplit.Add(channelTarget);
                }
                return targetSplit;
            }
        }

        private FhssSetup GenerateFhssSetup()
        {
            var thresholds = _fhssChannelTargets
                .Select(t => t.Threshold)
                .Take(_fhssChannelTargets.Count)
                .ToArray();

            var bandSettings = _taskManager.HardwareController.DeviceManager.BandsSettingsSource.Settings;
            var fpgaReceiverSettings = _fhssChannelTargets
                .Select(t => new FhssReceiverSetup(
                    (int) (t.MinFrequencyKhz / 1000) * 1000,                            // removing frequency at which receiver can't stand
                    bandSettings[Utilities.GetBandNumber(t.MinFrequencyKhz + Constants.BandwidthKhz * 0.5f)].ShouldBeReversed))
                .ToArray();

            return new FhssSetup(_radioJamManager.FhssFftResoultion, thresholds, _fhssChannelTargets, fpgaReceiverSettings);
        }

        private void StartJamming()
        {
            _fhssSetup = GenerateFhssSetup();
            SetupReceivers();
            if (!_taskManager.HardwareController.DeviceManager.StartFhssMode(_fhssSetup))
                throw new Exception("Device manager can't start fhss mode");
            _shaper.StartFhssJamming((uint) _radioJamManager.FhssFftResoultion - 1, _radioJamManager.FhssJamDuration,
                _fhssTargets);
        }

        private void SetupReceivers()
        {
            for (var i = 0; i < _fhssChannelTargets.Count; ++i)
            {
                var receiverCentralFrequencyKhz = _fhssChannelTargets[i].MinFrequencyKhz + Constants.BandwidthKhz * 0.5f;
                _taskManager.HardwareController.ReceiverManager.Receivers[i]
                    .SetFrequency((int) (receiverCentralFrequencyKhz / 1000)); //creates <1 MHz mistake with float to int cut, outplayed at setting targets
            }
        }

        private float IndexToFrequency(int networkStartFrequencyKhz, int sampleIndex, int bandSampleCount)
        {//todo : find out why the simulator indices do not work as intended
            var bandCenterSample = bandSampleCount / 2;
            if (sampleIndex >= bandCenterSample)
                sampleIndex -= bandCenterSample;
            else
                sampleIndex += bandCenterSample;
            var multiplier = Constants.ReceiverSampleCount / bandSampleCount;
            sampleIndex *= multiplier;

            const int arrayOffset = (Constants.ReceiverSampleCount - Constants.BandSampleCount) / 2;
            sampleIndex -= arrayOffset;

            return networkStartFrequencyKhz + Constants.SamplesGapKhz * sampleIndex;
        }

        private void JammingTaskInner(CancellationToken token)
        {
            StartJamming();

            var device = _deviceManager.Devices[0];
            _lastEventTime = DateTime.UtcNow;
            var jammedTargets = new(int id, bool isJammed)[_fhssTargets.Count];
            for (var i = 0; i < jammedTargets.Length; ++i)
                jammedTargets[i].id = _fhssTargets[i].Id;

            try
            {
                JammingCycle();
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
            }
            finally
            {
                _deviceManager.StopFhssMode();
                _shaper.StopJamming();
            }

            void JammingCycle()
            {
                // is needed for simulation only
                var isSimulation = device is FakeFpgaDevice;
                var bandNumbers = _taskManager.HardwareController.ReceiverManager.Receivers
                    .Select(r => r.BandNumber)
                    .ToArray();

                var bandSampleCount = _radioJamManager.FhssFftResoultion.GetSampleCount();
                var previousScanIndex = -1;
                var peakFrequencies = new HashSet<float>();
                const int noPeakFoundCode = 16383;

                while (!token.IsCancellationRequested)
                {
                    if (isSimulation)
                    {
                        Array.Copy(
                            _fhssChannelTargets.Select(t => Utilities.GetBandNumber(t.MinFrequencyKhz)).ToArray(),
                            bandNumbers,
                            _fhssChannelTargets.Count
                        );
                        _deviceManager.GetScan(bandNumbers);
                    }
                    else
                    {
                        var scanIndex = _deviceManager.Devices[0].GetScanIndex();
                        if (scanIndex == previousScanIndex)
                            continue;
                        previousScanIndex = scanIndex;
                    }

                    for (var i = 0; i < _fhssSetup.ReceiverSettings.Count; ++i)
                    {
                        var peakIndex = device.GetFhssPeakIndex(i);
                        if (peakIndex == noPeakFoundCode)
                            continue;
                        var networkStartFrequency = _fhssSetup.ReceiverSettings[i].ReceiverStartFrequencyKhz;

                        var frequencyKhz = IndexToFrequency(networkStartFrequency, peakIndex, bandSampleCount);
                        if (peakFrequencies.Count < _maxPeakCount)
                            peakFrequencies.Add((int) frequencyKhz);

                        var jammedNetworks = _fhssChannelTargets.Where(t => t.MinFrequencyKhz <= frequencyKhz &&
                                                                     frequencyKhz <= t.MaxFrequencyKhz);
                        foreach (var network in jammedNetworks)
                        {
                            var index = Array.FindIndex(jammedTargets, e => e.id == network.Id);
                            jammedTargets[index].isJammed = true;
                        }
                    }
                    var now = DateTime.UtcNow;
                    if (now - _lastEventTime > _peakAccumulationDuration)
                    {
                        // send event
                        _updateStateEventFunction(jammedTargets, peakFrequencies.ToArray());

                        peakFrequencies = new HashSet<float>();
                        jammedTargets = new(int id, bool isJammed)[_fhssTargets.Count];
                        for (var i = 0; i < jammedTargets.Length; ++i)
                            jammedTargets[i].id = _fhssTargets[i].Id;
                        _lastEventTime = DateTime.UtcNow;
                    }
                }
            }
        }
    }
}