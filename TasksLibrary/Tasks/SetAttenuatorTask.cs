﻿using System.Collections.Generic;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Tasks;

namespace TasksLibrary.Tasks
{
    public class SetAttenuatorTask : ReceiverTask<object>
    {
        private readonly float _attenuatorValue;

        private readonly bool _isConstAttenuatorEnabled;

        private readonly ITaskManager _taskManager;

        public SetAttenuatorTask(ITaskManager taskManager, int bandNumber, bool isConstAttenuatorEnabled, float attenuatorValue)
            : base(taskManager.DataProcessor, new[] {new Band(bandNumber),}, new[] {0, 1, 2, 3, 4, 5}, 1, false, 2)
        {
            _attenuatorValue = attenuatorValue;
            _isConstAttenuatorEnabled = isConstAttenuatorEnabled;
            _taskManager = taskManager;
            IsEndless = false;
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var bandNumber = objectives[0].BandNumber;
            _taskManager.HardwareController.ReceiverManager
                .SetAttenuatorValue(bandNumber, _isConstAttenuatorEnabled, _attenuatorValue);
            TypedResult = null;
        }
    }
}