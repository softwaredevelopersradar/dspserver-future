﻿using System;
using System.Collections.Generic;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Tasks;

namespace TasksLibrary.Tasks
{
    public class ReceiversSequenceTask : ReceiverTask<ReceiversSequenceTask.TaskResult>
    {
        private readonly ITaskManager _taskManager;

        private readonly bool _findSignals;

        /// <summary>
        /// fires when new portion of data (set of scans) is handled
        /// </summary>
        public event EventHandler<TaskResult> DataProcessedEvent;

        public ReceiversSequenceTask(ITaskManager taskManager, int averagingCount, bool findSignals = true, bool isEndless = true, int priority = 0)
            : base(taskManager.DataProcessor, taskManager.FilterManager.Bands, new [] {0, 1, 2, 3, 4}, averagingCount, true, priority, moveReceiversSynchronously: false)
        {
            _taskManager = taskManager;
            _findSignals = findSignals;
            IsEndless = isEndless;
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var scans = aggregator.GetSpectrums();

            var result = ProcessScans();
            if (IsEndless)
            {   
                DataProcessedEvent?.Invoke(this, result);
            }
            else
            {
                TypedResult = result;
            }

            TaskResult ProcessScans()
            {
                var resultScans = new List<IDataScan>();
                var resultSignals = new List<ISignal>();
                var updatedBands = new HashSet<int>();

                for (var i = 0; i < scans.Count; i++)
                {
                    var scan = scans[i];
                    if (updatedBands.Contains(scan.BandNumber) || objectives[i].BandNumber == -1)
                    {
                        continue;
                    }

                    var noiseLevel = scan.Amplitudes.GetNoiseLevel();
                    _taskManager.FilterManager.UpdateNoiseLevel(scan.BandNumber, noiseLevel);

                    if (_findSignals)
                    {
                        var threshold = _taskManager.FilterManager.GetThreshold(objectives[i].BandNumber);
                        var processConfig = ScanProcessConfig.CreateConfigWithoutDf(threshold);
                        resultSignals.AddRange(DataProcessor.GetSignals(scan, processConfig).Signals);
                    }
                    resultScans.Add(scan);
                    updatedBands.Add(scan.BandNumber);
                }

                return new TaskResult(resultScans, resultSignals);
            }
        }

        public class TaskResult
        {
            public IReadOnlyList<IAmplitudeScan> Scans { get; }

            /// <summary>
            /// Signals is empty list if user turns off find signals option in task constructor
            /// </summary>
            public IReadOnlyList<ISignal> Signals { get; }

            public TaskResult(IReadOnlyList<IAmplitudeScan> scans, IReadOnlyList<ISignal> signals)
            {
                Scans = scans;
                Signals = signals;
            }
        }
    }
}
