﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Tasks;

namespace TasksLibrary.Tasks
{
    public class DirectionFindingTask : ReceiverTask<ProcessResult>
    {
        private IReadOnlyList<FrequencyRange> _frequencyRanges;

        /// <summary>
        /// fires when new portion of data (set of scans) is handled
        /// </summary>
        public event EventHandler<SignalsProcessedEventArg> SignalsProcessedEvent;

        private readonly List<ISignal> _signals;
        private readonly List<ISignal> _rawSignals;

        private readonly ITaskManager _taskManager;
        private readonly DirectionFindingTaskSetup _setup;

        public DirectionFindingTask(DirectionFindingTaskSetup setup)
            : base(setup.TaskManager.DataProcessor, setup.GetObjectives(), setup.ReceiversIndexes, setup.ScanAveragingCount * setup.BearingAveragingCount, setup.AutoUpdateObjectives, setup.Priority)
        {
            _taskManager = setup.TaskManager;
            _frequencyRanges = setup.GetFrequencyRanges();
            _setup = setup;
            IsEndless = setup.IsEndless;
            _signals = new List<ISignal>();
            _rawSignals = new List<ISignal>();
        }

        public override void UpdateObjectives(IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            base.UpdateObjectives(frequencyRanges);
            _frequencyRanges = frequencyRanges;
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var bandNumber = objectives[0].BandNumber;
            var scan = aggregator.GetDataScan();
            _taskManager.FilterManager.UpdateNoiseLevel(scan);
            var threshold = _taskManager.FilterManager.GetThreshold(bandNumber);
            var dfConfig = ScanProcessConfig.CreateDfConfig(threshold, _setup.BearingAveragingCount, _setup.ScanAveragingCount, _frequencyRanges, _setup.CalculateHighQualityPhases);
            var signals = DataProcessor.GetSignals(scan, dfConfig);
            var result = new ProcessResult(signals.Signals, signals.RawSignals);

            if (IsEndless)
            {
                SignalsProcessedEvent?.Invoke(this, new SignalsProcessedEventArg(result, scan));
            }
            else
            {
                _signals.AddRange(result.Signals.Where(IsSignalInFrequencyRange));
                _rawSignals.AddRange(result.RawSignals.Where(IsSignalInFrequencyRange));
                if (bandNumber != Objectives.Last().Number)
                {
                    return;
                }
                TypedResult = new ProcessResult(_signals, _rawSignals);
            }
        }

        private bool IsSignalInFrequencyRange(ISignal signal)
        {
            var signalFrequencyRange = signal.GetFrequencyRange();
            return _frequencyRanges.Any(range => FrequencyRange.AreIntersected(range, signalFrequencyRange));
        }

        /// <summary>
        /// This method is used only when master sets bandnumber to slave
        /// DO NOT USE THIS METHOD otherwise, concequencies may be inevitable
        /// </summary>
        public void TrySetCurrentObjective(int bandNumber) 
        {
            if(Objectives.Last().Number + 1 == bandNumber)
            {
                BandIndex = 0;
                return;
            }

            for (int i = 0; i < Objectives.Count; i++)
            {
                if (Objectives[i].Number == bandNumber)
                {
                    BandIndex = i;
                    return;
                }
            }
            MessageLogger.Warning($"Couldn't set band number to {bandNumber} during rdf");
        }
    }

    public struct SignalsProcessedEventArg
    {
        public readonly IReadOnlyList<ISignal> Signals;
        public readonly IReadOnlyList<ISignal> RawSignals;
        public readonly IDataScan Scan;

        public SignalsProcessedEventArg(ProcessResult result, IDataScan scan)
        {
            Signals = result.Signals;
            RawSignals = result.RawSignals;
            Scan = scan;
        }
    }

    //todo : to separate file
    public class DirectionFindingTaskSetup
    {
        /// you should use either FrequencyRanges or Objectives, not both
        public IReadOnlyList<FrequencyRange> FrequencyRanges
        {
            private get => _frequencyRanges;
            set
            {
                _frequencyRanges = value.OrderBy(f => f.StartFrequencyKhz).ToArray();
            }
        }

        public IReadOnlyList<Band> Objectives
        {
            private get => _objectives;
            set
            {
                _objectives = value.OrderBy(b => b.Number).ToArray(); 
            }
        }

        public ITaskManager TaskManager;
        public int[] ReceiversIndexes;
        public int Priority = 0;
        public int ScanAveragingCount = Config.Instance.DirectionFindingSettings.DefaultDirectionScanCount;
        public int BearingAveragingCount = Config.Instance.DirectionFindingSettings.DefaultBearingScanCount;
        public bool IsEndless;
        public bool CalculateHighQualityPhases = false;
        public bool AutoUpdateObjectives;
        private IReadOnlyList<FrequencyRange> _frequencyRanges;
        private IReadOnlyList<Band> _objectives;

        public IReadOnlyList<Band> GetObjectives()
        {
            return Objectives ?? FrequencyRange.SplitInBands(FrequencyRanges).ToArray();
        }

        internal IReadOnlyList<FrequencyRange> GetFrequencyRanges()
        {
            return Objectives == null
                ? FrequencyRanges
                : new[] {new FrequencyRange(Objectives[0].FrequencyFromKhz, Objectives.Last().FrequencyToKhz)};
        }
    }
}
