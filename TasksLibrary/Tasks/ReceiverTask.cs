﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataProcessor;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Tasks;
using Settings;
using SharpExtensions;

namespace TasksLibrary.Tasks
{
    public abstract class ReceiverTask<TResult> : IReceiverTask
    {
        public bool IsEndless { get; protected set; }
        public bool IsReady { get; private set; }

        public TResult TypedResult
        {
            get => _result;
            protected set
            {
                _result = value;
                if (!IsEndless)
                {
                    IsReady = true;
                }
                if (!_resultCompletionSource.Task.IsCompleted && !_resultCompletionSource.Task.IsCanceled)
                {
                    _resultCompletionSource.SetResult(value);
                    TaskEndEvent?.Invoke(this, EventArgs.Empty);
                }
            }
        }

        public object Result => _result;

        public int AveragingCount { get; protected set; }
        public int Priority { get; protected set; }
        public IReadOnlyList<Band> Objectives { get; set; }

        protected int BandIndex;

        public IReadOnlyList<IterationTask> IterationTasks => IterationTasksArray;

        public bool AutoUpdateObjectives { get; }

        protected readonly IterationTask[] IterationTasksArray;

        protected IDataAggregator DataAggregator;
        protected readonly IDataProcessor DataProcessor;

        private readonly TaskCompletionSource<object> _resultCompletionSource;
        private TResult _result;

        private readonly int[] _receiversIndexes;

        private Task _previousProcessTask;

        public event EventHandler TaskEndEvent;

        private readonly bool _moveReceiversSynchronously;

        protected ReceiverTask(
            IDataProcessor dataProcessor,
            IReadOnlyList<Band> objectives, 
            int[] receiversIndexes, 
            int averagingCount, 
            bool updateObjectives = false, 
            int priority = 0, 
            bool moveReceiversSynchronously = true)
        {
            if (objectives.IsNullOrEmpty())
            {
                _resultCompletionSource = new TaskCompletionSource<object>();
                return;
            }
            BandIndex = 0;
            _receiversIndexes = receiversIndexes;
            _moveReceiversSynchronously = moveReceiversSynchronously;
            DataAggregator = new DataAggregator(_receiversIndexes, updateObjectives);
            DataProcessor = dataProcessor;
            SetupObjectives(objectives);
            Priority = priority;
            AutoUpdateObjectives = updateObjectives;
            AveragingCount = averagingCount;
            IsReady = false;
            _resultCompletionSource = new TaskCompletionSource<object>();
            IterationTasksArray = receiversIndexes.Select(index => new IterationTask(index, 0)).ToArray();
            UpdateIterationsTasks();
        }

        protected Band CurrentObjective
        {
            get
            {
                BandIndex %= Objectives.Count;
                return Objectives[BandIndex];
            }
        }

        public void Cancel()
        {
            if (!_resultCompletionSource.Task.IsCompleted)
            {
                _resultCompletionSource.SetCanceled();
            }
        }

        protected void SetupObjectives(IReadOnlyList<Band> objectives)
        {
            if (Config.Instance.BandSettings.ReceiverUsrpSetup == ReceiverUsrpSetup.Receiver3GhzUsrp6Ghz)
            {
                Objectives = objectives.Where(b => b.Number < Constants.UsrpFirstBandNumber).ToList();
                if (Objectives.Count == 0) //hack for ursp, because we can have 0 fpga objectives, which leads to inapropriate server behavior
                    Objectives = new List<Band>() { new Band(0) };
            }
            else
            {
                Objectives = objectives;
            }
        }

        public virtual void UpdateObjectives(IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            var newObjectives = FrequencyRange.SplitInBands(frequencyRanges).ToList();
            SetupObjectives(newObjectives);
        }

        public async Task<object> WaitForResult()
        {
            try
            {
                await _resultCompletionSource.Task;
            }
            catch (TaskCanceledException)
            {
                // ignored
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
            }
            return Result;
        }

        protected virtual void UpdateIterationsTasks()
        {
            if (_moveReceiversSynchronously)
            {
                UpdateIterationsTasksSync();
            }
            else
            {
                UpdateIterationsTasksInRow();
            }

            void UpdateIterationsTasksSync()
            {
                for (var i = 0; i < IterationTasksArray.Length; ++i)
                {
                    IterationTasksArray[i].BandNumber = CurrentObjective.Number;
                }
            }

            void UpdateIterationsTasksInRow()
            {
                var defaultBandNumber = Objectives[Objectives.Count - 1].Number + 1;
                for (var i = 0; i < IterationTasksArray.Length; ++i)
                {
                    var bandNumber = defaultBandNumber;
                    if (bandNumber >= Config.Instance.BandSettings.BandCount)
                    {
                        bandNumber = Config.Instance.BandSettings.BandCount - 1;
                    }
                    if (bandNumber >= Constants.UsrpFirstBandNumber 
                        && Config.Instance.BandSettings.ReceiverUsrpSetup == ReceiverUsrpSetup.Receiver3GhzUsrp6Ghz)
                    {
                        bandNumber = Constants.UsrpFirstBandNumber - 1;
                    }
                    if (BandIndex + i < Objectives.Count)
                    {
                        bandNumber = Objectives[BandIndex + i].Number;
                    }
                    else
                    {
                        defaultBandNumber++;
                    }
                    IterationTasksArray[i].BandNumber = bandNumber;
                }
            }
        }

        protected virtual void UpdateBandNumber()
        {
            if (_moveReceiversSynchronously)
            {
                UpdateBandNumbersSync();
            }
            else
            {
                UpdateBandNumbersInRow();
            }
            UpdateIterationsTasks();

            void UpdateBandNumbersSync()
            {
                ++BandIndex;
                if (BandIndex == Objectives.Count)
                {
                    BandIndex = 0;
                }
            }

            void UpdateBandNumbersInRow()
            {
                BandIndex += IterationTasks.Count;
                if (BandIndex >= Objectives.Count)
                {
                    BandIndex = 0;
                }
            }
        }

        internal virtual bool IsDataReadyForProcessing()
        {
            return DataAggregator.DataCount >= AveragingCount;
        }

        public virtual async Task HandleScan(IFpgaDataScan scan)
        {
            DataAggregator.AddScan(scan);
            if (IsDataReadyForProcessing())
            {
                var aggregator = DataAggregator;
                if (_previousProcessTask != null && !_previousProcessTask.IsCompleted)
                {
                    await _previousProcessTask;
                }
                if (IsReady)
                {
                    return;
                }
                var currentObjectives = IterationTasksArray.ToArray();
                _previousProcessTask = Task.Run(() => ProcessData(aggregator, currentObjectives));
                UpdateBandNumber();
                DataAggregator = new DataAggregator(_receiversIndexes, AutoUpdateObjectives);
            }
        }

        protected abstract void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives);
    }
}
