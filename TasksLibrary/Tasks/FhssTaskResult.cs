﻿using System;
using System.Collections.Generic;
using DspDataModel;

namespace TasksLibrary.Tasks
{
    public class FhssTaskResult
    {
        public List<FhssScan> Scans { get; }

        public FhssTaskResult()
        {
            Scans = new List<FhssScan>();
        }

        public void AddScan()
        {
            Scans.Add(new FhssScan(Scans.Count));
        }
    }

    public class FhssScan
    {
        public int ScanNumber { get; }
        public List<(ISignal signal, DateTime time)> Signals { get; }

        public FhssScan(int scanNumber)
        {
            ScanNumber = scanNumber;
            Signals = new List<(ISignal, DateTime)>();
        }
    }
}
