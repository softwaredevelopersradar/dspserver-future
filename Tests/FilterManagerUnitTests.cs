﻿using System.IO;
using System.Linq;
using Settings;
using DspDataModel;
using DspDataModel.Storages;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class FilterManagerUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestFilterManagerCreation()
        {
            var filterManager = new FilterManager();

            Assert.AreEqual(filterManager.Filters.Count, 0);
            Assert.AreEqual(filterManager.ImportantFrequencies.Count, 0);
            Assert.AreEqual(filterManager.KnownFrequencies.Count, 0);
            Assert.AreEqual(filterManager.ForbiddenFrequencies.Count, 0);

            Assert.AreEqual(filterManager.Bands.Count, 0);
            Assert.AreEqual(filterManager.Thresholds.Count, Config.Instance.BandSettings.BandCount);

            for (int i = 0; i < filterManager.Bands.Count; ++i)
            {
                Assert.AreEqual(i, filterManager.Bands[i].Number);
            }

            foreach (var threshold in filterManager.Thresholds)
            {
                Assert.AreEqual(threshold, Constants.DefaultThresholdValue);
            }
        }

        [Test]
        public void TestFilterManagerThresholdUpdate()
        {
            var filterManager = new FilterManager();
            var eventFireCount = 0;
            filterManager.ThresholdsUpdatedEvent += (s, e) => eventFireCount++;
            filterManager.SetThreshold(0, -50);

            Assert.AreEqual(filterManager.GetThreshold(0), -50);
            for (int i = 1; i < filterManager.Thresholds.Count; ++i)
            {
                Assert.AreEqual(filterManager.GetThreshold(i), Constants.DefaultThresholdValue);
            }

            filterManager.SetGlobalThreshold(-60);
            for (int i = 0; i < filterManager.Thresholds.Count; ++i)
            {
                Assert.AreEqual(filterManager.GetThreshold(i), -60);
            }

            Assert.AreEqual(eventFireCount, 2);
        }

        [Test]
        public void TestFilterManagerRadioSourceTypes()
        {
            var filterManager = new FilterManager();
            var eventFireCount = 0;
            filterManager.ForbiddenFrequenciesUpdatedEvent += (s, e) => eventFireCount++;
            filterManager.ImportantFrequenciesUpdatedEvent += (s, e) => eventFireCount++;
            filterManager.KnownFrequenciesUpdatedEvent += (s, e) => eventFireCount++;

            Assert.AreEqual(filterManager.GetRadioSourceType(150), RadioSourceType.Normal);

            filterManager.SetKnownFrequencies(new []{new FrequencyRange(100, 200)});

            Assert.AreEqual(filterManager.GetRadioSourceType(150), RadioSourceType.Known);
            Assert.AreEqual(filterManager.GetRadioSourceType(250), RadioSourceType.Normal);

            filterManager.SetForbiddenFrequencies(new[] { new FrequencyRange(80, 200) });

            Assert.AreEqual(filterManager.GetRadioSourceType(80), RadioSourceType.Forbidden);
            Assert.AreEqual(filterManager.GetRadioSourceType(150), RadioSourceType.Forbidden);
            Assert.AreEqual(filterManager.GetRadioSourceType(250), RadioSourceType.Normal);

            filterManager.SetImportantFrequencies(new[] { new FrequencyRange(150, 250) });

            Assert.AreEqual(filterManager.GetRadioSourceType(150), RadioSourceType.Important);
            Assert.AreEqual(filterManager.GetRadioSourceType(250), RadioSourceType.Important);
            Assert.AreEqual(filterManager.GetRadioSourceType(140), RadioSourceType.Forbidden);
            Assert.AreEqual(filterManager.GetRadioSourceType(350), RadioSourceType.Normal);

            Assert.AreEqual(eventFireCount, 3);
        }

        [Test]
        public void TestFilterManagerFilters()
        {
            var filterManager = new FilterManager();
            var eventFireCount = 0;
            var minFrequency = Constants.FirstBandMinKhz;
            filterManager.FiltersUpdatedEvent += (s, e) => eventFireCount++;
            var filters = new[]
            {
                new Filter(minFrequency + 100, minFrequency + 200, 0, 200),
                new Filter(minFrequency + 100, minFrequency + 200, 0, 200),
                new Filter(minFrequency + Constants.BandwidthKhz * 2 + 100, minFrequency + Constants.BandwidthKhz * 2 + 200, 0, 360),
            };

            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 200));

            filterManager.SetFilters(RangeType.Intelligence, filters.Take(1));

            Assert.IsTrue(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 200));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 250));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 300, direction: 200));
            Assert.IsTrue(filterManager.Filters.Count == 1);
            Assert.IsTrue(filterManager.Bands.Count == 1);

            filterManager.SetFilters(RangeType.Intelligence, filters.Take(2));

            Assert.IsTrue(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 200));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 250));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 300, direction: 200));
            Assert.IsTrue(filterManager.Filters.Count == 2);
            Assert.IsTrue(filterManager.Bands.Count == 1);

            filterManager.SetFilters(RangeType.Intelligence, filters);

            Assert.IsTrue(filterManager.IsInFilter(frequencyKhz: minFrequency + Constants.BandwidthKhz * 2 + 100, direction: 200));
            Assert.IsTrue(filterManager.Filters.Count == 3);
            Assert.IsTrue(filterManager.Bands.Count == 2);

            filterManager.SetFilters(RangeType.Intelligence, filters.Skip(1));

            Assert.IsTrue(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 200));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 250));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 300, direction: 200));
            Assert.IsTrue(filterManager.Filters.Count == 2);
            Assert.IsTrue(filterManager.Bands.Count == 2);

            filterManager.SetFilters(RangeType.Intelligence, new[] { new Filter(minFrequency + Constants.BandwidthKhz * 2 + 100, minFrequency + Constants.BandwidthKhz * 2 + 200, 0, 360) });

            Assert.IsTrue(filterManager.IsInFilter(frequencyKhz: minFrequency + Constants.BandwidthKhz * 2 + 100, direction: 200));
            Assert.IsTrue(filterManager.Filters.Count == 1);
            Assert.IsTrue(filterManager.Bands.Count == 1);

            filterManager.SetFilters(RangeType.Intelligence, new Filter[0]);

            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 100, direction: 250));
            Assert.IsFalse(filterManager.IsInFilter(frequencyKhz: minFrequency + 300, direction: 200));
            Assert.IsTrue(filterManager.Filters.Count == 0);
            Assert.IsTrue(filterManager.Bands.Count == 0);

            Assert.AreEqual(eventFireCount, 6);
        }
    }
}
