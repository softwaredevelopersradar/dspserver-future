﻿using System;
using System.IO;
using System.Linq;
using DspDataModel.Data;
using NUnit.Framework;
using SharpExtensions;

namespace Tests
{
    [TestFixture]
    public class DataExtensionsUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestGetPhaseDifferencies()
        {
            var phases = new float[] {100, 80, 60, 40, 20};
            var phaseDifferencies = phases.GetPhaseDifferences();
            var expectedDifferencies = new float[] { 20, 40, 60, 80, 20, 40, 60, 20, 40, 20 };

            Assert.AreEqual(phaseDifferencies.Length, expectedDifferencies.Length);
            Assert.IsTrue(phaseDifferencies.SequenceEqual(expectedDifferencies));
        }

        [Test]
        public void TestGetAmplitude()
        {
            var band = new AmplitudeBand(new[] {0f, -100f});
            var bands = new[] {band};

            for (int i = 0; i < band.Count; ++i)
            {
                Assert.IsTrue(bands.GetAmplitude(i).ApproxEquals(band.Amplitudes[i]));
            }

            bands = new[] { band, band, band };

            for (int i = 0; i < band.Count; ++i)
            {
                Assert.IsTrue(bands.GetAmplitude(i).ApproxEquals(band.Amplitudes[i]));
            }

            var band2 = new AmplitudeBand(new [] {0f, -50});
            bands = new[] { band, band2 };

            Assert.IsTrue(bands.GetAmplitude(0).ApproxEquals(band.Amplitudes[0]));
            Assert.IsTrue(bands.GetAmplitude(1).ApproxEquals(-50f));
        }

        [Test]
        public void TestMergeSpectrum()
        {
            var now = DateTime.UtcNow;

            var scan = new AmplitudeScan(new[] {0f, -150}, 1, now, 0);
            var scans = new[] {scan};
            var result = scans.MergeSpectrum();

            Assert.AreEqual(result.BandNumber, scan.BandNumber);
            for (int i = 0; i < scan.Count; ++i)
            {
                Assert.IsTrue(scan.Amplitudes[i].ApproxEquals(result.Amplitudes[i]));
            }

            var scan2 = new AmplitudeScan(new[] { 0f, -50 }, 1, now, 0);
            scans = new[] {scan, scan2};
            result = scans.MergeSpectrum();

            Assert.AreEqual(result.BandNumber, scan.BandNumber);
            Assert.IsTrue(result.Amplitudes[0].ApproxEquals(scan2.Amplitudes[0]));
            Assert.IsTrue(result.Amplitudes[1].ApproxEquals(-50f));
        }
    }
}
