﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DataProcessor;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using Settings;
using NUnit.Framework;
using Simulator;

namespace Tests
{
    [TestFixture]
    public class DataProcessorTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestDataProcessor()
        {
            var dataProcessor = new DataProcessor.DataProcessor();
            var simulator = new SignalSimulator
            {
                NoiseLevel = -90,
                FuzzinessLevel = 0
            };

            var scan = GetDataScan(simulator, 0);
            var processorConfig = ScanProcessConfig.CreateConfigWithoutDf(threshold: -60);
            Assert.AreEqual(dataProcessor.GetSignals(scan, processorConfig).Signals.Count, 0);
        }

        private static IDataScan GetDataScan(SignalSimulator simulator, int bandNumber, IEnumerable<Simulator.Signal> signals = null)
        {
            simulator.Signals.Clear();
            if (signals != null)
            {
                foreach (var signal in signals)
                {
                    simulator.Signals.Add(signal);
                }
            }

            var response = simulator.GetResponse(Enumerable.Repeat((byte) bandNumber, Constants.ReceiversCount).ToArray());
            var dateTime = DateTime.Now;

            var fpgaScan = new FpgaDataScan(response.Channels.Select(
                c => new ReceiverScan(
                    c.Amplitudes.Select(a => a + Constants.ReceiverMinAmplitude).ToArray(),
                    c.Phases.Select(p => 0.1f * p).ToArray(),
                    bandNumber,
                    dateTime,
                    0
                ) as IReceiverScan).ToArray());

            var aggregator = new DataAggregator(Enumerable.Range(0, Constants.ReceiversCount).ToArray(), false);
            aggregator.AddScan(fpgaScan);

            return aggregator.GetDataScan();
        }

        private struct SignalInfo
        {
            public SignalInfo(float frequency, float bandwidth, float direction)
            {
                Frequency = frequency;
                Bandwidth = bandwidth;
                Direction = direction;
            }

            private float Frequency { get; }
            private float Bandwidth { get; }
            private float Direction { get; }
        }
    }
}
