﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.LinkedStation;
using DspDataModel.Server;
using DspDataModel.Tasks;
using NUnit.Framework;
using Protocols;
using Simulator;

namespace Tests
{
    [TestFixture]
    public class LinkedStationTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        private ClientServerPair _pairClient;
        private ClientServerPair _pairServer;

        [SetUp]
        public async Task Initialize()
        {
            var serverPort = Config.Instance.ServerSettings.ServerPort;
            var simulatorPort = Config.Instance.SimulatorSettings.SimulatorPort;

            (_pairClient, _pairServer) = await Utils.CreateClientAndServer2(serverPort, simulatorPort, serverPort + 1, simulatorPort + 1);

            //Assert.IsTrue(_pairServer.Server.Controller.StartLinkedStationServer("127.0.0.1:9108"));
            //Assert.IsTrue(_pairClient.Server.Controller.ConnectToLinkedStation("127.0.0.1:9108"));
        }

        // executed after each test
        [TearDown]
        public async Task TearDown()
        {
            await Task.WhenAll(_pairClient.Stop(), _pairServer.Stop());
        }

        [Test]
        public async Task TestConnection()
        {
           await Task.Delay(100);

            //Assert.IsTrue(_pairClient.Server.Controller.LinkedStationManager.Connected);
            //Assert.IsTrue(_pairServer.Server.Controller.LinkedStationManager.Connected);
            
            //_pairClient.Server.Controller.LinkedStationManager.Stop();
            //_pairServer.Server.Controller.LinkedStationManager.Stop();

            await _pairClient.Client.SetStationRole(StationRole.Standalone, 0, 0);
            await _pairServer.Client.SetStationRole(StationRole.Standalone, 0, 0);
            var host = Config.Instance.ServerSettings.ServerHost;
            var address = host + ":9108";

            Assert.AreEqual(await _pairClient.Client.ConnectToLinkedStation(address), RequestResult.LinkedStationConnectionError);
            Assert.AreEqual(await _pairServer.Client.ConnectToLinkedStation(address), RequestResult.LinkedStationConnectionError);

            Assert.AreEqual(await _pairClient.Client.SetStationRole(StationRole.Slave, 2, 1), RequestResult.Ok);
            Assert.AreEqual(await _pairServer.Client.SetStationRole(StationRole.Master, 1, 2), RequestResult.Ok);

            Assert.AreEqual(await _pairServer.Client.ConnectToLinkedStation(address), RequestResult.Ok);

            Assert.AreEqual(await _pairServer.Client.DisconnectFromLinkedStation(), RequestResult.Ok);
        }

        [Test]
        public async Task TestSettingSectorsAndRanges()
        {
            var isEventFired = false;
            var inputFilter = new Filter(250_000, 350_000, 100, 350);
            var inputRangeSector = new RangeSector(250_000_0, 350_000_0, 100_0, 350_0);

            _pairClient.Client.SectorsAndRangesUpdateEvent += (sender, filters) =>
            {
                isEventFired = true;
                Assert.AreEqual(1, filters.Length);
                var filter = filters.First();

                Assert.AreEqual(filter, inputRangeSector);
            };

            var result = await _pairServer.Client.SetSectorsAndRanges(RangeType.Intelligence, TargetStation.Linked, new[] {inputRangeSector});

            Assert.AreEqual(RequestResult.Ok, result);
            Assert.IsTrue(isEventFired);
            
            Assert.AreEqual(1, _pairClient.Server.TaskManager.FilterManager.Filters.Count);
            Assert.AreEqual(inputFilter, _pairClient.Server.TaskManager.FilterManager.Filters[0]);
        }

        [Test]
        public async Task TestSendingSignals()
        {
            var clientSignals = new List<DataProcessor.Signal>
            {
                new DataProcessor.Signal(35_000, -5, 100, 10),
                new DataProcessor.Signal(36_000, -10, 320, 15)
            };
            var serverSignals = new List<DataProcessor.Signal>
            {
                new DataProcessor.Signal(32_000, -7, 110, 12),
                new DataProcessor.Signal(35_000, -12, 300, 25)
            };

            var clientEventFiredCount = 0;
            var serverEventFiredCount = 0;

            //_pairClient.Server.Controller.LinkedStationManager.SignalsReceivedEvent += 
            //    (_, signals) => OnSignalsReceived(ref clientEventFiredCount, signals, serverSignals);

            //_pairServer.Server.Controller.LinkedStationManager.SignalsReceivedEvent += 
            //    (_, signals) => OnSignalsReceived(ref serverEventFiredCount, signals, clientSignals);

            //Assert.IsTrue(_pairClient.Server.Controller.LinkedStationManager.SendSignals(clientSignals));
            //Assert.IsTrue(_pairServer.Server.Controller.LinkedStationManager.SendSignals(serverSignals));

            await Task.Delay(500);

            Assert.AreEqual(clientEventFiredCount, 1);
            Assert.AreEqual(serverEventFiredCount, 1);

            clientSignals.RemoveAt(0);
            serverSignals.RemoveAt(0);

            //Assert.IsTrue(_pairClient.Server.Controller.LinkedStationManager.SendSignals(clientSignals));
            //Assert.IsTrue(_pairServer.Server.Controller.LinkedStationManager.SendSignals(serverSignals));

            await Task.Delay(500);
            Assert.AreEqual(clientEventFiredCount, 2);
            Assert.AreEqual(serverEventFiredCount, 2);

            void OnSignalsReceived(ref int eventFiredCount, IReadOnlyCollection<ISignal> signals, IReadOnlyList<ISignal> inputSignals)
            {
                Assert.AreEqual(signals.Count, inputSignals.Count);
                for (var i = 0; i < inputSignals.Count; ++i)
                {
                    var s1 = inputSignals[i];
                    var s2 = signals.ElementAt(i);
                    Assert.AreEqual(s1.FrequencyKhz, s2.FrequencyKhz, delta: 1);
                    Assert.AreEqual(s1.Amplitude, s2.Amplitude, delta: 1);
                    Assert.AreEqual(s1.BandwidthKhz, s2.BandwidthKhz, delta: 1);
                    Assert.AreEqual(s1.Direction, s2.Direction, delta: 1);
                }
                Interlocked.Increment(ref eventFiredCount);
            }
        }

        [Test]
        public async Task TestSignalsSharing()
        {
            var serverSignal = new Signal(35_000, 10, 20, -40);
            var clientSignal = new Signal(42_000, 10, 160, -50);

            _pairServer.SimulatorCore.SignalSimulator.Signals.Add(serverSignal);
            _pairClient.SimulatorCore.SignalSimulator.Signals.Add(clientSignal);

            await _pairServer.Client.SetSectorsAndRanges(RangeType.Intelligence, TargetStation.Current, new[] {new RangeSector(25_000_0, 85_000_0, 0, 360_0)});
            await _pairServer.Client.SetSectorsAndRanges(RangeType.Intelligence, TargetStation.Linked,  new[] {new RangeSector(25_000_0, 85_000_0, 0, 360_0)});
            await _pairServer.Client.SetMode(DspServerMode.RadioIntelligenceWithDf);

            // wait some time until signals are placed into radiosignal table
            // right there should be no shared signals
            await Task.Delay(1000);

            var serverSignals = await _pairServer.Client.GetRadioSources();
            Assert.AreEqual(1, serverSignals.Length);
            CompareSignals(serverSignal, serverSignals[0]);

            var clientSignals = await _pairClient.Client.GetRadioSources();
            Assert.AreEqual(1, clientSignals.Length);
            CompareSignals(clientSignal, clientSignals[0]);

            // add some signal to share (second direction should be filled in)
            var clientSignal2 = new Signal(35_000, 10, 50, -40);
            _pairClient.SimulatorCore.SignalSimulator.Signals.Add(clientSignal2);

            await Task.Delay(1000);

            clientSignals = await _pairClient.Client.GetRadioSources();
            //Assert.AreEqual(serverSignal.Direction, clientSignals[0].Direction2 * 0.1f, delta: 2);
            Assert.AreEqual(2, clientSignals.Length);

            serverSignals = await _pairServer.Client.GetRadioSources();
            Assert.AreEqual(1, serverSignals.Length);
            //Assert.AreEqual(clientSignal2.Direction, serverSignals[0].Direction2 * 0.1f, delta: 2);
            CompareSignals(serverSignal, serverSignals[0]);

            void CompareSignals(Signal s, RadioSource r)
            {
                Assert.AreEqual(s.FrequencyKhz, r.Frequency * 0.1f, delta: 3);
                Assert.AreEqual(s.Amplitude, -r.Amplitude, delta: 1);
                Assert.AreEqual(s.BandwidthKhz, r.Bandwidth * 0.1f, delta: 1);
                Assert.AreEqual(s.Direction, r.Direction * 0.1f, delta: 1);
            }
        }

        [Test]
        public async Task TestTextMessage()
        {
            var texts = new[] {"test text 1", "hello!"};
            var eventFlags = new[] {0, 0};
            _pairServer.Client.TextReceivedEvent += (sender, text) =>
            {
                Assert.AreEqual(text, texts[0]);
                eventFlags[0]++;
                Assert.AreEqual(1, eventFlags[0]);
            };

            _pairClient.Client.TextReceivedEvent += (sender, text) =>
            {
                Assert.AreEqual(text, texts[1]);
                eventFlags[1]++;
                Assert.AreEqual(1, eventFlags[1]);
            };
            _pairClient.Client.Client.AutoReconnectEnabled = false;

            await TestWithConnection();

            // client stoped
            //_pairClient.Server.Controller.LinkedStationManager.Stop();

            Assert.AreEqual(await _pairClient.Client.SendText(texts[0]), RequestResult.LinkedStationConnectionError);
            Assert.AreEqual(await _pairServer.Client.SendText(texts[1]), RequestResult.LinkedStationConnectionError);

            // reconnecting
            //Assert.IsTrue(_pairClient.Server.Controller.ConnectToLinkedStation("127.0.0.1:9108"));

            //while (!_pairServer.Server.Controller.LinkedStationManager.Connected)
            //{
            //    await Task.Delay(10);
            //}

            //await TestWithConnection();

            //// server stoped
            //_pairServer.Server.Controller.LinkedStationManager.Stop();

            //Assert.AreEqual(await _pairClient.Client.SendText(texts[0]), RequestResult.LinkedStationConnectionError);
            //Assert.AreEqual(await _pairServer.Client.SendText(texts[1]), RequestResult.LinkedStationConnectionError);

            //// reconnecting
            ////Assert.IsTrue(_pairServer.Server.Controller.StartLinkedStationServer("127.0.0.1:9108"));
            ////Assert.IsTrue(_pairClient.Server.Controller.ConnectToLinkedStation("127.0.0.1:9108"));

            //while (!_pairServer.Server.Controller.LinkedStationManager.Connected)
            //{
            //    await Task.Delay(10);
            //}

            await TestWithConnection();

            async Task TestWithConnection()
            {
                eventFlags = new[] {0, 0};
                Assert.AreEqual(await _pairClient.Client.SendText(texts[0]), RequestResult.Ok);
                Assert.AreEqual(await _pairServer.Client.SendText(texts[1]), RequestResult.Ok);

                while (eventFlags[0] == 0 || eventFlags[1] == 0)
                {
                    await Task.Delay(50);
                }
            }
        }

        [Test]
        public async Task TestSendCoordinates()
        {
            var coordinates = new[]
            {
                (23.5f, 54.6f, 322),
                (33.0f, 57.9f, 233)
            };
            var eventFlags = new[] { false, false };
            //_pairServer.Server.Controller.LinkedStationManager.CoordinatesReceivedEvent += (sender, message) =>
            //{
            //    Assert.AreEqual(message.Item1.Latitude, coordinates[0].Item1, 1e-3);
            //    Assert.AreEqual(message.Item1.Longitude, coordinates[0].Item2, 1e-3);
            //    Assert.AreEqual(message.Item1.Altitude, coordinates[0].Item3, 1e-3);
            //    eventFlags[0] = true;
            //};

            //_pairClient.Server.Controller.LinkedStationManager.CoordinatesReceivedEvent += (sender, message) =>
            //{
            //    Assert.AreEqual(message.Item1.Latitude, coordinates[1].Item1, 1e-3);
            //    Assert.AreEqual(message.Item1.Longitude, coordinates[1].Item2, 1e-3);
            //    Assert.AreEqual(message.Item1.Altitude, coordinates[1].Item3, 1e-3);
            //    eventFlags[1] = true;
            //};
            _pairClient.Client.Client.AutoReconnectEnabled = false;

            Assert.AreEqual(await _pairClient.Client.SetStationLocation(coordinates[0].Item1, coordinates[0].Item2, coordinates[0].Item3, true, TargetStation.Current), RequestResult.Ok);
            Assert.AreEqual(await _pairServer.Client.SetStationLocation(coordinates[1].Item1, coordinates[1].Item2, coordinates[1].Item3, true, TargetStation.Current), RequestResult.Ok);

            while (!eventFlags[0] || !eventFlags[1])
            {
                await Task.Delay(50);
            }
        }

        [Test]
        public async Task TestExecutiveDfRequest()
        {
            //Assert.AreEqual(await _pairServer.Server.Controller.LinkedStationManager.PerformExecutiveDf(25_000, 35_000, 5, 5), null);

            //_pairClient.SimulatorCore.SignalSimulator.Signals.Add(new Signal(30_000, 10, 50, -30));

            //var direction = await _pairServer.Server.Controller.LinkedStationManager.PerformExecutiveDf(25_000, 35_000, 5, 5);
            //Assert.IsTrue(direction != null);
            //Assert.AreEqual(direction.Value, 50f, 2);
        }

        [Test]
        public async Task TestQuasiSimulataneousRequest()
        {
            const int phaseAveragingCount = 3;
            const int directionAveragingCount = 3;

            await _pairClient.Client.SetStationLocation(-1, -1, -1, true, TargetStation.Current);
            await _pairServer.Client.SetStationLocation(-1, -1, -1, true, TargetStation.Current);

            // test without signals
            Assert.IsTrue(await _pairClient.Client.QuasiSimultaneousDfRequest(29_000, 31_000, phaseAveragingCount, directionAveragingCount) == null);
            Assert.IsTrue(await _pairServer.Client.QuasiSimultaneousDfRequest(29_000, 31_000, phaseAveragingCount, directionAveragingCount) == null);

            _pairServer.SimulatorCore.SignalSimulator.Signals.Add(new Signal(30_000, 1, 17, -20));

            // test with signal visible only for server
            var signal = await _pairServer.Client.QuasiSimultaneousDfRequest(29_000, 31_000, phaseAveragingCount, directionAveragingCount);

            Assert.IsTrue(signal != null);
            Assert.AreEqual(signal.Value.Frequency * 0.1f, 30_000, 3);
            Assert.AreEqual(signal.Value.Direction * 0.1f, 17, 2);
            //Assert.AreEqual(signal.Value.Direction2, -1);
            Assert.AreEqual(signal.Value.Latitude, -1, 1e-3);
            Assert.AreEqual(signal.Value.Longitude, -1, 1e-3);

            // test with signal visible for both stations
            _pairClient.SimulatorCore.SignalSimulator.Signals.Add(new Signal(30_000, 1, 249, -20));

            signal = await _pairServer.Client.QuasiSimultaneousDfRequest(29_000, 31_000, phaseAveragingCount, directionAveragingCount);

            Assert.IsTrue(signal != null);
            Assert.AreEqual(signal.Value.Frequency * 0.1f, 30_000, 3);
            Assert.AreEqual(signal.Value.Direction * 0.1f, 17, 2);
            //Assert.AreEqual(signal.Value.Direction2 * 0.1, 249, 2);
            Assert.AreEqual(signal.Value.Latitude, -1, 1e-3);
            Assert.AreEqual(signal.Value.Longitude, -1, 1e-3);

            await _pairServer.Client.SetStationLocation(53.881244, 27.606453, -1, true, TargetStation.Current);
            await _pairClient.Client.SetStationLocation(53.9218687, 27.6799367, -1, true, TargetStation.Current);

            // test with coordinates
            signal = await _pairServer.Client.QuasiSimultaneousDfRequest(29_000, 31_000, phaseAveragingCount, directionAveragingCount);

            Assert.IsTrue(signal != null);
            Assert.AreEqual(signal.Value.Frequency * 0.1f, 30_000, 3);
            Assert.AreEqual(signal.Value.Direction * 0.1f, 17, 2);
            //Assert.AreEqual(signal.Value.Direction2 * 0.1, 249, 2);
            Assert.AreEqual(signal.Value.Latitude, 53.913369, 1e-2);
            Assert.AreEqual(signal.Value.Longitude, 27.630211, 1e-2);
        }
    }
}
