﻿using System.Threading.Tasks;
using DspDataModel;
using NUnit.Framework;
using Simulator;

namespace Tests
{
    internal static class Utils
    {
        internal static void CreateClientAndServer(out DspClient.DspClient client, out DspServer.DspServer server, out SimulatorCore simulator)
        {
            simulator = new SimulatorCore(saveSimulatedSignals: false);
            simulator.Server.Start(Config.Instance.SimulatorSettings.SimulatorHost, Config.Instance.SimulatorSettings.SimulatorPort);
            server = new DspServer.DspServer();
            server.Start();
            client = new DspClient.DspClient();
        }

        internal static async Task<(ClientServerPair, ClientServerPair)> CreateClientAndServer2(int serverPort1, int simulatorPort1, int serverPort2, int simulatorPort2)
        {
            var pairs = await Task.WhenAll(
                ClientServerPair.Create(serverPort1, simulatorPort1),
                ClientServerPair.Create(serverPort2, simulatorPort2));

            //pairs[0].Server.Controller.LinkedStationManager.SetupAddresses(1, 2);
            //pairs[1].Server.Controller.LinkedStationManager.SetupAddresses(2, 1);

            return (pairs[0], pairs[1]);
        }

        internal static async Task StopClientAndServer(DspClient.DspClient client, DspServer.DspServer server, SimulatorCore simulator)
        {
            client.Stop();
            await Task.WhenAll(server.Stop(), simulator.Stop());
        }

        internal static async Task Connect(DspClient.DspClient client)
        {
            Assert.IsTrue(await client.Connect(Config.Instance.ServerSettings.ServerHost, Config.Instance.ServerSettings.ServerPort));
        }
    }
}
