﻿using System;
using System.IO;
using System.Threading.Tasks;
using Correlator;
using DataProcessor;
using DataStorages;
using DspDataModel;
using NUnit.Framework;
using Phases;
using Settings;
using SharpExtensions;

namespace Tests
{
    [TestFixture]
    public class RadioSourceUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public async Task TestRadioSourceCreation()
        {
            var correlator = new PhaseCorrelator(new RadioPathTable());

            var signal = new Signal(30000, 30000, 230, 1, 10, -100, 0, 0, 0, 1, new float[Constants.PhasesDifferencesCount], SignalModulation.Unknown, TimeSpan.Zero);
            var radioSource = new RadioSource(signal, 100, TimeSpan.MaxValue);

            Assert.IsTrue(radioSource.IsSameSource(signal));

            Assert.IsTrue(radioSource.IsNew);
            Assert.IsTrue(radioSource.IsActive);

            Assert.IsTrue(radioSource.Amplitude.ApproxEquals(signal.Amplitude));
            Assert.IsTrue(radioSource.BandwidthKhz == signal.BandwidthKhz);
            Assert.IsTrue(radioSource.FrequencyKhz == signal.FrequencyKhz);
            Assert.IsTrue(radioSource.CentralFrequencyKhz == signal.CentralFrequencyKhz);
            Assert.IsTrue(radioSource.Direction.ApproxEquals(signal.Direction));
            Assert.IsTrue(radioSource.DiscardedDirectionsPart.ApproxEquals(signal.DiscardedDirectionsPart));
            Assert.IsTrue(radioSource.Reliability.ApproxEquals(signal.Reliability));

            Assert.AreEqual(correlator.GetCorrelationCurve(radioSource), correlator.GetCorrelationCurve(signal));

            await Task.Delay(Config.Instance.StoragesSettings.NewSignalTimeSpan + TimeSpan.FromMilliseconds(500));

            radioSource.Update();
            Assert.IsFalse(radioSource.IsNew);
            Assert.IsFalse(radioSource.IsActive);
        }

        [Test]
        public async Task TestRadioSourceUpdate()
        {
            const int direction = 230;
            var signal = new Signal(35000, 35000, direction, 1, 10, -100, 0, 0, 0, 1, new float[Constants.PhasesDifferencesCount], SignalModulation.Unknown, TimeSpan.Zero);
            var radioSource = new RadioSource(signal, 100, TimeSpan.MaxValue);
            var config = Config.Instance;
            await Task.Delay(config.StoragesSettings.NewSignalTimeSpan + TimeSpan.FromMilliseconds(500));

            radioSource.Update();
            Assert.IsFalse(radioSource.IsNew);
            Assert.IsFalse(radioSource.IsActive);

            var direction2 = direction + config.DirectionFindingSettings.SignalsMergeDirectionDeviation - 1;
            var signal2 = new Signal(35000, 35000, direction2, 1, 10, -100, 0, 0, 0, 1, new float[Constants.PhasesDifferencesCount], SignalModulation.Unknown, TimeSpan.Zero);
            Assert.IsTrue(radioSource.IsSameSource(signal2));
            radioSource.Update(signal2);

            Assert.IsFalse(radioSource.IsNew);
            Assert.IsTrue(radioSource.IsActive);
            Assert.IsTrue(radioSource.Direction.ApproxEquals(PhaseMath.AveragePhase(new[] {direction, direction2})));

            // same signal but with different direction, should be "not the same source"
            var signal3 = new Signal(35000, 35000, direction2 + config.DirectionFindingSettings.SignalsMergeDirectionDeviation, 1, 10, -100, 0, 0, 0, 1, new float[Constants.PhasesDifferencesCount], SignalModulation.Unknown, TimeSpan.Zero);
            Assert.IsFalse(radioSource.IsSameSource(signal3));
        }
    }
}

