﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace DatabaseLibrary
{
    public class DatabaseControllerMock : IDatabaseController
    {
        public bool IsConnected { get; private set; }
        public StationRole Role { get; set; } = StationRole.Standalone;
        public int OwnStationId { get; }
        public IReadOnlyList<int> LinkedStationsId { get; }
    
        public event EventHandler<IEnumerable<Filter>> IntelligenceFiltersReceivedEvent;
        public event EventHandler<IEnumerable<Filter>> RadioJamFiltersReceivedEvent;
        public event EventHandler<IReadOnlyList<IRadioJamTarget>> FrsJamTargetsReceivedEvent;
        public event EventHandler<IReadOnlyList<IRadioJamFhssTarget>> FhssJamTargetsReceivedEvent;
        public event EventHandler<JammingSettings> JammingSettingsReceived;
        public event EventHandler<IReadOnlyList<FrequencyRange>> KnownRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ImportantRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesReceivedEvent;
        public event EventHandler StopInfiniteJamming;

        public void RadioSourceTableAddRange(IEnumerable<IRadioSource> radioSources)
        { }

        public void ClearRadioSourceTable()
        { }

        public async Task Connect()
        {
            IsConnected = true;
        }

        public void Disconnect()
        {
            IsConnected = false;
        }

        public void ClearRadioJamStateTables()
        {}

        public void UpdateFrsRadioJamState(IReadOnlyList<IRadioJamTarget> targets)
        {}

        public void UpdateFhssRadioJamState(IReadOnlyList<(int id, bool isJammed)> targets, IReadOnlyList<float> jammedFrequencies)
        {}

        public void UpdateGpsCoordinates(IGpsReceiver gpsReceiver)
        {}

        public void UpdateFrsJamTarget(IRadioJamTarget target)
        {}

        public void FhssTableAddRange(IEnumerable<IFhssNetwork> fhssNetworks)
        {}

        public void UpdateStationMode(DspServerMode mode)
        {}

        public void DeleteRadioSourceRecord(int id)
        {}

        public void FhssTableClear()
        {}
    }
}
