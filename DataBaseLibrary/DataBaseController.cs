﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Bearing;
using ClientDataBase;
using DataStorages;
using DspDataModel;
using DspDataModel.Database;
using DspDataModel.Hardware;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using InheritorsEventArgs;
using ModelsTablesDBLib;
using Settings;

namespace DatabaseLibrary
{
    public class DatabaseController : IDatabaseController
    {
        public bool IsConnected { get; private set; }

        public event EventHandler<IEnumerable<Filter>> IntelligenceFiltersReceivedEvent;
        public event EventHandler<IEnumerable<Filter>> RadioJamFiltersReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> KnownRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ImportantRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenRangesReceivedEvent;
        public event EventHandler<IReadOnlyList<IRadioJamTarget>> FrsJamTargetsReceivedEvent;
        public event EventHandler<IReadOnlyList<IRadioJamFhssTarget>> FhssJamTargetsReceivedEvent;
        public event EventHandler<JammingSettings> JammingSettingsReceived;
        public event EventHandler StopInfiniteJamming;

        private readonly DatabaseConfig _config;
        private int _reconnectionAttemptsCount;
        private const int _sliceSize = 500;

        private readonly ClientDB _client;
        private readonly IClassTables IntelligenceFiltersTable;
        private readonly IClassTables JamFiltersTable;
        private readonly IClassTables ImportantFrequenciesTable;
        private readonly IClassTables KnownFrequenciesTable;
        private readonly IClassTables ForbiddenFrequenciesTable;
        private readonly IClassTables RadiosourceTable;
        private readonly IClassTables FhssNetworksTable;
        private readonly IClassTables FhssSourcesTable;
        private readonly IClassTables FrsJamTargetsTable;
        private readonly IClassTables FrsJamStateTable;
        private readonly IClassTables FhssJamTargetsTable;
        private readonly IClassTables FhssForbiddenFrequenciesTable; //everytime we load it, since i don't want to / sure that I want to keep them
        private readonly IClassTables FhssJamStateTable;
        private readonly IClassTables LinkedStationsTable;
        private readonly IClassTables GlobalPropertiesTable;
        private readonly IClassTables GpsTable;

        /// <summary>
        /// number of tickets equals number of operations with database, usually two (clear and write)
        /// </summary>
        private readonly List<Ticket> _tickets = new List<Ticket>(5);
        private readonly object _reconnectLockObject = new object();

        private TableASP _ownStationAsp;

        public DatabaseController()
        {
            _config = Config.Instance.DatabaseSettings;
            _client = new ClientDB("DspServer", $"{_config.DatabaseHost}:{_config.DatabasePort}");
            _reconnectionAttemptsCount = _config.ReconnectionAttemptsCount;
            
            GlobalPropertiesTable = _client.Tables[NameTable.GlobalProperties];
            RadiosourceTable = _client.Tables[NameTable.TempFWS];
            FhssNetworksTable = _client.Tables[NameTable.TableReconFHSS];
            FhssSourcesTable = _client.Tables[NameTable.TableSourceFHSS];
            IntelligenceFiltersTable = _client.Tables[NameTable.TableSectorsRangesRecon];
            JamFiltersTable = _client.Tables[NameTable.TableSectorsRangesSuppr];
            KnownFrequenciesTable = _client.Tables[NameTable.TableFreqKnown];
            ImportantFrequenciesTable = _client.Tables[NameTable.TableFreqImportant];
            ForbiddenFrequenciesTable = _client.Tables[NameTable.TableFreqForbidden];
            FrsJamTargetsTable = _client.Tables[NameTable.TableSuppressFWS];
            FrsJamStateTable = _client.Tables[NameTable.TempSuppressFWS];
            FhssJamTargetsTable = _client.Tables[NameTable.TableSuppressFHSS];
            FhssForbiddenFrequenciesTable = _client.Tables[NameTable.TableFHSSExcludedFreq];
            FhssJamStateTable = _client.Tables[NameTable.TempSuppressFHSS];
            LinkedStationsTable = _client.Tables[NameTable.TableASP];
            GpsTable = _client.Tables[NameTable.TempGNSS];
        }

        private void SubscribeToDatabaseEvents()
        {
            _client.OnConnect += OnConnect;
            _client.OnDisconnect += OnDisconnect;
            _client.OnErrorDataBase += OnError;

            (FrsJamTargetsTable as ITableUpdate<TableSuppressFWS>).OnUpTable += OnFrsJamTargetsReceived;
            (FhssJamTargetsTable as ITableUpdate<TableSuppressFHSS>).OnUpTable += OnFhssJamTargetsReceived;
            (FhssForbiddenFrequenciesTable as ITableUpdate<TableFHSSExcludedFreq>).OnUpTable += OnFhssForbiddenFrequenciesReceived;
            (IntelligenceFiltersTable as ITableUpdate<TableSectorsRangesRecon>).OnUpTable += OnIntelligenceFiltersReceived;
            (JamFiltersTable as ITableUpdate<TableSectorsRangesSuppr>).OnUpTable += OnJammingFiltersReceived;
            (LinkedStationsTable as ITableUpdate<TableASP>).OnUpTable += OnLinkedStationReceived;
            (GlobalPropertiesTable as ITableUpdate<GlobalProperties>).OnUpTable += OnGlobalPropertiesReceived;
            (ImportantFrequenciesTable as ITableUpdate<TableFreqImportant>).OnUpTable += OnImportantFrequenciesReceived;
            (ForbiddenFrequenciesTable as ITableUpdate<TableFreqForbidden>).OnUpTable += OnForbiddenFrequenciesReceived;
            (KnownFrequenciesTable as ITableUpdate<TableFreqKnown>).OnUpTable += OnKnownFrequenciesReceived;
        }

        private void UnsubscribeFromDatabaseEvents()
        {
            _client.OnConnect -= OnConnect;
            _client.OnDisconnect -= OnDisconnect;
            _client.OnErrorDataBase -= OnError;

            (FrsJamTargetsTable as ITableUpdate<TableSuppressFWS>).OnUpTable -= OnFrsJamTargetsReceived;
            (FhssJamTargetsTable as ITableUpdate<TableSuppressFHSS>).OnUpTable -= OnFhssJamTargetsReceived;
            (FhssForbiddenFrequenciesTable as ITableUpdate<TableFHSSExcludedFreq>).OnUpTable -= OnFhssForbiddenFrequenciesReceived;
            (IntelligenceFiltersTable as ITableUpdate<TableSectorsRangesRecon>).OnUpTable -= OnIntelligenceFiltersReceived;
            (JamFiltersTable as ITableUpdate<TableSectorsRangesSuppr>).OnUpTable -= OnJammingFiltersReceived;
            (LinkedStationsTable as ITableUpdate<TableASP>).OnUpTable -= OnLinkedStationReceived;
            (GlobalPropertiesTable as ITableUpdate<GlobalProperties>).OnUpTable -= OnGlobalPropertiesReceived;
            (ImportantFrequenciesTable as ITableUpdate<TableFreqImportant>).OnUpTable -= OnImportantFrequenciesReceived;
            (ForbiddenFrequenciesTable as ITableUpdate<TableFreqForbidden>).OnUpTable -= OnForbiddenFrequenciesReceived;
            (KnownFrequenciesTable as ITableUpdate<TableFreqKnown>).OnUpTable -= OnKnownFrequenciesReceived;
        }

        private void OnConnect(object sender, ClientEventArgs args)
        {
            IsConnected = true;
            _reconnectionAttemptsCount = _config.ReconnectionAttemptsCount;
            MessageLogger.Log($"Connected to database {_config.DatabaseHost}:{_config.DatabasePort}");
            LoadAllTables();
        }

        private void OnDisconnect(object sender, ClientEventArgs args)
        {
            IsConnected = false;
            MessageLogger.Warning($"Disconnected from database : {args.GetMessage}, trying to reconnect");
            UnsubscribeFromDatabaseEvents();
            Task.Run(Reconnect);
        }

        private void OnError(object sender, OperationTableEventArgs args)
        {
            MessageLogger.Warning($"Database error : {args.TypeError}, " +
                                  $"message : {args.GetMessage}, " +
                                  $"table name : {args.TableName}, " +
                                  $"operation : {args.Operation}");
        }

        private void OnIntelligenceFiltersReceived(object sender, TableEventArs<TableSectorsRangesRecon> args)
        {
            var filters = args.Table
                .Where(filter => filter.IsCheck && filter.NumberASP == _ownStationAsp.Id)
                .Select(f => new Filter((int)f.FreqMinKHz, (int)f.FreqMaxKHz, f.AngleMin, f.AngleMax));
            IntelligenceFiltersReceivedEvent?.Invoke(sender, filters);
        }

        private void OnJammingFiltersReceived(object sender, TableEventArs<TableSectorsRangesSuppr> args)
        {
            var filters = args.Table
                .Where(filter => filter.IsCheck && filter.NumberASP == _ownStationAsp.Id)
                .Select(f => new Filter((int)f.FreqMinKHz, (int)f.FreqMaxKHz, f.AngleMin, f.AngleMax));
            RadioJamFiltersReceivedEvent?.Invoke(sender, filters);
        }

        private int _numberOfFrsJamButtonTargets = 0;

        private void OnFrsJamTargetsReceived(object sender, TableEventArs<TableSuppressFWS> args)
        {
            var jamTargets = args.Table.Where(t => t.NumberASP == _ownStationAsp.Id).Select(t => new RadioJamTarget
            (
                frequencyKhz: (float)t.FreqKHz,
                priority: t.Priority ?? 0,
                threshold: t.Threshold ?? 0,
                direction: t.Bearing ?? 0,
                useAdaptiveThreshold: false,
                modulationCode: t.InterferenceParam.Modulation,
                deviationCode: t.InterferenceParam.Deviation,
                manipulationCode: t.InterferenceParam.Manipulation,
                durationCode: t.InterferenceParam.Duration,
                id: t.Id,
                liter: t.Letter ?? 0,                
                targetConfig: null, // when it goes up, radioJamManager is added as when linked station targets come
                creator: t.Sender.HasValue && t.Sender.Value == SignSender.Button 
                ? TargetCreator.Button
                : TargetCreator.Unknown
            )).ToList();

            FrsJamTargetsReceivedEvent?.Invoke(sender, jamTargets);
            if (!args.Table.Any(t => t.Sender.HasValue && t.Sender.Value == SignSender.Button) && _numberOfFrsJamButtonTargets != 0)
            {
                _numberOfFrsJamButtonTargets = 0;
                StopInfiniteJamming?.Invoke(this, EventArgs.Empty);
            }
            else 
            {
                _numberOfFrsJamButtonTargets = args.Table.Count(s => s.Sender.HasValue && s.Sender.Value == SignSender.Button);
            }
        }

        private void OnFhssJamTargetsReceived(object sender, TableEventArs<TableSuppressFHSS> args)
        {
            var excludedFrequencies = FhssForbiddenFrequenciesTable.Load<TableFHSSExcludedFreq>();
            var fhssJamTargets = args.Table
                .Where(t => t.NumberASP == _ownStationAsp.Id)
                .Select(t => new RadioJamFhssTarget(
                    minFrequencyKhz: (float)t.FreqMinKHz,
                    maxFrequencyKhz: (float)t.FreqMaxKHz,
                    threshold: (float)t.Threshold,
                    modulationCode: t.InterferenceParam.Modulation,
                    deviationCode: t.InterferenceParam.Deviation,
                    manipulationCode: t.InterferenceParam.Manipulation,
                    forbiddenRanges: GetForbiddenRanges(t.Id, excludedFrequencies),
                    id: t.Id
                )).ToList();
            FhssJamTargetsReceivedEvent?.Invoke(this, fhssJamTargets);
        }
        
        private void OnFhssForbiddenFrequenciesReceived(object sender, TableEventArs<TableFHSSExcludedFreq> args)
        {
            var fhssJamTargets = FhssJamTargetsTable.Load<TableSuppressFHSS>()
                .Where(t => t.NumberASP == _ownStationAsp.Id)
                .Select(t => new RadioJamFhssTarget(
                    minFrequencyKhz: (float)t.FreqMinKHz,
                    maxFrequencyKhz: (float)t.FreqMaxKHz,
                    threshold: (float)t.Threshold,
                    modulationCode: t.InterferenceParam.Modulation,
                    deviationCode: t.InterferenceParam.Deviation,
                    manipulationCode: t.InterferenceParam.Manipulation,
                    forbiddenRanges: GetForbiddenRanges(t.Id, args.Table),
                    id: t.Id
                )).ToList();
            FhssJamTargetsReceivedEvent?.Invoke(this, fhssJamTargets);
        }

        private void OnLinkedStationReceived(object sender, TableEventArs<TableASP> args)
        {
            if(CheckTicket(NameTable.TableASP))
                return;
            if (args.Table.Count == 0)
            {
                IntelligenceFiltersReceivedEvent?.Invoke(this, new Filter[0]);
                RadioJamFiltersReceivedEvent?.Invoke(this, new Filter[0]);

                FrsJamTargetsReceivedEvent?.Invoke(this, new RadioJamTarget[0]);
                FhssJamTargetsReceivedEvent?.Invoke(this, new RadioJamFhssTarget[0]);

                ImportantRangesReceivedEvent?.Invoke(this, new FrequencyRange[0]);
                KnownRangesReceivedEvent?.Invoke(this, new FrequencyRange[0]);
                ForbiddenRangesReceivedEvent?.Invoke(this, new FrequencyRange[0]);
                return;
            }
            
            var own = args.Table.FirstOrDefault(station => station.ISOwn == true);
            if (own == null)
            {
                MessageLogger.Warning($"There is no station with property IsOwn == true. Update table asp");
                return;
            }

            _ownStationAsp = own;
            Config.Instance.StationsSettings.OwnPosition = new StationPositionConfig()
            {
                StationId = own.Id,
                Altitude = own.Coordinates.Altitude,
                Longitude = own.Coordinates.Longitude,
                Latitude = own.Coordinates.Latitude
            };
            Config.Instance.StationsSettings.Role = (StationRole)own.Role;

            var linkedStations = Config.Instance.StationsSettings.LinkedPositions;
            linkedStations.Clear();
            foreach (var station in args.Table.Where(station => station.ISOwn == false))
            {
                linkedStations.Add(new StationPositionConfig()
                {
                    StationId = station.Id,
                    Altitude = station.Coordinates.Altitude,
                    Latitude = station.Coordinates.Latitude,
                    Longitude = station.Coordinates.Longitude
                });
            }
        }

        private void OnGlobalPropertiesReceived(object sender, TableEventArs<GlobalProperties> args)
        {
            var dfConfig = Config.Instance.DirectionFindingSettings;
            var propertiesTable = args.Table[0];

            dfConfig.DirectionCorrection = propertiesTable.HeadingAngle;
            if (propertiesTable.NumberAveragingBearing != 0 && propertiesTable.NumberAveragingPhase != 0)
            {
                dfConfig.DefaultDirectionScanCount = propertiesTable.NumberAveragingBearing;
                dfConfig.DefaultBearingScanCount = propertiesTable.NumberAveragingPhase;
            }
            else
            {
                MessageLogger.Error($"Direction and phase averaging numbers are incorrect : " +
                    $"{propertiesTable.NumberAveragingBearing} - {propertiesTable.NumberAveragingPhase}");
            }
            JammingSettingsReceived?.Invoke(this, new JammingSettings()
            {
                ChannelsInLiter = propertiesTable.NumberChannels,
                EmitionDuration = propertiesTable.TimeRadiateFWS,
                FhssRadiationTime = propertiesTable.TimeRadiateFHSS,
                FhssFftResolution = (FftResolution)(propertiesTable.FFTResolution + 2),
                LongWorkingSignalDurationMs = propertiesTable.TimeSearch,
                DirectionSearchSector = propertiesTable.SectorSearch,
                Threshold = propertiesTable.Threshold,
                JammingAutoStopIntervalSec = propertiesTable.TimeJamming
            });
        }

        private void OnImportantFrequenciesReceived(object sender, TableEventArs<TableFreqImportant> args)
        {
            ImportantRangesReceivedEvent?.Invoke(sender,
                args.Table
                    .Where(t => t.NumberASP == _ownStationAsp.Id)
                    .Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)).ToList());
        }

        private void OnKnownFrequenciesReceived(object sender, TableEventArs<TableFreqKnown> args)
        {
            KnownRangesReceivedEvent?.Invoke(sender,
                args.Table
                    .Where(t => t.NumberASP == _ownStationAsp.Id)
                    .Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)).ToList());
        }

        private void OnForbiddenFrequenciesReceived(object sender, TableEventArs<TableFreqForbidden> args)
        {
            ForbiddenRangesReceivedEvent?.Invoke(sender, 
                args.Table
                    .Where(t => t.NumberASP == _ownStationAsp.Id)
                    .Select(r => new FrequencyRange((float)r.FreqMinKHz, (float)r.FreqMaxKHz)).ToList());
        }

        private void LoadAllTables()
        {
            OnLinkedStationReceived(this, new TableEventArs<TableASP>(LinkedStationsTable.Load<TableASP>()));
            OnIntelligenceFiltersReceived(this, new TableEventArs<TableSectorsRangesRecon>(IntelligenceFiltersTable.Load<TableSectorsRangesRecon>()));
            OnJammingFiltersReceived(this, new TableEventArs<TableSectorsRangesSuppr>(JamFiltersTable.Load<TableSectorsRangesSuppr>()));
            OnFrsJamTargetsReceived(this, new TableEventArs<TableSuppressFWS>(FrsJamTargetsTable.Load<TableSuppressFWS>()));
            OnFhssJamTargetsReceived(this, new TableEventArs<TableSuppressFHSS>(FhssJamTargetsTable.Load<TableSuppressFHSS>()));
            OnFhssForbiddenFrequenciesReceived(this, new TableEventArs<TableFHSSExcludedFreq>(FhssForbiddenFrequenciesTable.Load<TableFHSSExcludedFreq>()));
            OnGlobalPropertiesReceived(this, new TableEventArs<GlobalProperties>(GlobalPropertiesTable.Load<GlobalProperties>()));
            OnImportantFrequenciesReceived(this, new TableEventArs<TableFreqImportant>(ImportantFrequenciesTable.Load<TableFreqImportant>()));
            OnKnownFrequenciesReceived(this, new TableEventArs<TableFreqKnown>(KnownFrequenciesTable.Load<TableFreqKnown>()));
            OnForbiddenFrequenciesReceived(this, new TableEventArs<TableFreqForbidden>(ForbiddenFrequenciesTable.Load<TableFreqForbidden>()));

            ClearRadioSourceTable();
            ClearRadioJamStateTables();
            GpsTable.Clear();
            FhssNetworksTable.Clear();
            FhssSourcesTable.Clear();
        }

        public async Task Connect()
        {
            try
            {
                if (!IsConnected)
                {
                    SubscribeToDatabaseEvents();
                    _client.Connect();
                }

                await Task.Delay(10);
            }
            catch (Exception e)
            {
                MessageLogger.Error($"Database connection error {e}");
            }
        }

        public void Disconnect()
        {
            try
            {
                if (IsConnected)
                {
                    UnsubscribeFromDatabaseEvents();
                    _client.Disconnect();
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error($"Error while trying to disconnect from database {e}");
            }
        }

        private async Task Reconnect()
        {
            lock (_reconnectLockObject)
            {
                while (_reconnectionAttemptsCount > 0 && !IsConnected)
                {
                    _reconnectionAttemptsCount--;
                    Connect();
                };

                if (!IsConnected)
                    MessageLogger.Error("Couldn't reconnect to database");
            }
        }

        public void RadioSourceTableAddRange(IEnumerable<IRadioSource> radioSources)
        {
            try
            {
                if (IsConnected)
                {
                    if (radioSources.Count() < _sliceSize)
                    {
                        RadiosourceTable.AddRange(radioSources.Select(RadioSourceToTempFWS).ToList());
                    }
                    else
                    {
                        /*
                         * we cut in slices, because wcf has max package size and ~600 sources is way to much for it
                         * and wcf connection dies.
                         */
                        var radioSourceArray = radioSources.ToArray();
                        
                        for (int i = 0; i < radioSourceArray.Length; i += _sliceSize)
                        {
                            RadiosourceTable.AddRange(SplitArray(radioSourceArray, i, _sliceSize).Select(RadioSourceToTempFWS).ToList());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error("Error while adding frs range" + e);
            }
        }

        public void ClearRadioSourceTable()
        {
            if (IsConnected)
                RadiosourceTable.Clear();
        }

        public void ClearRadioJamStateTables()
        {
            if (IsConnected)
            {
                FrsJamStateTable.Clear();
                FhssJamStateTable.Clear();
            }
        }
        
        private TempFWS RadioSourceToTempFWS(IRadioSource radioSource)
        {
            var radioSourceDataList = new ObservableCollection<JamDirect>()
            {
                new JamDirect()
                {
                    Bearing = radioSource.IsDirectionReliable() ? radioSource.Direction : -1,
                    DistanceKM = 0,
                    Level = (short)radioSource.Amplitude,
                    NumberASP = _ownStationAsp.Id,
                    IsOwn = true,
                    Std = radioSource.StandardDeviation
                }
            };
            var radioSourceCoordinates = new Coord()
            {
                Altitude = radioSource.Altitude,
                Latitude = radioSource.Latitude,
                Longitude = radioSource.Longitude
            };
            foreach (var linkedStationResult in radioSource.LinkedInfo)
            {
                radioSourceDataList.Add(new JamDirect()
                {
                    Bearing = linkedStationResult.Value.reliability > Constants.ReliabilityThreshold ? linkedStationResult.Value.direction : -1,
                    DistanceKM = 0,
                    Level = (short)radioSource.Amplitude,
                    NumberASP = linkedStationResult.Key,
                    IsOwn = false,
                    Std = radioSource.StandardDeviation
                });
            }

            foreach (var jamDirect in radioSourceDataList)
            {
                if (jamDirect.IsOwn)
                {
                    CalculateDistance(jamDirect, _ownStationAsp.Coordinates, radioSourceCoordinates);
                }
                else
                {
                    var station =
                        Config.Instance.StationsSettings.LinkedPositions.FirstOrDefault(s =>
                            s.StationId == jamDirect.NumberASP);
                    CalculateDistance(
                        jamDirect: jamDirect, 
                        stationCoordinates: new Coord()
                        {
                            Altitude = station.Altitude,
                            Latitude = station.Latitude,
                            Longitude = station.Longitude
                        }, 
                        radioSourceCoord: radioSourceCoordinates
                        );
                }
            }
            
            return new TempFWS
            {
                Id = radioSource.Id,
                IdMission = 1,
                Coordinates = radioSourceCoordinates,
                Deviation = radioSource.BandwidthKhz,
                FreqKHz = radioSource.FrequencyKhz,
                ListQ = radioSourceDataList,
                Time = radioSource.FirstBroadcastStartTime,
                Type = (byte)radioSource.SourceType,
                Checking = radioSource.IsActive,
                Control = radioSource.IsActive ? Led.Green : Led.Empty
            };
            
            void CalculateDistance(JamDirect jamDirect, Coord stationCoordinates, Coord radioSourceCoord)
            {
                if (radioSourceCoord.Altitude == -1
                    && radioSourceCoord.Latitude == -1
                    && radioSourceCoord.Longitude == -1)
                    return;

                jamDirect.DistanceKM = (float)ClassBearing.Distance(
                        CoordJam: new List<Coord>()
                        {
                            stationCoordinates
                        },
                        CoordSource: radioSourceCoordinates)
                    .First();
            }
        }

        private IEnumerable<IRadioSource> SplitArray(IRadioSource[] radioSources, int startIndex, int count)
        {
            var amount = Math.Min(startIndex + count, radioSources.Length);
            var slice = new IRadioSource[amount - startIndex];
            for (int i = startIndex; i < amount; i++)
                slice[i - startIndex] = radioSources[i];
            return slice;
        }

        public void UpdateFrsJamTarget(IRadioJamTarget target)
        {
            FrsJamTargetsTable.Change(
                new TableSuppressFWS()
                {
                    Bearing = null,
                    Coordinates = null,
                    FreqKHz = target.FrequencyKhz,
                    Id = target.Id,
                    InterferenceParam = null,
                    Letter = null,
                    NumberASP = _ownStationAsp.Id,
                    Priority = null,
                    Sender = null,
                    Threshold = null,
                });
        }

        public void UpdateFrsRadioJamState(IReadOnlyList<IRadioJamTarget> targets)
        {
            if (IsConnected)
            {
                var dbTargets = targets.Select(t => new TempSuppressFWS()
                {
                    Id = t.Id,
                    FreqKHz = t.FrequencyKhz,
                    Threshold = (short)t.Amplitude,
                    IdMission = 1,
                    NumberASP = Config.Instance.StationsSettings.OwnPosition.StationId,
                    Control = ControlStateToLed(t.ControlState),
                    Radiation = t.IsEmitted ? Led.Blue : Led.Empty,
                    Suppress = ControlStateToLed(t.JamState)
                }).ToList();
                FrsJamStateTable.Clear();
                FrsJamStateTable.AddRange(dbTargets);
            }

            Led ControlStateToLed(TargetState state)
            {
                switch (state)
                {
                    case TargetState.NotActive: return Led.Empty;
                    case TargetState.Active: return Led.Red;
                    case TargetState.ActiveLongTime: return Led.Green;
                    case TargetState.NotActiveLongTime: return Led.Yellow;
                    default: return Led.Empty;
                }
            }
        }

        public void UpdateFhssRadioJamState(IReadOnlyList<(int id, bool isJammed)> targets,
            IReadOnlyList<float> jammedFrequencies)
        {
            FhssJamStateTable.Clear();

            var databaseTargets = targets.Select(
                t => new TempSuppressFHSS()
                {
                    Id = t.id,
                    Control = t.isJammed ? Led.Green : Led.Empty,
                    Radiation = t.isJammed ? Led.Blue : Led.Empty,
                    Suppress = t.isJammed ? Led.Green : Led.Empty,
                    IdMission = 1
                }).ToList();

            FhssJamStateTable.AddRange(databaseTargets);
        }

        public void UpdateGpsCoordinates(IGpsReceiver gpsReceiver)
        {
            if(!IsConnected)
                return;

            var gpsData = GpsTable.Load<TempGNSS>().FirstOrDefault();
            var updatedGpsData = new TempGNSS()
            {
                AntennaHeight = gpsReceiver.AntennaHeight,
                Id = gpsData?.Id ?? 1,
                IdMission = 1,
                LocalTime = gpsReceiver.LocalTime,
                Location = new Coord()
                {
                    Altitude = gpsReceiver.Location.Altitude,
                    Latitude = gpsReceiver.Location.Latitude,
                    Longitude = gpsReceiver.Location.Longitude
                },
                NumberOfSatellites = gpsReceiver.NumberOfSatellites,
                UtcTime = gpsReceiver.UtcTime,
                CmpPA = gpsData?.CmpPA ?? 0,
                CmpRR = gpsData?.CmpRR ?? 0
            };

            if (gpsData != null)
                GpsTable.Change(updatedGpsData);
            else
                GpsTable.Add(updatedGpsData);
        }

        private IReadOnlyList<FrequencyRange> GetForbiddenRanges(int fhssJamId, IReadOnlyList<TableFHSSExcludedFreq> excludedFrequencies)
        {
            if(excludedFrequencies.Count == 0)
                return new List<FrequencyRange>();

            var forbiddenRanges = excludedFrequencies.Where(f => f.IdFHSS == fhssJamId).Select(
                excluded => new FrequencyRange(
                    (float)excluded.FreqKHz - excluded.Deviation / 2,
                    (float)excluded.FreqKHz + excluded.Deviation / 2))
                .ToList();
            var count = forbiddenRanges.Count;
            for (int i = 0; i < count; i++)
            {
                var minFrequency = excludedFrequencies[i].FreqKHz;
                var difference = minFrequency - ((int)minFrequency / 1000) * 1000;
                if (difference > 0)
                {
                    forbiddenRanges.Add(new FrequencyRange((float)(excludedFrequencies[i].FreqKHz - difference), (float)excludedFrequencies[i].FreqKHz));
                }
            }
            
            return forbiddenRanges;
        }

        public void FhssTableAddRange(IEnumerable<IFhssNetwork> fhssNetworks)
        {
            if (!IsConnected)
            {
                MessageLogger.Warning("Not connected to database to perform fhss table add range!");
                return;
            }

            try
            {
                //note : AddRange() is not working properly
                FhssTableClear();
                foreach (var network in fhssNetworks)
                {
                    FhssNetworksTable.Add(new TableReconFHSS()
                    {
                        Id = network.Id,
                        IdMission = 1,
                        FreqMinKHz = network.MinFrequencyKhz,
                        FreqMaxKHz = network.MaxFrequencyKhz,
                        Deviation = network.BandwidthKhz,
                        Time = network.LastUpdateTime,
                        StepKHz = (short)network.StepKhz,
                        ImpulseDuration = (ushort)network.ImpulseDurationMs,
                        QuantitySignal = (short)network.FrequenciesCount
                    });
                    AddFhssToFhssSourceTable(network);
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error("Error while adding frs range" + e);
            }
        }

        private void AddFhssToFhssSourceTable(IFhssNetwork network)
        {
            //add our info
            foreach (var userInfo in network.UserInfo)
            {
                var fhssSourceRecord = new TableSourceFHSS()
                {
                    IdFHSS = network.Id,
                    Coordinates = new Coord(),
                    ListJamDirect = new ObservableCollection<TableJamDirect>()
                };
                fhssSourceRecord.ListJamDirect.Add(
                    new TableJamDirect()
                    {
                        JamDirect = new JamDirect()
                        {
                            Bearing = userInfo.Direction,
                            Level = (short)userInfo.Amplitude,
                            NumberASP = _ownStationAsp.Id,
                            Std = userInfo.StandardDeviation,
                            DistanceKM = -1 // todo : calculate distance
                        }
                    }
                );
                FhssSourcesTable.Add(fhssSourceRecord);
            }
            //add linked info
            foreach (var info in network.LinkedUserInfo)
            {
                var fhssSourceRecord = new TableSourceFHSS()
                {
                    IdFHSS = network.Id,
                    Coordinates = new Coord(),
                    ListJamDirect = new ObservableCollection<TableJamDirect>()
                };

                fhssSourceRecord.ListJamDirect.Add(
                    new TableJamDirect()
                    {
                        JamDirect = new JamDirect()
                        {
                            Bearing = info.Value.Direction,
                            Level = (short)info.Value.Amplitude,
                            NumberASP = info.Key,
                            Std = info.Value.StandardDeviation,
                            DistanceKM = -1 // todo : calculate distance
                        }
                    }
                );
                FhssSourcesTable.Add(fhssSourceRecord);
            }
        }

        public void FhssTableClear()
        {
            if (IsConnected)
            {
                FhssNetworksTable.Clear();
                FhssSourcesTable.Clear();
            }
        }

        public void UpdateStationMode(DspServerMode mode)
        {
            _tickets.Add(new Ticket(NameTable.TableASP));

            if (_ownStationAsp == null)
                return;

            _ownStationAsp.Mode = ServerModeToByte(mode);
            LinkedStationsTable.Change(_ownStationAsp);

            byte ServerModeToByte(DspServerMode serverMode)
            {
                switch (serverMode)
                {
                    case DspServerMode.Stop: return 0;
                    case DspServerMode.RadioIntelligence: return 1;
                    case DspServerMode.RadioIntelligenceWithDf: return 1;
                    default: return 2;
                }
            }
        }

        private bool CheckTicket(NameTable tableToCheck)
        {
            if (_tickets.Count == 0)
                return false;
            var ticket = _tickets.FirstOrDefault(t => t.IsExpired() == false && t.Table == tableToCheck);
            if (ticket.IsDefault() == false)
            {
                _tickets.Remove(ticket);
                return true;
            }
            return false;
        }

        public void DeleteRadioSourceRecord(int id)
        {
            if(IsConnected)
                RadiosourceTable.Delete(new TempFWS()
                {
                    Id = id
                });
        }
    }
}
