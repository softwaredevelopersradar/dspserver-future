﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DspDataModel;
using Settings;
using SimulatorProtocols;

namespace RadioRecorder
{
    public class RecordPlayer : IDisposable
    {
        public string Filename { get; private set; }

        public bool IsRecordOpened { get; private set; }

        private FileStream _fileStream;

        private DataScanInfoHolder _scanInfoHolder;


        private readonly object _lockObject = new object();

        public RecordPlayer()
        {
            IsRecordOpened = false;
        }

        public void OpenRecord(string filename)
        {
            CloseRecord();
            Filename = filename;
            IsRecordOpened = true;
            _fileStream = new FileStream(filename, FileMode.Open);
            lock (_lockObject)
            {
                _scanInfoHolder = new DataScanInfoHolder(_fileStream);
            }
        }

        public void CloseRecord()
        {
            IsRecordOpened = false;
            _fileStream?.Close();
        }

        public DataResponse ReadScan(IReadOnlyList<int> bandNumbers)
        {
            var tasks = new List<ReadScanTask>(Constants.ReceiversCount);
            for (var i = 0; i < Constants.ReceiversCount; ++i)
            {
                tasks.Add(new ReadScanTask(bandNumbers[i], i));
            }
            return ReadScan(tasks);
        }

        public DataResponse ReadScan(IReadOnlyList<byte> bandNumbers)
        {
            var tasks = new List<ReadScanTask>(Constants.ReceiversCount);
            for (var i = 0; i < Constants.ReceiversCount; ++i)
            {
                tasks.Add(new ReadScanTask(bandNumbers[i], i));
            }
            return ReadScan(tasks);
        }

        private DataResponse ReadScan(List<ReadScanTask> tasks)
        {
            if (!IsRecordOpened)
            {
                return null;
            }
            var channels = new ChannelData[Constants.ReceiversCount];

            while (tasks.Count > 0)
            {
                var task = tasks[0];
                DataScanInfo dataScanInfo;

                lock (_lockObject)
                {
                    dataScanInfo = _scanInfoHolder.GetNextDataScanInfo(task.BandNumber, task.ChannelIndex);
                }
                tasks.Remove(task);
                channels[task.ChannelIndex] = GetDataChannel(dataScanInfo, task.BandNumber, task.ChannelIndex);
                if (dataScanInfo == null)
                {
                    continue;
                }
                for (var i = 0; i < tasks.Count; ++i)
                {
                    if (dataScanInfo.BandNumbers[tasks[i].ChannelIndex] == tasks[i].BandNumber)
                    {
                        channels[tasks[i].ChannelIndex] = GetDataChannel(dataScanInfo, tasks[i].BandNumber, tasks[i].ChannelIndex);
                        tasks.Remove(tasks[i]);
                        --i;
                    }
                }
                
            }
            return new DataResponse(channels, true);
        }

        private ChannelData GetDataChannel(DataScanInfo dataScanInfo, int bandNumber, int channelIndex)
        {
            if (dataScanInfo == null)
            {
                return new ChannelData();
            }
            _fileStream.Position = dataScanInfo.Position + dataScanInfo.ChannelShifts[channelIndex];
            var buffer = new byte[dataScanInfo.ChannelSizes[channelIndex]];
            _fileStream.Read(buffer, 0, buffer.Length);

            return ParseChannel(RecorderTypes.DataChannel.Parse(buffer));
        }

        private ChannelData ParseChannel(RecorderTypes.DataChannel channel)
        {
            var amplitudes = new byte[Constants.BandSampleCount];
            var phases = new short[Constants.BandSampleCount];

            var amplitudeIndex = 0;
            var phaseIndex = 0;
            var resultIndex = 0;
            const float phaseMultiplier = 360f / byte.MaxValue * 10;

            while (amplitudeIndex < channel.Amplitudes.Length)
            {
                if (channel.Amplitudes[amplitudeIndex] != 255)
                {
                    amplitudes[resultIndex] = channel.Amplitudes[amplitudeIndex++];
                    phases[resultIndex] = (short) (channel.Phases[phaseIndex++] * phaseMultiplier);
                    ++resultIndex;
                }
                else
                {
                    var zeroSamplesCount = channel.Amplitudes[amplitudeIndex + 1];
                    amplitudeIndex += 2;
                    for (var i = 0; i < zeroSamplesCount; ++i)
                    {
                        amplitudes[resultIndex] = channel.ThresholdAmplitude;
                        phases[resultIndex] = channel.ThresholdAmplitude;
                        ++resultIndex;
                    }
                }
            }

            return new ChannelData(amplitudes, phases);
        }

        public void Dispose()
        {
            CloseRecord();
        }

        private struct ReadScanTask
        {
            public readonly int BandNumber;
            public readonly int ChannelIndex;

            public ReadScanTask(int bandNumber, int channelIndex)
            {
                BandNumber = bandNumber;
                ChannelIndex = channelIndex;
            }
        }

        private class DataScanInfoHolder
        {
            private readonly DataScanInfoChannelList[] _scanInfoChannelLists;

            private readonly FileStream _fileStream;

            public DataScanInfoHolder(FileStream fileStream)
            {
                _fileStream = fileStream;
                _scanInfoChannelLists = new DataScanInfoChannelList[Constants.ReceiversCount];
                for (var i = 0; i < Constants.ReceiversCount; ++i)
                {
                    _scanInfoChannelLists[i] = new DataScanInfoChannelList();
                }
                Initialize();
            }

            public DataScanInfo GetNextDataScanInfo(int bandNumber, int channelIndex)
            {
                return _scanInfoChannelLists[channelIndex].GetNextDataScanInfo(bandNumber);
            }

            private void Initialize()
            {
                var position = 0;
                var length = _fileStream.Length;

                while (position < length)
                {
                    var buffer = new byte[RecorderTypes.DataScanHeader.BinarySize];
                    _fileStream.Read(buffer, 0, buffer.Length);

                    var header = RecorderTypes.DataScanHeader.Parse(buffer);
                    var dataScanInfo = new DataScanInfo(position, header);

                    for (var i = 0; i < Constants.ReceiversCount; ++i)
                    {
                        _scanInfoChannelLists[i].Add(header.BandNumbers[i], dataScanInfo);
                    }

                    position += header.ScanSize;
                    _fileStream.Position = position;
                }
            }

            private class DataScanInfoChannelList
            {
                private readonly List<DataScanInfo>[] ScanInfoLists;

                private int _index;

                public DataScanInfoChannelList()
                {
                    ScanInfoLists = new List<DataScanInfo>[Config.Instance.BandSettings.BandCount];
                    for (var i = 0; i < Config.Instance.BandSettings.BandCount; ++i)
                    {
                        ScanInfoLists[i] = new List<DataScanInfo>();
                    }
                    _index = -1;
                }

                public void Add(int bandNumber, DataScanInfo scanInfo)
                {
                    ScanInfoLists[bandNumber].Add(scanInfo);
                }

                public DataScanInfo GetNextDataScanInfo(int bandNumber)
                {
                    if (ScanInfoLists[bandNumber].Count == 0)
                    {
                        return null;
                    }
                    ++_index;
                    if (_index >= ScanInfoLists[bandNumber].Count)
                    {
                        _index = 0;
                    }
                    return ScanInfoLists[bandNumber][_index];
                }
            }
        }

        private class DataScanInfo
        {
            public readonly int Position;
            public readonly int ScanSize;
            public readonly int[] ChannelShifts;
            public readonly int[] ChannelSizes;
            public readonly int[] BandNumbers;

            public DataScanInfo(int position, RecorderTypes.DataScanHeader header)
            {
                ScanSize = header.ScanSize;
                Position = position;
                BandNumbers = header.BandNumbers.Select(n => (int) n).ToArray();
                ChannelShifts = header.ChannelShifts;
                ChannelSizes = new int[Constants.ReceiversCount];
                for (var i = 0; i < Constants.ReceiversCount - 1; ++i)
                {
                    ChannelSizes[i] = ChannelShifts[i + 1] - ChannelShifts[i];
                }
                ChannelSizes[Constants.ReceiversCount - 1] = ScanSize - ChannelShifts[Constants.ReceiversCount - 1];
            }
        }
    }
}
