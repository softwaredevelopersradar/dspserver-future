namespace RecorderTypes
{
	struct DataScanHeader
	{
		byte[6] BandNumbers
		int ScanSize
		int[6] ChannelShifts
	}

	class RecorderDataScan
	{
		DataScanHeader Header
		DataChannel[6] Channels
	}

	class DataChannel
	{
		byte ThresholdAmplitude
		short AmplitudesLength
		short PhasesLength
		byte[AmplitudesLength] Amplitudes
		byte[PhasesLength] Phases
	}
}