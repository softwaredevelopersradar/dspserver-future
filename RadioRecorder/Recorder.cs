﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Settings;
using DspDataModel.Data;
using RecorderTypes;

namespace RadioRecorder
{
    public class Recorder
    {
        public float Quality
        {
            get => _quality;
            set
            {
                Contract.Assert(0 < value && value <= 1);
                _quality = value;
            }
        }

        public bool IsRecording { get; private set; }

        public int CurrentRecordId { get; private set; }

        private FileStream _fileStream;

        private readonly object _fileStreamLock = new object();

        private float _quality;

        private const string FolderName = "Records";

        public Recorder(float quality)
        {
            Quality = quality;
            IsRecording = false;
            CurrentRecordId = -1;
        }

        public void StartRecording()
        {
            var id = GetRecordId();
            CurrentRecordId = id;
            StartRecording("RadioRecord" + id + ".bin");
        }

        private int GetRecordId()
        {
            if (!Directory.Exists(FolderName))
            {
                Directory.CreateDirectory(FolderName);
                return 0;
            }
            var indexes = Directory.EnumerateFiles(FolderName)
                .Select(Path.GetFileName)
                .Where(f => f.StartsWith("RadioRecord") && f.EndsWith(".bin"))
                .Select(f =>
                {
                    var numberString = f.Replace("RadioRecord", "").Replace(".bin", "");
                    if (!int.TryParse(numberString, out var index))
                    {
                        index = -1;
                    }
                    return index;
                })
                .ToArray();
            return indexes.Any() ? indexes.Max() + 1 : 0;
        }

        public void StartRecording(string filename)
        {
            if (IsRecording)
            {
                StopRecording();
            }
            IsRecording = true;

            lock (_fileStreamLock)
            {
                try
                {
                    _fileStream = new FileStream(Path.Combine(FolderName, filename), FileMode.Create, FileAccess.Write);
                }
                catch (Exception)
                {
                    IsRecording = false;
                    _fileStream?.Close();
                }
            }
        }

        public void StopRecording()
        {
            if (!IsRecording)
            {
                return;
            }
            IsRecording = false;
            lock (_fileStreamLock)
            {
                try
                {
                    _fileStream?.Close();
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public void Record(IFpgaDataScan fpgaScan)
        {
            var channels = fpgaScan.Scans.Select(GetDataChannel).ToArray();
            var scanSize = channels.Sum(c => c.StructureBinarySize) + DataScanHeader.BinarySize;
            var bandNumbers = fpgaScan.Scans.Select(r => (byte) r.BandNumber).ToArray();
            var shifts = new int[Constants.ReceiversCount];
            shifts[0] = DataScanHeader.BinarySize;
            for (var i = 1; i < Constants.ReceiversCount; ++i)
            {
                shifts[i] = shifts[i - 1] + channels[i - 1].StructureBinarySize;
            }

            var dataScan = new RecorderDataScan(new DataScanHeader(bandNumbers, scanSize, shifts), channels);
            var data = dataScan.GetBytes();
            lock (_fileStreamLock)
            {
                try
                {
                    _fileStream.Write(data, 0, data.Length);
                }
                catch (Exception)
                {
                    IsRecording = false;
                    _fileStream.Close();
                }
            }
        }

        private DataChannel GetDataChannel(IReceiverScan scan)
        {
            var threshold = GetThresholdAmplitude(scan);
            var amplitudeChannel = new List<byte>(scan.Count);
            var phaseChannel = new List<byte>(scan.Count);

            const float phaseMultiplier = byte.MaxValue / 360f;

            var byteThresold = (byte) (threshold - Constants.ReceiverMinAmplitude);

            for (var i = 0; i < scan.Count; ++i)
            {
                var amplitude = scan.Amplitudes[i];
                var byteAmplitude = (byte) (amplitude - Constants.ReceiverMinAmplitude);
                if (byteAmplitude % 2 == 1)
                {
                    --byteAmplitude;
                }

                if (byteAmplitude > byteThresold)
                {
                    amplitudeChannel.Add(byteAmplitude);

                    var bytePhase = (byte) (scan.Phases[i] * phaseMultiplier);
                    phaseChannel.Add(bytePhase);
                }
                else
                {
                    int j;
                    for (j = i + 1; j < scan.Count && j - i < byte.MaxValue - 1; ++j)
                    {
                        if (scan.Amplitudes[j] > threshold)
                        {
                            --j;
                            break;
                        }
                    }
                    if (j == scan.Count)
                    {
                        --j;
                    }
                    // 255 means that next byte indicates how many samples in a row are zero valued 
                    // zero valued means that sample's amplitude is lower or equal than threshold
                    amplitudeChannel.Add(255);
                    amplitudeChannel.Add((byte)(j - i + 1));

                    i = j;
                }
            }

            var byteThrehsold = (byte) (threshold - Constants.ReceiverMinAmplitude);
            return new DataChannel(
                byteThrehsold, 
                (short) amplitudeChannel.Count, 
                (short) phaseChannel.Count,
                amplitudeChannel.ToArray(), 
                phaseChannel.ToArray());
        }

        private float GetThresholdAmplitude(IReceiverScan scan)
        {
            var amplitudes = new int[byte.MaxValue];
            foreach (var amplitude in scan.Amplitudes)
            {
                var index = (int) (amplitude - Constants.ReceiverMinAmplitude);
                amplitudes[index]++;
            }
            var sum = 0;
            var countThreshold = (int) (scan.Count * (1 - Quality));
            for (var i = 0; i < amplitudes.Length; ++i)
            {
                sum += amplitudes[i];
                if (sum > countThreshold)
                {
                    return i + Constants.ReceiverMinAmplitude;
                }
            }
            return 0;
        }
    }
}
