﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using DspDataModel;
using Settings;
using TasksLibrary.Modes;

namespace DspServerApp
{
    public static class Program
    {
        private static DspServer.DspServer _server;

        private static Process _simulatorProcess;

        private static NotifyIcon _trayIcon;
        private static ContextMenu _trayMenu;

        private static bool _isWindowVisible;

        private static string WindowTitleBase;

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        static void Main(string[] args)
        {
            Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
            MessageLogger.SetLogger(new NLogLogger());

            if (Instance.HasRunningCopy)
            {
                MessageLogger.Log("Another copy of the application is already running", ConsoleColor.Red);
                Console.ReadLine();
                return;
            }
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);
            WindowTitleBase = GetWindowTitle();

            if (Config.Instance.AppSettings.StartupHideWindow)
            {
                HideWindow();
            }
            CreateTrayIcon();
            DefineUsrpReceiverSetup();
            var simulatorTask = Task.Run(() => StartSimulatorIfNecessary());
            AppDomain.CurrentDomain.UnhandledException += OnUnhandledException;
            var stopwatch = Stopwatch.StartNew();
            _server = new DspServer.DspServer();

            SetConsoleCloseHandler();
            simulatorTask.Wait();
            try
            {
                _server.Start();
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
                ShowWindow();
                Console.ReadLine();
                return;
            }
            stopwatch.Stop();

            _trayIcon.Icon = Properties.Resources.GreenIndicator;
            var workingRange = Config.Instance.BandSettings.BandCount == 100 ? "3 GHz" : "6 GHz";

            Console.Title = WindowTitleBase;
            MessageLogger.Log($"{workingRange} DSP server version {GetVersion()}");

            if (Config.Instance.HardwareSettings.SimulateFpgaDevices)
            {
                MessageLogger.Log("Simulation mode");
            }
            MessageLogger.Log($"Started at: {_server.Host}:{_server.Port}");
            MessageLogger.Log($"Startup time: {stopwatch.ElapsedMilliseconds} ms");
            WindowStatusUpdateLoop();
            Application.Run();
        }

        private static string GetWindowTitle()
        {
            var workingRange = Config.Instance.BandSettings.BandCount == 100 ? "3 GHz" : "6 GHz";
            return $"{workingRange} DSP server {GetVersion()}";
        }

        private static async void WindowStatusUpdateLoop()
        {
            while (true)
            {
                await Task.Delay(1000);
                if (_server.TaskManager.CurrentMode is CalibrationMode mode)
                {
                    var progress = (int) (mode.Progress * 100);
                    Console.Title = WindowTitleBase + ". Calibration " + progress + "%";
                }
                else
                {
                    Console.Title = WindowTitleBase;
                }
            }
        }

        private static void SetConsoleCloseHandler()
        {
            _handler = ConsoleEventCallback;
            SetConsoleCtrlHandler(_handler, true);
        }

        static bool ConsoleEventCallback(int eventType)
        {
            // Console window closing
            if (eventType == 2)
            {
                _server.TaskManager.ClearTasks();
                _server.TaskManager.HardwareController.DeviceManager.Close();
                _server.TaskManager.DatabaseController.Disconnect();
                _server.Controller.DataTransferController.Disconnect();
                _trayIcon.Dispose();
                StopSimulatorIfNecessary();
                Environment.Exit(0);//maybe it just ensures closure
            }
            return false;
        }

        private static void CreateTrayIcon()
        {
            _trayMenu = new ContextMenu(new[]
            {
                new MenuItem("Calibrate", OnCalibrateTrayMenuClick),
                new MenuItem("Enable trace log", OnTraceLogTrayMenuClick),
                new MenuItem("Disable server log", OnServerLogTrayMenuClick),
                new MenuItem("Enable receivers log", OnReceiversLogTrayMenuClick),
                new MenuItem("Exit", OnExitTrayMenuClick),
            });

            _trayIcon = new NotifyIcon
            {
                Text = "Пеленгатор",
                Icon = Properties.Resources.RedIndicator,
                Visible = true,
                ContextMenu = _trayMenu
            };
            _trayIcon.MouseClick += OnTrayIconClick;
        }

        private static void OnTraceLogTrayMenuClick(object sender, EventArgs eventArgs)
        {
            MessageLogger.IsTraceLogEnabled = !MessageLogger.IsTraceLogEnabled;
            UpdateTrayMenuItem("trace", _trayMenu.MenuItems[1], MessageLogger.IsTraceLogEnabled);
        }

        private static void OnServerLogTrayMenuClick(object sender, EventArgs eventArgs)
        {
            MessageLogger.IsServerLogEnabled = !MessageLogger.IsServerLogEnabled;
            UpdateTrayMenuItem("server", _trayMenu.MenuItems[2], MessageLogger.IsServerLogEnabled);
        }

        private static void OnReceiversLogTrayMenuClick(object sender, EventArgs eventArgs)
        {
            MessageLogger.IsReceiversLogEnabled = !MessageLogger.IsReceiversLogEnabled;
            UpdateTrayMenuItem("receivers", _trayMenu.MenuItems[3], MessageLogger.IsReceiversLogEnabled);
        }

        private static void UpdateTrayMenuItem(string logName, MenuItem menuItem, bool messageLoggerFlag)
        {
            menuItem.Text = messageLoggerFlag
                ? $"Disable {logName} log"
                : $"Enable {logName} log";
        }

        private static void OnCalibrateTrayMenuClick(object sender, EventArgs eventArgs)
        {
            try
            {
                _server.StartCalibration();
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
            }
        }

        private static void OnExitTrayMenuClick(object sender, EventArgs eventArgs)
        {
            ConsoleEventCallback(2);
            Application.Exit();
        }

        private static void OnTrayIconClick(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Left)
            {
                return;
            }
            if (_isWindowVisible)
            {
                HideWindow();
            }
            else
            {
                ShowWindow();
            }
        }

        private static void ShowWindow()
        {
            const int cmdShow = 5;

            var handle = GetConsoleWindow();
            ShowWindow(handle, cmdShow);
            _isWindowVisible = true;
        }

        private static void HideWindow()
        {
            const int cmdHide = 0;

            var handle = GetConsoleWindow();
            ShowWindow(handle, cmdHide);
            _isWindowVisible = false;
        }

        private static string GetVersion()
        {
            var assembly = Assembly.GetEntryAssembly();
            return assembly == null 
                ? "Can't find assembly!"
                : FileVersionInfo.GetVersionInfo(assembly.Location).FileVersion;
        }

        private static void OnUnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            _trayIcon.Icon = Properties.Resources.RedIndicator;
            var exception = e.ExceptionObject as Exception ?? new Exception("Can't parse exception");
            MessageLogger.Error(exception);
        }

        private static void DefineUsrpReceiverSetup()
        {
            var is3GhzRange = Config.Instance.BandSettings.BandCount == 100 ? true : false;
            var isUsrpExists = Config.Instance.HardwareSettings.UsrpSettings.IsUsrpExists
                ? ReceiverUsrpSetup.Receiver3GhzUsrp6Ghz
                : ReceiverUsrpSetup.Receiver6Ghz;
            Config.Instance.BandSettings.ReceiverUsrpSetup = is3GhzRange
                ? ReceiverUsrpSetup.Receiver3Ghz
                : isUsrpExists;
        }

        private static void StartSimulatorIfNecessary()
        {
            //yet another hack for usrp
            if (Config.Instance.BandSettings.ReceiverUsrpSetup == ReceiverUsrpSetup.Receiver3GhzUsrp6Ghz &&
                !Config.Instance.HardwareSettings.UsrpSettings.SimulateUsrp)
                return;

            if (Config.Instance.HardwareSettings.SimulateFpgaDevices ||
            Config.Instance.HardwareSettings.UsrpSettings.SimulateUsrp)
            {
                if (Process.GetProcessesByName("Simulator").Any())
                {
                    return;
                }
                _simulatorProcess = new Process
                {
                    StartInfo = { FileName = "Simulator.exe" }
                };
                _simulatorProcess.Start();
            }
        }

        private static void StopSimulatorIfNecessary()
        {
            if (Config.Instance.HardwareSettings.SimulateFpgaDevices == false)
            {
                return;
            }

            if (Process.GetProcessesByName("Simulator").Any() == false)
            {
                return;
            }
            _simulatorProcess.CloseMainWindow();
            _simulatorProcess.Close();
        }

        private static ConsoleEventDelegate _handler;

        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);
    }
}
