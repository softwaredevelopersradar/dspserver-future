﻿using System;
using Settings;
using DataProcessor;
using DspDataModel.Data;

namespace Benchmarks
{
    public static class Utils
    {
        private static readonly Random Random = new Random(DateTime.Now.Millisecond);

        public static FpgaDataScan CreateFpgaDataScan()
        {
            var dfScans = new IReceiverScan[Constants.DfReceiversCount];
            for (var i = 0; i < dfScans.Length; ++i)
            {
                dfScans[i] = CreateScan();
            }
            var rcScan = CreateScan();

            return new FpgaDataScan(dfScans, rcScan);
        }

        public static IDataScan CreateDataScan()
        {
            var aggregator = new DataAggregator(new[] {0, 1, 2, 3, 4}, false);
            for (var i = 0; i < 10; ++i)
            {
                aggregator.AddScan(CreateFpgaDataScan());
            }

            return aggregator.GetDataScan();
        }

        public static IReceiverScan CreateScan()
        {
            var amplitudes = new float[Constants.BandSampleCount];
            var phases = new float[Constants.BandSampleCount];

            for (var i = 0; i < Constants.BandSampleCount; ++i)
            {
                amplitudes[i] = Constants.DefaultThresholdValue + (float)Random.NextDouble() * 10;
                phases[i] = Random.Next(0, 359);
            }

            return new ReceiverScan(amplitudes, phases, 0, DateTime.UtcNow, 0);
        }
}
}
