﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using DataProcessor;
using DspDataModel;
using DspDataModel.Data;

namespace Benchmarks
{
    [ShortRunJob]
    public class CalibrationBenchmark
    {
        private IDataAggregator _aggregator;

        [Setup]
        public void Setup()
        {
            _aggregator = new DataAggregator(new[] {0, 1, 2, 3, 4}, false);
            for (var i = 0; i < Config.Instance.CalibrationSettings.CalibrationMaxScanCount; ++i)
            {
                _aggregator.AddScan(Utils.CreateFpgaDataScan());
            }
        }

        [Benchmark]
        public void TestBandCalibration()
        {
            // 26 ms
            _aggregator.GetCalibrationData(Config.Instance.CalibrationSettings.CalibrationStepKhz);
        }

    }
}
