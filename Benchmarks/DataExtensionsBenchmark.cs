using System.Linq;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using DspDataModel.Data;
using Settings;

namespace Benchmarks
{
    [ShortRunJob]
    public class DataExtensionsBenchmark
    {
        private float[] _spectrum;

        [Setup]
        public void Setup()
        {
            _spectrum = Enumerable.Range(0, Constants.BandSampleCount)
                .Select(i => -1f * (i % 140))
                .ToArray();
        }

        [Benchmark]
        public float TestGetNoiseLevel()
        {
            // 13 us
            return _spectrum.GetNoiseLevel();
        }
    }
}