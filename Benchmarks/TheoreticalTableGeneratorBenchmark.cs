﻿using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using Correlator;
using DspDataModel;

namespace Benchmarks
{
    [ShortRunJob]
    public class TheoreticalTableGeneratorBenchmark
    {
        [Setup]
        public void Setup()
        {
            // just load config
            var _ = Config.Instance.TheoreticalTableSettings;
        }

        [Benchmark(Description = "Generate theoretical table")]
        public void GenerateTheoreticalTable()
        {
            var table = new TheoreticalTable();
        }
    }
}
