﻿using System;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using DataProcessor.AmplitudeCalculators;
using Settings;
using DspDataModel.Data;
using DspDataModel.AmplitudeCalulators;

namespace Benchmarks
{
    [ShortRunJob]
    public class DataScanBenchmark
    {
        private IReceiverScan[][] _receiverScans;

        [Setup]
        public void Setup()
        {
            var random = new Random(DateTime.Now.Millisecond);
            _receiverScans = new IReceiverScan[Constants.ReceiversCount][];

            for (var i = 0; i < _receiverScans.Length; i++)
            {
                _receiverScans[i] = new IReceiverScan[10];
                for (var j = 0; j < _receiverScans[i].Length; ++j)
                {
                    _receiverScans[i][j] = CreateScan();
                }
            }

            IReceiverScan CreateScan()
            {
                var amplitudes = new float[Constants.BandSampleCount];
                var phases = new float[Constants.BandSampleCount];

                for (var i = 0; i < Constants.BandSampleCount; ++i)
                {
                    amplitudes[i] = Constants.DefaultThresholdValue + (float) random.NextDouble() * 5;
                    phases[i] = random.Next(0, 359);
                }

                return new ReceiverScan(amplitudes, phases, 0, DateTime.UtcNow, 0);
            }
        }

        [Benchmark]
        public void TestDataScanCreation()
        {
            // 160 us
            new DataScan(_receiverScans, AmplitudeCalculatorFactory.Instance.CreateCalculator());
        }
    }
}
