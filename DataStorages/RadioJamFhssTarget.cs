﻿using System.Collections.Generic;
using DspDataModel;
using DspDataModel.RadioJam;

namespace DataStorages
{
    public class RadioJamFhssTarget : IRadioJamFhssTarget
    {
        public float MinFrequencyKhz { get; }
        public float MaxFrequencyKhz { get; }
        public float Threshold { get; }
        public byte ModulationCode { get; }
        public byte DeviationCode { get; }
        public byte ManipulationCode { get; }
        public IReadOnlyList<FrequencyRange> ForbiddenRanges { get; }
        public int Id { get; }

        public RadioJamFhssTarget(float minFrequencyKhz, float maxFrequencyKhz, float threshold, byte modulationCode, byte deviationCode, byte manipulationCode, IReadOnlyList<FrequencyRange> forbiddenRanges, int id)
        {
            MinFrequencyKhz = minFrequencyKhz;
            MaxFrequencyKhz = maxFrequencyKhz;
            Threshold = threshold;
            ModulationCode = modulationCode;
            DeviationCode = deviationCode;
            ManipulationCode = manipulationCode;
            Id = id;
            ForbiddenRanges = forbiddenRanges;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RadioJamFhssTarget) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = MinFrequencyKhz.GetHashCode();
                hashCode = (hashCode * 397) ^ MaxFrequencyKhz.GetHashCode();
                hashCode = (hashCode * 397) ^ Threshold.GetHashCode();
                hashCode = (hashCode * 397) ^ ModulationCode.GetHashCode();
                hashCode = (hashCode * 397) ^ DeviationCode.GetHashCode();
                hashCode = (hashCode * 397) ^ ManipulationCode.GetHashCode();
                return hashCode;
            }
        }

        public bool Equals(IRadioJamFhssTarget other)
        {
            return MinFrequencyKhz.Equals(other.MinFrequencyKhz) && MaxFrequencyKhz.Equals(other.MaxFrequencyKhz) &&
                   Threshold.Equals(other.Threshold) && ModulationCode == other.ModulationCode &&
                   DeviationCode == other.DeviationCode && ManipulationCode == other.ManipulationCode && CompareForbiddenRanges();

            bool CompareForbiddenRanges()
            {
                if (ForbiddenRanges.Count != other.ForbiddenRanges.Count)
                    return false;
                foreach (var range in ForbiddenRanges)
                {
                    var otherListHasRange = false;
                    foreach (var otherRange in other.ForbiddenRanges)
                    {
                        if (range.Equals(otherRange))
                        {
                            otherListHasRange = true;
                            break;
                        }
                    }
                    if (otherListHasRange == false)
                        return false;
                }
                return true;
            }
        }
    }
}