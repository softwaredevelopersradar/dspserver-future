using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.Storages;
using Settings;
using SharpExtensions;

namespace DataStorages
{
    using DataDictionary = ConcurrentDictionary<int, ImpulseSignal>;

    /// <inheritdoc />
    /// <summary>
    ///  Signal storage contains all discovered impulse signals for fhss search
    /// </summary>
    public class SignalStorage : ISignalStorage
    {
        private readonly List<DataDictionary> _data;

        private readonly IFilterManager _filterManager;

        private static int _scanCounter = 0;

        private readonly FhssSearchConfig _config;

        public SignalStorage(IFilterManager filterManager) 
            : this(filterManager, Config.Instance.FhssSearchSettings)
        { }

        public SignalStorage(IFilterManager filterManager, FhssSearchConfig config)
        {
            _config = config;
            _filterManager = filterManager;
            _data = new List<DataDictionary>(Config.Instance.BandSettings.BandCount);
            _data.AddRange(
                Config.Instance.BandSettings.BandCount.Repeat(() => new DataDictionary())
            );
            PeriodicTask.Run(CollectOldSignals, _config.OldFhssSignalCollectInterval);
        }

        /// <summary>
        /// This method is running in additional thread endlessly in cycle with Config.Instance.OldRadioSourcesCollectTimeSpan delay
        /// </summary>
        private void CollectOldSignals()
        {
            var oldSignals = new List<ImpulseSignal>();
            var now = DateTime.Now;
            var maxTimeSpan = _config.OldFhssSignalCollectTimeSpan;
            try
            {
                foreach (var dictionary in _data)
                {
                    oldSignals.Clear();
                    foreach (var valuePair in dictionary)
                    {
                        var source = valuePair.Value;
                        if (now - source.Time > maxTimeSpan)
                        {
                            oldSignals.Add(source);
                        }
                    }
                    RemoveSignals(dictionary, oldSignals);
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Collecting old signals error.");
            }
        }

        private static void RemoveSignals(DataDictionary dictionary, IEnumerable<ImpulseSignal> signals)
        {
            foreach (var source in signals)
            {
                dictionary.TryRemove(source.Id, out _);
            }
        }

        private DataDictionary GetDataDictionary(float frequency)
        {
            return _data[Utilities.GetBandNumber(frequency)];
        }

        public void Clear()
        {
            foreach (var dictionary in _data)
            {
                dictionary.Clear();
            }
        }

        public void Put(IEnumerable<ISignal> signals)
        {
            try
            {
                var scanIndex = Interlocked.Increment(ref _scanCounter);
                foreach (var signal in signals)
                {
                    Put(signal, scanIndex);
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Adding radio source to storage error.");
            }
        }

        public IEnumerable<ImpulseSignal> GetSignals()
        {
            var bands = _filterManager.Bands;
            if (bands.IsNullOrEmpty())
            {
                return new ImpulseSignal[0];
            }
            var from = bands[0];
            var to = bands[bands.Count - 1];
            if (from.Number == to.Number)
            {
                return GetSignals(from.Number);
            }
            var sourcesLists = new List<IEnumerable<ImpulseSignal>>(bands.Count);

            foreach (var band in bands)
            {
                sourcesLists.Add(GetSignals(band.Number));
            }
            return sourcesLists.SelectMany(_ => _);
        }

        private IEnumerable<ImpulseSignal> GetSignals(int index)
        {
            var dictionary = _data[index];
            var result = new List<ImpulseSignal>();
            foreach (var valuePair in dictionary)
            {
                if (_filterManager.IsInFilter(valuePair.Value.Signal))
                {
                    result.Add(valuePair.Value);
                }
            }
            return result;
        }

        private void Put(ISignal signal, int scanIndex)
        {
            var dictionary = GetDataDictionary(signal.FrequencyKhz);
            var radioSource = new RadioSource(signal);
            var impulseSignal = new ImpulseSignal(radioSource, scanIndex);
            dictionary.TryAdd(impulseSignal.Id, impulseSignal);
        }
    }
}