﻿namespace DataProcessor
{
    internal struct Range
    {
        public readonly int From;
        public readonly int To;

        public int CentralPointNumber => (To + From) / 2;

        public Range(int from, int to) : this()
        {
            From = from;
            To = to;
        }
    }
}