﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Storages;
using Phases;
using Settings;
using SharpExtensions;

namespace DataProcessor.Fhss
{
    public class PossibleFhssNetwork
    {
        public float MedianDirection => _directions.Count != 0 ? _directions.ElementAt(_directions.Count() / 2).Value : -1;

        public float StartFrequencyKhz { get; private set; } = float.MaxValue;
        public float EndFrequencyKhz { get; private set; } = float.MinValue;

        public int Count => _signals.Count;

        public IReadOnlyList<ImpulseSignal> Signals => _signals;

        private readonly SortedList<int, float> _directions;
        private readonly List<ImpulseSignal> _signals;

        public PossibleFhssNetwork(ImpulseSignal signal)
        {
            _directions = new SortedList<int, float>(5000);//todo : to constants
            _signals = new List<ImpulseSignal>(5000);
            AddSignal(signal);
        }

        public void AddSignal(ImpulseSignal impulseSignal)
        {
            if (StartFrequencyKhz > impulseSignal.Signal.FrequencyKhz)
                StartFrequencyKhz = impulseSignal.Signal.FrequencyKhz;

            if (EndFrequencyKhz < impulseSignal.Signal.FrequencyKhz)
                EndFrequencyKhz = impulseSignal.Signal.FrequencyKhz;

            _directions.Add(impulseSignal.Id, impulseSignal.Signal.Direction);
            _signals.Add(impulseSignal);
        }
    }

    public static class TestClass
    {
        public static IReadOnlyList<PossibleFhssNetwork> Test(IReadOnlyList<ImpulseSignal> signals)
        {
            var list = signals.ToList();
            list.Sort((a, b) => a.Signal.FrequencyKhz >= b.Signal.FrequencyKhz ? 1 : -1);

            var possibleFhssNetworks = new List<PossibleFhssNetwork>(20);
            var directionThreshold = Config.Instance.FhssSearchSettings.DirectionSector;
            var frequencyThresholdKhz = Config.Instance.FhssSearchSettings.RangeSectorKhz;
            var countThreshold = Config.Instance.FhssSearchSettings.FrequenciesFilterThreshold;
            foreach (var impulseSignal in list)
            {
                var network = possibleFhssNetworks.FirstOrDefault(n =>
                    PhaseMath.Angle(n.MedianDirection, impulseSignal.Signal.Direction) <= directionThreshold &&
                    Math.Abs(n.EndFrequencyKhz - impulseSignal.Signal.FrequencyKhz) <= frequencyThresholdKhz);
                if (network != null)
                {
                    network.AddSignal(impulseSignal);
                }
                else
                {
                    possibleFhssNetworks.Add(new PossibleFhssNetwork(impulseSignal));
                }
            }

            var result = possibleFhssNetworks.Where(n => n.Count > countThreshold).ToList();
            return result;
        }
    }

    public class FhssProcessor
    {
        private readonly IRadioSourceStorage _radioSourceStorage;
        private readonly FhssSearchConfig _config;
        //TODO : clear this
        public FhssProcessor(IRadioSourceStorage radioSourceStorage)
            : this(radioSourceStorage, Config.Instance.FhssSearchSettings)
        { }

        public FhssProcessor(IRadioSourceStorage radioSourceStorage, FhssSearchConfig config)
        {
            _config = config;
            _radioSourceStorage = radioSourceStorage;
        }

        public List<IFhssNetwork> FindNetworks(IReadOnlyList<ImpulseSignal> signals)
        {
            var now = DateTime.Now;
            var lastTimeActiveTimeThreshold = _config.FixedRadioSourceLastUpdateTimeThreshold;
            var activeTimeThreshold = _config.FixedRadioSourceTimeThreshold;

            var fixedRadioSources = _radioSourceStorage
                .GetRadioSources()
                .Where(r => now - r.LastActiveTime() < lastTimeActiveTimeThreshold &&
                            r.BroadcastTimeSpan >= activeTimeThreshold)
                .ToList();

            var signalsWoFrs = signals.ToList();
            foreach (var frs in fixedRadioSources)
            {
                signalsWoFrs = signalsWoFrs.Where(s => IsClose(s.Signal.FrequencyKhz, frs.FrequencyKhz) == false).ToList();
            }
            var networkRanges = TestClass.Test(signalsWoFrs);
            var netwroksignals = networkRanges.Select(r => r.Signals).SelectMany(ProcessNetworkRangeSignals);
            return networkRanges.Select(r => r.Signals).SelectMany(ProcessNetworkRangeSignals).Select(n => CreateFhssNetwork(n, fixedRadioSources)).ToList();

            bool IsClose(float frequency1, float frequency2)
            {
                return Math.Abs(frequency1 - frequency2) < 50;//todo ?_?_ to constants
            }
        }

        private IFhssNetwork CreateFhssNetwork(Network network, IReadOnlyList<IRadioSource> activeRadioSources)
        {
            var signals = network.Networks
                .SelectMany(s => s.Signals)
                .ToList();
            var minFrequency = (float)Math.Round(signals.Min(s => s.Signal.FrequencyKhz));
            var maxFrequency = (float)Math.Round(signals.Max(s => s.Signal.FrequencyKhz));
            var bandwidths = signals
                .Select(s => s.Signal.BandwidthKhz)
                .ToList();
            var bandwidth = bandwidths.QuickSelect(bandwidths.Count / 2);
            var step = GetStep(signals, _config.StepFrequencyThreshold);
            var frequencyCount = (int)Math.Ceiling((maxFrequency - minFrequency) / step);
            var amplitude = signals.Average(s => s.Signal.Amplitude);

            var fixedRadioSources = activeRadioSources
                .Where(r => network.Range.Contains(r.FrequencyKhz))
                .Select(r => new FixedRadioSource(
                        r.CentralFrequencyKhz,
                        r.Amplitude,
                        r.Direction,
                        r.GetIntersectionBandwidth()))
                .ToList();

            return new FhssNetwork(
                minFrequencyKhz: minFrequency,
                maxFrequencyKhz: maxFrequency,
                bandwidthKhz: bandwidth,
                stepKhz: step,
                impulseDurationMs: 0,
                isDurationMeasured: false,
                frequenciesCount: frequencyCount,
                fhssUserInfos: network.Networks.Select(n => n.UserInfo),
                fixedRadioSources: fixedRadioSources
            );
        }

        private float GetStep(IReadOnlyList<ImpulseSignal> networkSignals, float frequencyThreshold)
        {
            if (networkSignals.Count <= 3)
            {
                throw new ArgumentException("Can't find step for network with less than 3 signals");
            }
            var signals = networkSignals.ToList();
            signals.Sort((s1, s2) => s1.Signal.FrequencyKhz.CompareTo(s2.Signal.FrequencyKhz));

            var frequencies = new List<float>
            {
                signals[0].Signal.FrequencyKhz
            };

            for (var i = 1; i < signals.Count; ++i)
            {
                var delta = signals[i].Signal.FrequencyKhz - frequencies[frequencies.Count - 1];
                if (delta < frequencyThreshold)
                {
                    continue;
                }
                frequencies.Add(signals[i].Signal.FrequencyKhz);
            }

            // replace frequencies with frequency-deltas
            for (var i = 2; i < frequencies.Count; ++i)
            {
                frequencies[i - 2] = frequencies[i] - frequencies[i - 1];
            }
            // remove trash
            frequencies.RemoveRange(frequencies.Count - 2, 2);
            frequencies.Sort();

            float step = 1;
            // select median
            if(frequencies.Count != 0)                
                step = frequencies.QuickSelect(frequencies.Count / 2);

            return ApproximateStep(step);
        }

        private float ApproximateStep(float step)
        {
            var knownSteps = new[]
            {
                6.25f, 12.5f, 25, 50, 100 //todo : to constants
            };
            const float relativeThreshold = 0.15f;

            foreach (var knownStep in knownSteps)
            {
                var from = knownStep * (1 - relativeThreshold);
                var to = knownStep * (1 + relativeThreshold);
                if (step.IsInBounds(from, to))
                {
                    return knownStep;
                }
            }

            return (float)Math.Round(step);
        }

        private List<Network> ProcessNetworkRangeSignals(IReadOnlyList<ImpulseSignal> signals)
        {
            if (signals.Count < _config.NetworkMinSignalCount)
            {
                return new List<Network>();
            }

            var signalDirections = signals.Select(s => s.Signal.Direction).ToList();
            var directionSector = _config.DirectionSector;

            var directions = FhssDirectionFinder.CalculateDirections(
                signalDirections,
                directionSector,
                _config.DirectionGlobalRelativeThreshold,
                _config.DirectionLocalRelativeThreshold);

            var networks = new List<Network>();

            foreach (var direction in directions)
            {
                var directionSignals = signals
                    .Where(s => PhaseMath.Angle(s.Signal.Direction, direction) < directionSector)
                    .ToList();

                if (directionSignals.Count < _config.NetworkMinSignalCount)
                {
                    continue;
                }
                var directionNetwork = new DirectionNetwork(
                    signals: directionSignals,
                    direction: direction,
                    amplitude: directionSignals.Select(s => s.Signal.Amplitude).Average(),
                    standardDeviation: Signal.CalculateStandardDeviation(directionSignals.Select(s => s.Signal), Constants.ReceiverMinAmplitude));

                var network = networks.FirstOrDefault(n => n.IsInThisNetwork(directionNetwork));
                if (network == null)
                {
                    networks.Add(new Network(directionNetwork));
                }
                else
                {
                    network.AddToNetwork(directionNetwork);
                }
            }

            return networks;
        }
    }
}