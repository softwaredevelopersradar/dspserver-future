﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using Phases;
using Settings;

namespace DataProcessor.Fhss
{
    public class FhssNetwork : IFhssNetwork
    {
        public float MinFrequencyKhz { get; private set; }
        public float MaxFrequencyKhz { get; private set; }
        public float BandwidthKhz { get; private set; }
        public float StepKhz { get; private set; }
        public int FrequenciesCount { get; private set; }
        public float ImpulseDurationMs { get; private set; }
        public bool IsDurationMeasured { get; private set; }
        public int Id { get; }
        public bool IsActive { get; private set; }
        public DateTime LastUpdateTime { get; private set; }

        public IReadOnlyList<FhssUserInfo> UserInfo => _userInfo;
        public IReadOnlyDictionary<int, LinkedFhssUserInfo> LinkedUserInfo => _linkedUserInfo;
        public IReadOnlyList<FixedRadioSource> FixedRadioSources { get; private set; }

        private readonly List<FhssUserInfo> _userInfo;
        private readonly Dictionary<int, LinkedFhssUserInfo> _linkedUserInfo;
        private static int _idCounter = 0;

        public FhssNetwork(float minFrequencyKhz, float maxFrequencyKhz, float bandwidthKhz, float stepKhz, 
            float impulseDurationMs, bool isDurationMeasured, int frequenciesCount, IEnumerable<FhssUserInfo> fhssUserInfos, IEnumerable<FixedRadioSource> fixedRadioSources)
        {
            _linkedUserInfo = new Dictionary<int, LinkedFhssUserInfo>(10);//todo : to constants as max station coutn
            _userInfo = new List<FhssUserInfo>(10);

            MinFrequencyKhz = minFrequencyKhz;
            MaxFrequencyKhz = maxFrequencyKhz;
            BandwidthKhz = bandwidthKhz;
            StepKhz = stepKhz;
            ImpulseDurationMs = impulseDurationMs;
            FrequenciesCount = frequenciesCount;
            _userInfo.AddRange(fhssUserInfos ?? new FhssUserInfo[0]);
            FixedRadioSources = fixedRadioSources?.ToArray() ?? new FixedRadioSource[0];
            IsActive = true;
            IsDurationMeasured = isDurationMeasured;
            LastUpdateTime = DateTime.Now;
            Id = Interlocked.Increment(ref _idCounter);
        }

        public void Update(IFhssNetwork network)
        {
            if (network == null)
            {
                IsActive = false;
                return;
            }

            var comparisonResult = CompareNetworks(this, network);
            var newUserInfo = UserInfo.ToList();
            switch (comparisonResult)
            {
                case FhssComparisonResult.DifferentNetworks: return;//why would we even try to do that?
                case FhssComparisonResult.SameNetworksSameUser:
                    var usedIndecies = new List<int>(network.UserInfo.Count);
                    for (int i = 0; i < UserInfo.Count; i++)
                    {
                        for (int j = 0; j < network.UserInfo.Count; j++)
                        {
                            if(usedIndecies.Contains(j))
                                continue;
                            if (PhaseMath.Angle(UserInfo[i].Direction, network.UserInfo[j].Direction) <= Config.Instance.FhssSearchSettings.DirectionSector)
                            {
                                newUserInfo[i] = new FhssUserInfo(network.UserInfo[j].Amplitude, network.UserInfo[j].Direction, network.UserInfo[j].StandardDeviation); //update value
                                usedIndecies.Add(j);//remove it from consideration
                                break;
                            }
                        }
                    }
                    _userInfo.Clear();
                    _userInfo.AddRange(newUserInfo);
                    break;
                case FhssComparisonResult.SameNetworksDifferentUsers:
                    for (int i = 0; i < network.UserInfo.Count; i++)
                    {
                        var counter = 0;
                        for (int j = 0; j < UserInfo.Count; j++)
                        {
                            if (PhaseMath.Angle(UserInfo[j].Direction, network.UserInfo[i].Direction) <= Config.Instance.FhssSearchSettings.DirectionSector)
                            {
                                newUserInfo[i] = new FhssUserInfo(network.UserInfo[i].Amplitude, network.UserInfo[i].Direction, network.UserInfo[i].StandardDeviation); //update value
                                break;
                            }
                            counter++;
                        }
                        if (counter == UserInfo.Count)
                            newUserInfo.Add(new FhssUserInfo(network.UserInfo[i].Amplitude, network.UserInfo[i].Direction, network.UserInfo[i].StandardDeviation));
                    }

                    _userInfo.Clear();
                    _userInfo.AddRange(newUserInfo);
                    break;
            }

            IsActive = true;
            MinFrequencyKhz = network.MinFrequencyKhz;
            MaxFrequencyKhz = network.MaxFrequencyKhz;
            StepKhz = network.StepKhz;
            LastUpdateTime = network.LastUpdateTime;
            FixedRadioSources = network.FixedRadioSources;
            FrequenciesCount = network.FrequenciesCount;
            if (IsDurationMeasured == false) 
            {
                //no remeasuring when network changed, because network changes frequently and we don't want to remeasure all the time
                ImpulseDurationMs = network.ImpulseDurationMs;
                IsDurationMeasured = network.IsDurationMeasured;
            }
        }

        public void RemeasureDuration()
        {
            IsDurationMeasured = false;
        }

        /// <summary>
        /// This operation is not COMMUTATIVE! (means, a * b != b * a)! 
        /// Network2 is considered as a condidate to be a network1 - which is valid by default
        /// </summary>
        public static FhssComparisonResult CompareNetworks(IFhssNetwork network1, IFhssNetwork network2)
        {
            var range1 = new FrequencyRange(network1.MinFrequencyKhz, network1.MaxFrequencyKhz);
            var range2 = new FrequencyRange(network2.MinFrequencyKhz, network2.MaxFrequencyKhz);
            var areFrequenciesTheSame = FrequencyRange.IntersectionFactor(range1, range2) >= Constants.FhssIntersectionThreshold;
            if (!areFrequenciesTheSame)
                return FhssComparisonResult.DifferentNetworks;

            var network1Direction = network1.UserInfo.Select(info => info.Direction);
            var network2Direction = network2.UserInfo.Select(info => info.Direction);

            var areDirectionsTheSame = network2Direction
                .Select(d2 => network1Direction
                .Any(d1 => PhaseMath.Angle(d1, d2) <= Config.Instance.FhssSearchSettings.DirectionSector))
                .All(r => r);
            if (areDirectionsTheSame)
                return FhssComparisonResult.SameNetworksSameUser;
            else
                return FhssComparisonResult.SameNetworksDifferentUsers;
        }
    }
}
