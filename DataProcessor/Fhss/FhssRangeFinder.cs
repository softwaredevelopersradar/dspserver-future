﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;

namespace DataProcessor.Fhss
{
    public static class FhssRangeFinder
    {
        internal static List<FrequencyRange> FindRanges(
            IReadOnlyList<float> frequencies, 
            float sectorKhz = 500, 
            float relativeThreshold = 0.25f)
        {
            var minFrequency = frequencies.Min();
            var maxFrequency = frequencies.Max();
            var chart = CreateChart(frequencies, sectorKhz, minFrequency, maxFrequency);
            var threshold = Utils.GetThreshold(chart, relativeThreshold);
            
            var ranges = SplitInRanges(chart, threshold)
                .Select(CreateRange)
                .ToList();

            var result = new List<FrequencyRange> {ranges[0]};

            // merge intersected ranges
            for (var i = 1; i < ranges.Count; ++i)
            {
                var lastResult = result.Last();
                if (FrequencyRange.AreIntersected(lastResult, ranges[i]))
                {
                    result[result.Count - 1] = FrequencyRange.Union(lastResult, ranges[i]);
                }
                else
                {
                    result.Add(ranges[i]);
                }
            }

            return result;

            FrequencyRange CreateRange(Range indexRange)
            {
                // increase range size by one sector for each range side
                var startFrequency = minFrequency + (indexRange.From - 1) * sectorKhz;
                var endFrequency = minFrequency + (indexRange.To + 1) * sectorKhz;
                if (startFrequency < minFrequency)
                {
                    startFrequency = minFrequency;
                }
                if (endFrequency > maxFrequency)
                {
                    endFrequency = maxFrequency;
                }

                return new FrequencyRange(startFrequency, endFrequency);
            }
        }

        private static IEnumerable<Range> SplitInRanges(uint[] chart, uint threshold)
        {
            for (var i = 0; i < chart.Length; ++i)
            {
                if (chart[i] < threshold)
                {
                    continue;
                }

                var rangeEndIndex = GetRangeEndIndex(i);
                yield return new Range(i, rangeEndIndex);

                i = rangeEndIndex;
            }

            int GetRangeEndIndex(int rangeStartIndex)
            {
                for (var i = rangeStartIndex; i < chart.Length; ++i)
                {
                    if (chart[i] < threshold)
                    {
                        return i;
                    }
                }

                return chart.Length;
            }
        }

        private static uint[] CreateChart(IReadOnlyList<float> frequencies, float sectorKhz, float minFrequency, float maxFrequency)
        {
            var chartSize = (int) ((maxFrequency - minFrequency) / sectorKhz + 1);
            var chart = new uint[chartSize];

            foreach (var frequency in frequencies)
            {
                var index = (int) ((frequency - minFrequency) / sectorKhz);
                chart[index]++;
            }

            return chart;
        }
    }
}