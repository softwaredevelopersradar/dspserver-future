﻿using System.Collections.Generic;
using SharpExtensions;

namespace DataProcessor.Fhss
{
    internal static class Utils
    {
        internal static uint GetThreshold(IReadOnlyList<uint> chart, float relativeThreshold)
        {
            var index = (int) (chart.Count * relativeThreshold);
            return chart.QuickSelect(index);
        }
    }
}