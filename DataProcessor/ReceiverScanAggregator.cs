﻿using System.Collections.Generic;
using DspDataModel.Data;
using Settings;

namespace DataProcessor
{
    public class ReceiverScanAggregator : IReceiverScanAggregator
    {
        public IReadOnlyList<IReceiverScan> Scans => _scans;

        private int _bandLength;
        private readonly List<IReceiverScan> _scans;

        private readonly bool _flushOnBandNumberChanged;

        public ReceiverScanAggregator(bool flushOnBandNumberChanged)
        {
            _scans = new List<IReceiverScan>();
            _flushOnBandNumberChanged = flushOnBandNumberChanged;
        }

        public void AddScan(IReceiverScan scan)
        {
            if (_flushOnBandNumberChanged)
            {
                if (_scans.Count > 0 && scan.BandNumber != _scans[0].BandNumber)
                {
                    _scans.Clear();
                }
            }
            _scans.Add(scan);
            _bandLength = _scans[0].Count;
            Contract.Assert(scan.Count == _bandLength);
            Contract.Assert(scan.BandNumber == _scans[0].BandNumber);
        }

        public void Clear()
        {
            _scans.Clear();
        }

        public IAmplitudeScan GetSpectrum()
        {
            return Scans.MergeSpectrum();
        }
    }
}
