﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.DataProcessor;
using DspDataModel.Storages;
using Settings;

namespace DataProcessor
{
    public class LinkedRdfProcessor : ILinkedRdfProcessor
    {
        public event EventHandler<IReadOnlyList<IRadioSource>> SignalsProcessed;
        public event EventHandler<bool> OnStateChanged;

        private readonly Dictionary<int, List<IRadioSource>> _stationsDictionary = new Dictionary<int, List<IRadioSource>>(Constants.ApproximateMaxNumberOfStations);
        private readonly Dictionary<int, (int shardsToReceiveLeft, bool isWorking)> _stateDictionary = new Dictionary<int, (int shardsToReceiveLeft, bool isWorking)>(Constants.ApproximateMaxNumberOfStations);
        private readonly Dictionary<int, Stopwatch> _watchDictionary = new Dictionary<int, Stopwatch>(Constants.ApproximateMaxNumberOfStations);

        private readonly StationsConfig _config;
        private readonly object _lockObject;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private const int TimeoutCheckIntervalMs = 100;

        private bool _isWorking;
        
        public LinkedRdfProcessor()
        {
            _config = Config.Instance.StationsSettings;
            _lockObject = new object();
        }
        
        public void Initialize()
        {
            InitializeDictionaries();
            _isWorking = true;
            OnStateChanged?.Invoke(this, _isWorking);
            //PeriodicTask.Run(action: TimeOutCheck, 
            //    period: TimeSpan.FromMilliseconds(TimeoutCheckIntervalMs), 
            //    cancellationToken: _cancellationTokenSource.Token);
        }

        /// <summary>
        /// Initialize dictionaries based on number of shards we are going to use
        /// </summary>
        private void InitializeDictionaries(int numberOfShards = 1)
        {
            _stationsDictionary.Clear();
            _stateDictionary.Clear();
            //_watchDictionary.Clear();

            _stationsDictionary.Add(_config.OwnPosition.StationId, new List<IRadioSource>(Constants.ApproximateMaxNumberOfStations * 10));
            _stateDictionary.Add(_config.OwnPosition.StationId, (numberOfShards, true));
            //_watchDictionary.Add(_config.OwnPosition.StationId, new Stopwatch());

            foreach (var station in _config.LinkedPositions)
            {
                _stationsDictionary.Add(station.StationId, new List<IRadioSource>(Constants.ApproximateMaxNumberOfStations * 10));
                _stateDictionary.Add(station.StationId, (shardsToReceiveLeft : numberOfShards, true));
                //_watchDictionary.Add(station.StationId, new Stopwatch());
            }

            //foreach (var watch in _watchDictionary)
            //{
            //    watch.Value.Restart();
            //}
        }

        private void TimeOutCheck()
        {
            foreach (var watch in _watchDictionary)
            {
                if(watch.Value.IsRunning == false)
                    continue;

                if (watch.Value.ElapsedMilliseconds > Constants.LinkedRdfResultReceiveTimeout)
                {
                    watch.Value.Stop();
                    _stateDictionary[watch.Key] = (0, false);
                }
            }
        }
        
        public void Put(IReadOnlyList<IRadioSource> signals, int stationId)
        {
            if (_isWorking == false)
                return;

            lock(_lockObject)
            {
                try
                {
                    if (_stateDictionary[stationId].isWorking == false || 
                        _stateDictionary[stationId].shardsToReceiveLeft == 0)
                        return;
                    
                    _stationsDictionary[stationId].AddRange(signals.ToList());
                    _stateDictionary[stationId] = (
                        shardsToReceiveLeft: _stateDictionary[stationId].shardsToReceiveLeft - 1,
                        isWorking: true);
                    //_watchDictionary[stationId].Restart();

                    if (IsDataReadyForProcessing())
                        Process();
                }
                catch (Exception e)
                {
                    if (_stateDictionary.ContainsKey(stationId))
                    {
                        Disconnect(stationId);
                    }
                    else
                    {
                        MessageLogger.Warning($"there is no such key in dictionary : {stationId}");
                    }

                    MessageLogger.Warning($"error during linked rdf processor.put : {e.StackTrace}");
                }
            }
        }

        private void Disconnect(int stationId)
        {
            _stateDictionary[stationId] = (-1, false);
            //_watchDictionary[stationId].Stop();
            _stationsDictionary[stationId].Clear();
        }

        public void Stop()
        {
            lock(_lockObject)
            {
                _cancellationTokenSource.Cancel();
                _isWorking = false;

                _stateDictionary.Clear();
                _stationsDictionary.Clear();
                //_watchDictionary.Clear();
                OnStateChanged?.Invoke(this, _isWorking);
            }
        }

        private bool IsDataReadyForProcessing()
        {
            var workingStationsState = _stateDictionary.Where(state => state.Value.isWorking);
            if (workingStationsState.Count() == 1) // with only one station (our own) it is impossible to calculate coordinates 
            {
                _isWorking = false;
                OnStateChanged?.Invoke(this, _isWorking);
                return false;
            }

            return workingStationsState.All(state => state.Value.shardsToReceiveLeft == 0);
        }

        private void Process()
        {
            var results = new Dictionary<float, IRadioSource>();
            foreach (var station in _stationsDictionary)
            {
                foreach (var source in station.Value)
                {
                    var key = results.FirstOrDefault(f =>
                        Math.Abs(f.Key - source.FrequencyKhz) <
                        Config.Instance.DirectionFindingSettings.SignalsMergeGapKhz).Key;

                    if (key != 0)
                    {
                        results[key].UpdateLinkedStationDirection(station.Key, source.Direction, 1);
                    }
                    else
                    {
                        results.Add(source.FrequencyKhz, source);
                    }
                }
            }

            InitializeDictionaries();
            SignalsProcessed?.Invoke(
                this, results
                    .Where(s => s.Value.Latitude != -1 && s.Value.Longitude != -1)
                    .Select(p => p.Value)
                    .ToList());
        }
    }
}
