﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using Phases;
using Settings;

namespace DataProcessor
{
    public class Signal : ISignal
    {
        public float FrequencyKhz { get; }
        public float CentralFrequencyKhz { get; }
        public float Direction { get; }
        public float Reliability { get; }
        public float BandwidthKhz { get; }
        public float Amplitude { get; }
        public float StandardDeviation { get; }
        public float DiscardedDirectionsPart { get; }
        public float PhaseDeviation { get; }
        public SignalModulation Modulation { get; }
        public IReadOnlyList<float> Phases { get; }
        public TimeSpan BroadcastTimeSpan { get; }
        public float RelativeSubScanCount { get; }

        public Signal(
            float frequencyKhz, float centralFrequencyKhz, float direction, float reliability, 
            float bandwidthKhz, float amplitude, float standardDeviation, float discardedDirectionsPart, float phaseDeviation, float relativeSubScanCount,
            IReadOnlyList<float> phases, SignalModulation modulation, TimeSpan broadcastTimeSpan)
        {
            FrequencyKhz = frequencyKhz;
            CentralFrequencyKhz = centralFrequencyKhz;
            Direction = direction;
            Reliability = reliability;
            BandwidthKhz = bandwidthKhz;
            Amplitude = amplitude;
            StandardDeviation = standardDeviation;
            DiscardedDirectionsPart = discardedDirectionsPart;
            Phases = phases;
            PhaseDeviation = phaseDeviation;
            Modulation = modulation;
            RelativeSubScanCount = relativeSubScanCount;
            BroadcastTimeSpan = broadcastTimeSpan;
        }

        /// <summary>
        /// Simplified constructor for testing purpose only
        /// </summary>
        public Signal(float frequencyKhz, float amplitude, float direction = 0, float bandwidthKhz = 1)
        {
            FrequencyKhz = frequencyKhz;
            CentralFrequencyKhz = frequencyKhz;
            Direction = direction;
            Reliability = 1;
            BandwidthKhz = bandwidthKhz;
            Amplitude = amplitude;
            StandardDeviation = 0;
            DiscardedDirectionsPart = 0;
            Phases = new float[Constants.PhasesDifferencesCount];
            PhaseDeviation = 0;
            Modulation = SignalModulation.Unknown;
            BroadcastTimeSpan = TimeSpan.Zero;
            RelativeSubScanCount = 1;
        }

        public static List<ISignal> MergeSubScanSignals(IReadOnlyList<ISignal> signals, float threshold, int directionAveragingCount = 1, bool calculateHighQualityPhases = false)
        {
            var resultSignals = new List<ISignal>();

            for (var i = 0; i < signals.Count;)
            {
                var j = i + 1;
                for (; j < signals.Count; ++j)
                {
                    if (!SubScanShouldBeMerged(signals[j - 1], signals[j]))
                    {
                        break;
                    }
                }
                if (j == i + 1)
                {
                    resultSignals.Add(signals[i]);
                }
                else
                {
                    var signalsToMerge = new ListSegment<ISignal>(signals, i, j - i);
                    resultSignals.Add(MergeSignals(signalsToMerge, threshold, directionAveragingCount, calculateHighQualityPhases));
                }
                i = j;
            }

            return resultSignals;
        }

        private static ISignal MergeSignals(IReadOnlyList<ISignal> signalsToMerge, float threshold, int directionAveragingCount = 1, bool calculateHighQualityPhases = false)
        {
            if (signalsToMerge.Count == 0)
            {
                throw new ArgumentException("Can't merge empty list of signals");
            }
            var left = signalsToMerge[0].LeftFrequency();
            var right = signalsToMerge[signalsToMerge.Count - 1].RightFrequency();
            var centralFrequencyKhz = (left + right) / 2;

            var amplitude = signalsToMerge.Max(s => s.Amplitude);
            var reliabilty = signalsToMerge.Max(s => s.Reliability);
            var bestSignal = signalsToMerge.First(s => s.Reliability == reliabilty);

            var frequency = bestSignal.FrequencyKhz;
            var phaseDeviation = bestSignal.PhaseDeviation;
            var subScanCount = signalsToMerge.Sum(s => s.RelativeSubScanCount) / directionAveragingCount;
            var phases = bestSignal.Phases;
            var disardedDirectionsPart = signalsToMerge.Average(s => s.DiscardedDirectionsPart);
            var broadcastTimeSpan = signalsToMerge.Aggregate(TimeSpan.Zero, (time, s) => time + s.BroadcastTimeSpan);

            if (calculateHighQualityPhases && bestSignal.IsDirectionReliable())
            {
                var mergedPhases = new float[Constants.PhasesDifferencesCount];
                for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
                {
                    var phaseFactors = signalsToMerge
                        .Select(s => new PhaseFactor(s.GetMergeFactor(threshold), s.Phases[i])).ToArray();
                    mergedPhases[i] = PhaseMath.CalculatePhase(phaseFactors);
                }
                phases = mergedPhases;
            }

            var direction = 0f;
            var standardDeviation = 0f;
            if (reliabilty > Constants.ReliabilityThreshold)
            {
                var directionFactors = signalsToMerge
                    .Select(s => new PhaseFactor(s.GetMergeFactor(threshold), s.Direction))
                    .ToArray();
                direction = PhaseMath.CalculatePhase(directionFactors);
                standardDeviation = PhaseMath.StandardDeviation(directionFactors);
            }

            return new Signal(
                frequencyKhz: frequency,
                centralFrequencyKhz: centralFrequencyKhz,
                direction: direction,
                reliability: reliabilty,
                bandwidthKhz: right - left,
                amplitude: amplitude,
                discardedDirectionsPart: disardedDirectionsPart,
                standardDeviation: standardDeviation,
                phases: phases,
                relativeSubScanCount: subScanCount,
                modulation: bestSignal.Modulation,
                phaseDeviation: phaseDeviation,
                broadcastTimeSpan: broadcastTimeSpan);
        }

        /// <summary>
        /// Used for fhss networks, since impulse signals don't calculate it.
        /// Returns average deviation. 
        /// </summary>
        public static float CalculateStandardDeviation(IEnumerable<ISignal> signals, float threshold)
        {
            var directionFactors = signals
                .Where(signal => signal.Reliability > Constants.ReliabilityThreshold)
                .Select(s => new PhaseFactor(s.GetMergeFactor(threshold), s.Direction))
                .ToArray();
            return PhaseMath.StandardDeviation(directionFactors);
        }

        public static bool ShouldBeMerged(ISignal s1, ISignal s2)
        {
            var config = Config.Instance.DirectionFindingSettings;

            if (!s1.IsDirectionReliable() || !s2.IsDirectionReliable())
            {
                return SignalExtensions.Distance(s1, s2) < config.SignalsMergeGapKhz;
            }
            return SignalExtensions.Distance(s1, s2) < config.SignalsMergeGapKhz
                   && PhaseMath.Angle(s1.Direction, s2.Direction) < config.SignalsMergeDirectionDeviation;
        }

        public static bool SubScanShouldBeMerged(ISignal s1, ISignal s2)
        {
            return SignalExtensions.Distance(s1, s2) < Config.Instance.DirectionFindingSettings.SignalsMergeGapKhz;
        }
    }
}
