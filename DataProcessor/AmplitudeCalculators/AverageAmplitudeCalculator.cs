﻿using System.Numerics;
using DspDataModel.AmplitudeCalulators;
using Settings;

namespace DataProcessor.AmplitudeCalculators
{
    public class AverageAmplitudeCalculator : IAmplitudeCalculator
    {
        public AverageAmplitudeCalculator()
        { }

        public float[] CalculateAmplitudes(float[][] amplitudes)
        {
            var sumAmplitudes = new float[Constants.BandSampleCount];
            amplitudes[0].CopyTo(sumAmplitudes, 0);

            if (amplitudes.Length > 1)
            {
                for (var i = 1; i < amplitudes.Length; ++i)
                {
                    GetSumAmplitudes(sumAmplitudes, amplitudes[i]);
                }
                var multiplier = 1f / amplitudes.Length;
                for (var i = 0; i < Constants.BandSampleCount; ++i)
                {
                    sumAmplitudes[i] *= multiplier;
                }
            }

            return sumAmplitudes;
        }

        private void GetSumAmplitudes(float[] sumAmplitudes, float[] spectrum)
        {
            var to = (Constants.BandSampleCount / Vector<float>.Count) * Vector<float>.Count;
            for (var i = 0; i < to; i += Vector<float>.Count)
            {
                var s = new Vector<float>(sumAmplitudes, i);
                var s2 = new Vector<float>(spectrum, i);
                var res = s + s2;
                res.CopyTo(sumAmplitudes, i);
            }
            for (var i = to; i < Constants.BandSampleCount; ++i)
            {
                sumAmplitudes[i] += spectrum[i];
            }
        }
    }
}