using System.Numerics;
using DspDataModel;
using DspDataModel.AmplitudeCalulators;
using DspDataModel.Data;
using Settings;

namespace DataProcessor.AmplitudeCalculators
{
    /// <summary>
    /// Uses min calculation policy for noise part of spectrum, and max otherwise
    /// </summary>
    public class MinMaxAmplitudeCalculator : IAmplitudeCalculator
    {
        public MinMaxAmplitudeCalculator()
        { }

        public float[] CalculateAmplitudes(float[][] amplitudes)
        {
            var maxAmplitudes = new float[Constants.BandSampleCount];
            amplitudes[0].CopyTo(maxAmplitudes, 0);

            var minAmplitudes = new float[Constants.BandSampleCount];
            amplitudes[0].CopyTo(minAmplitudes, 0);

            if (amplitudes.Length > 1)
            {
                for (var i = 1; i < amplitudes.Length; ++i)
                {
                    GetMaxAmplitudes(maxAmplitudes, amplitudes[i]);
                    GetMinAmplitudes(minAmplitudes, amplitudes[i]);
                }
            }

            var threshold = maxAmplitudes.GetNoiseLevel() + Config.Instance.DirectionFindingSettings.AdditionalAdaptiveLevel;

            for (var i = 0; i < Constants.BandSampleCount; ++i)
            {
                if (maxAmplitudes[i] > threshold)
                {
                    minAmplitudes[i] = maxAmplitudes[i];
                }
            }

            return minAmplitudes;
        }

        void GetMaxAmplitudes(float[] maxAmplitudes, float[] spectrum)
        {
            var to = (Constants.BandSampleCount / Vector<float>.Count) * Vector<float>.Count;
            for (var i = 0; i < to; i += Vector<float>.Count)
            {
                var s = new Vector<float>(maxAmplitudes, i);
                var s2 = new Vector<float>(spectrum, i);
                var res = Vector.Max(s, s2);
                res.CopyTo(maxAmplitudes, i);
            }
            for (var i = to; i < Constants.BandSampleCount; ++i)
            {
                if (spectrum[i] > maxAmplitudes[i])
                {
                    maxAmplitudes[i] = spectrum[i];
                }
            }
        }

        void GetMinAmplitudes(float[] minAmplitudes, float[] spectrum)
        {
            var to = (Constants.BandSampleCount / Vector<float>.Count) * Vector<float>.Count;
            for (var i = 0; i < to; i += Vector<float>.Count)
            {
                var s = new Vector<float>(minAmplitudes, i);
                var s2 = new Vector<float>(spectrum, i);
                var res = Vector.Min(s, s2);
                res.CopyTo(minAmplitudes, i);
            }
            for (var i = to; i < Constants.BandSampleCount; ++i)
            {
                if (spectrum[i] < minAmplitudes[i])
                {
                    minAmplitudes[i] = spectrum[i];
                }
            }
        }
    }
}