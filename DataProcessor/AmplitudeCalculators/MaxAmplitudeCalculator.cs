using System.Numerics;
using DspDataModel.AmplitudeCalulators;
using Settings;

namespace DataProcessor.AmplitudeCalculators
{
    public class MaxAmplitudeCalculator : IAmplitudeCalculator
    {
        public MaxAmplitudeCalculator()
        { }

        public float[] CalculateAmplitudes(float[][] amplitudes)
        {
            var maxAmplitudes = new float[Constants.BandSampleCount];
            amplitudes[0].CopyTo(maxAmplitudes, 0);

            if (amplitudes.Length > 1)
            {
                for (var i = 1; i < amplitudes.Length; ++i)
                {
                    GetMaxAmplitudes(maxAmplitudes, amplitudes[i]);
                }
            }

            return maxAmplitudes;
        }

        void GetMaxAmplitudes(float[] maxAmplitudes, float[] spectrum)
        {
            var to = (Constants.BandSampleCount / Vector<float>.Count) * Vector<float>.Count;
            for (var i = 0; i < to; i += Vector<float>.Count)
            {
                var s = new Vector<float>(maxAmplitudes, i);
                var s2 = new Vector<float>(spectrum, i);
                var res = Vector.Max(s, s2);
                res.CopyTo(maxAmplitudes, i);
            }
            for (var i = to; i < Constants.BandSampleCount; ++i)
            {
                if (spectrum[i] > maxAmplitudes[i])
                {
                    maxAmplitudes[i] = spectrum[i];
                }
            }
        }
    }
}