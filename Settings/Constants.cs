﻿using System.Runtime.InteropServices;

namespace Settings
{
    public static class Constants
    {
        public const int ReceiversCount = 6;

        /// <summary>
        /// Direction finding receivers count
        /// </summary>
        public const int DfReceiversCount = 5;

        public const int PhasesDifferencesCount = DfReceiversCount * (DfReceiversCount - 1) / 2;

        public const int BandwidthKhz = 30000;

        public const float SamplesGapKhz = 1f * ReceiverBandwidthKhz / (ReceiverSampleCount - 1);

        /// <summary>
        /// Count of samples in one band that server take from whole receiver's band
        /// </summary>
        public const int BandSampleCount = (BandwidthKhz * (ReceiverSampleCount - 1) - ReceiverSampleCount + 1) / ReceiverBandwidthKhz + 2;

        public const int DirectionsCount = 360;

        public const int DirectionStep = 1;

        public const float ReliabilityThreshold = 0.5f;
        
        public const int UsrpFirstBandNumber = 100;

        /// <summary>
        /// Khz frequency of first sample in the first band
        /// </summary>
        public const int FirstBandMinKhz = 25000;

        public const int MinRadioJamFrequencyKhz = 30000;

        public const float ReceiverMinAmplitude = -130f;

        /// <summary>
        /// Receiver works inappropriately below this frequency
        /// </summary>
        public const int ReceiverMinWorkFrequencyKhz = 30_000;

        /// <summary>
        ///  Receiver works inappropriately above this frequency
        /// </summary>
        public const int ReceiverMaxWorkFrequencyKhz = 6_000_000;

        /// <summary>
        /// Time in which receiver "should" change band. Need for scan speed calculation
        /// </summary>
        public const float ReceiverBandChangeTime = 1.3f;

        public const float DefaultThresholdValue = -80f;

        /// <summary>
        /// Real receiver's bandwidth
        /// </summary>
        public const int ReceiverBandwidthKhz = 50000;

        /// <summary>
        /// How much samples do receivers really "see" in one band
        /// </summary>
        public const int ReceiverSampleCount = 16384;

        public const int FhssChannelsCount = 4;

        /// <summary>
        /// intersection factor threshold for two fhss networks equality
        /// </summary>
        public const float FhssIntersectionThreshold = 0.7f;

        /// <summary>
        /// Size of peak that is permanently cut off from all scans in intelligence and fhss jamming modes
        /// </summary>
        public const int CenterPeakSampleWidth = 21;

        /// <summary>
        /// Time needed for fpga to collect one sample from air
        /// </summary>
        private const int FpgaSampleCollectionTimeNs = 20;

        public const int FpgaScanCollectionTimeNs = FpgaSampleCollectionTimeNs * ReceiverSampleCount;

        /// <summary>
        /// If number of objectives during rdf is more than this, 
        /// rdf results will be sharded, so we won't wait too long for master processing. 
        /// </summary>
        public const int LinkedRdfShardSize = 8;

        /// <summary>
        /// This constant is used in signal gap calculations
        /// </summary>
        public const float SignalBandwidthToGapMultiplier = 0.25f;

        /// <summary>
        /// This constant is used in signal gap calculations
        /// </summary>
        public const float SignalMaxGapKhz = 300;

        /// <summary>
        /// If we didn't receive rdf result from linked station during this period,
        /// it will be considered "lost", and won't be used in calculations
        /// </summary>
        public const int LinkedRdfResultReceiveTimeout = 1500;

        public const int ApproximateMaxNumberOfStations = 5;

        public const int ExecutiveDfSlaveResponseTimeoutSec = 1000;

        /// <summary>
        /// After this time span ticket is considered to be expired
        /// </summary>
        public const int DatabaseTicketExpirationTimeSec = 5;

        /// <summary>
        /// When we received gps values in n-th time, and n > threshold, we update gps values
        /// even if they are hasn't changed. Because otherwise some programs won't receive correct data
        /// </summary>
        public const int UpdateGpsValuesThreshold = 10;

        public const int FhssMinImpulseDuration = 50;
        public const int FhssMaxImpulseDuration = 10_000_000;
    }
}
