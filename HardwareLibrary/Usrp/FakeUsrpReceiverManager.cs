﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using NetLib;
using Settings;
using SimulatorProtocols;

namespace HardwareLibrary.Usrp
{
    class FakeUsrpReceiverManager : IUsrpReceiverManager
    {
        private readonly NetClientSlim _simulatorClient;
        private readonly Config _config;
        private static int ScanIndexFactory;

        public bool IsConnected { get; private set; }
        public bool IsWorking { get; set; } = false;

        private short _frequencyMhz = 3010;
        public short FrequencyMhz
        {
            get => _frequencyMhz;
            set
            {
                if (value >= 100 && value <= 6010)
                    _frequencyMhz = value;
                else
                    MessageLogger.Warning($"Can't set usrp receiver's frequency to : {value} MHz");
            }
        }

        public int BandNumber
        {
            get => Utilities.GetBandNumber(FrequencyMhz * 1000);
            set
            {
                //setting to frequency to band's central frequency and parsing it to MHz value
                if (value <= Config.Instance.BandSettings.BandCount && value >= Constants.UsrpFirstBandNumber - 1)
                    FrequencyMhz = (short)((Utilities.GetBandMinFrequencyKhz(value) + Constants.BandwidthKhz / 2) / 1000);
                else
                    MessageLogger.Warning($"Can't set usrp receiver to band : {value}");
            }
        }

        private byte _amplification;
        public byte Amplification
        {
            get => _amplification;
            set
            {
                if (value >= 1 && value <= 70)
                    _amplification = value;
                if (value > 70 && value <= 120)
                    _amplification = 70;
                else
                    MessageLogger.Warning($"Can't set usrp receiver's amplification to : {value}");
            }
        }

        public float ScanSpeed { get; private set; }
        private readonly Stopwatch _scanSpeedWatch = new Stopwatch();
        
        public FakeUsrpReceiverManager(NetClientSlim simulatorClient, Config config)
        {
            _simulatorClient = simulatorClient;
            _config = config;
            _amplification = _config.HardwareSettings.UsrpSettings.Amplification;
        }

        public IAmplitudeScan GetScan()
        {
            _scanSpeedWatch.Restart();
            _simulatorClient.Write(UsrpDataRequest.ToBinary((byte)BandNumber, Amplification));
            var responseData = new byte[UsrpDataResponse.BinarySize];
            _simulatorClient.ReadExactBytesCount(responseData, 0, responseData.Length);
            var response = UsrpDataResponse.Parse(responseData);
            Interlocked.Increment(ref ScanIndexFactory);
            _scanSpeedWatch.Stop();

            var newSpeed = (float)((Constants.BandwidthKhz / 1000) / (_scanSpeedWatch.Elapsed.TotalMilliseconds));
            if (ScanSpeed != 0)
                ScanSpeed = ScanSpeed / 2 + newSpeed / 2;
            else
                ScanSpeed = newSpeed;
            return new AmplitudeScan(
                amplitudes: response.UsrpData.Amplitudes.Select(
                    a => a + Config.Instance.HardwareSettings.UsrpSettings.AddedLevel + Constants.ReceiverMinAmplitude)
                    .ToArray(), 
                bandNumber: BandNumber, 
                creationTime: DateTime.Now, 
                scanIndex: ScanIndexFactory);
        }

        public bool Initialize()
        {
            if(!_simulatorClient.IsWorking)
                _simulatorClient.Connect(_config.SimulatorSettings.SimulatorHost, _config.SimulatorSettings.SimulatorPort).Wait(2000);
            IsConnected = true;
            return true;
        }
    }
}
