﻿using DspDataModel.Data;
using DspDataModel.Hardware;
using System;

namespace HardwareLibrary.Usrp
{
    /// <summary>
    /// Mock class to ensure that, when we don't have usrp we don't get any exception
    /// Remove it after uspr will leave this project... 
    /// </summary>
    public class MockUsrpReceiverManager : IUsrpReceiverManager
    {
        public bool IsConnected => true;

        public float ScanSpeed => 0;

        public short FrequencyMhz { get; set; }
        public int BandNumber { get; set; }
        public byte Amplification { get; set; }

        public IAmplitudeScan GetScan()
        {
            return new AmplitudeScan(new float[0], 0, DateTime.Now, 0);
        }

        public bool Initialize()
        {
            return true;
        }
    }
}
