﻿using DspDataModel;
using DspDataModel.Hardware;
using HardwareLibrary.TransmitionChannels;
using ReceiversMessages;
using Settings;

namespace HardwareLibrary.Receivers
{
    public class Receiver : IReceiver
    {
        private int _bandNumber;
        public int Number { get; }

        public ITransmissionChannel DataChannel { get; }

        public int Frequency { get; private set; }

        public int BandNumber
        {
            get => _bandNumber;
            set
            {
                _bandNumber = value;
                if (BandNumber != -1)
                {
                    Frequency = Constants.FirstBandMinKhz + BandNumber * Constants.BandwidthKhz + Constants.BandwidthKhz / 2;
                }
            }
        }

        public Receiver(int number, ITransmissionChannel channel)
        {
            Number = number;
            DataChannel = channel;
            BandNumber = -1;
        }

        public void SetBandNumber(int bandNumber)
        {
            if (bandNumber == BandNumber)
                return;
            var message = new SetBandRequest((byte) (Number + 1), (byte) bandNumber);
            var response = DataChannel.SendReceive<SetBandResponse>(message.GetBytes(), SetBandResponse.BinarySize);
#if DEBUG
            if (!(DataChannel is FakeTransmitionChannel))
            {
                //Contract.Assert(response.BandNumber == bandNumber);
            }
#endif
            BandNumber = bandNumber;
        }

        public void SetFrequency(int frequencyMhz)
        {
            var frequency = frequencyMhz * 1000;
            if (frequency == Frequency)
            {
                return;
            }
            Contract.Assert(Constants.FirstBandMinKhz <= frequency && frequency <= Config.Instance.BandSettings.LastBandMaxKhz, "Trying to set receiver frequency not in working range");

            var message = new SetFrequencyRequest((byte)(Number + 1), (short) frequencyMhz);
            var response = DataChannel.SendReceive<SetFrequencyResponse>(message.GetBytes(), SetFrequencyResponse.BinarySize);

            Frequency = frequency;
            BandNumber = -1;
        }
    }
}
