﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel.Hardware;
using SignalGeneratorDrivers;

namespace HardwareLibrary.SignalGenerators
{
    public class SignalGenerator : ISignalGenerator
    {
        private const int CmdPauseMs = 50;

        public float FrequencyKhz
        {
            get => _deviceDriver.Frequency;
            set
            {
                _deviceDriver.SetFrequency(value);
                Thread.Sleep(CmdPauseMs);
            }
        }

        public float Level
        {
            get => _deviceDriver.Level;
            set
            {
                _deviceDriver.SetLevel(value);
                Thread.Sleep(CmdPauseMs);
            }
        }

        public bool EmitionEnabled
        {
            get => _deviceDriver.EmitionEnabled;
            set
            {
                _deviceDriver.EmitionEnabled = value;
                Thread.Sleep(CmdPauseMs);
            }
        }

        public bool IsInitialized => _deviceDriver.IsInitialized;

        private ISignalGeneratorDriver _deviceDriver;

        private readonly SignalGeneratorType _generatorType;

        public SignalGenerator(SignalGeneratorType signalGeneratorType)
        {
            _generatorType = signalGeneratorType;
        }

        /// <summary>
        /// Force reset of all generator params to current state
        /// </summary>
        public void Reset()
        {
            var currentFrequency = FrequencyKhz;
            var currentLevel = Level;
            var currentEmitionEnabled = EmitionEnabled;

            _deviceDriver.EmitionEnabled = false;
            _deviceDriver.SetLevel(_deviceDriver.MinLevel);
            _deviceDriver.SetFrequency(_deviceDriver.MinFrequency);

            _deviceDriver.SetFrequency(currentFrequency, true);
            _deviceDriver.SetLevel(currentLevel, true);
            _deviceDriver.SetEmitionEnabled(currentEmitionEnabled, true);
        }

        private ISignalGeneratorDriver CreateDriver(SignalGeneratorType signalGeneratorType)
        {
            switch (signalGeneratorType)
            {
                case SignalGeneratorType.Keysight:
                    return new KeysightDriver();
                case SignalGeneratorType.SG6000L:
                    return new Sg6000Driver();
                default:
                    throw new ArgumentOutOfRangeException(nameof(signalGeneratorType), signalGeneratorType, null);
            }
        }

        public async Task<bool> Initialize(string hostname)
        {
            if (_deviceDriver?.IsInitialized ?? false)
            {
                Close();
            }
            _deviceDriver = CreateDriver(_generatorType);

            var initTask = _deviceDriver.InitializeAsync(hostname);
            await Task.WhenAny(initTask, Task.Delay(TimeSpan.FromSeconds(3)));
            var result = initTask.IsCompleted && initTask.Result;
            if (result)
            {
                // generator is not responding correctly little time after initialization, so just add a small pause
                await Task.Delay(200);
            }
            return result;
        }

        public void Close()
        {
            _deviceDriver?.Close();
        }
    }
}
