﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using Settings;

namespace HardwareLibrary.FpgaDevices
{
    public class FpgaDevice : IFpgaDevice
    {
        public IntPtr DeviceHandler { get; private set; }
        
        public bool IsOpened { get; private set; }
        public bool IsFhssModeRunning { get; private set; } = false;

        private readonly int _deviceNumber;

        private readonly byte[] _inputBuffer;

        public int ChannelsCount { get; }

        private int _receiversSkipCount;

        private readonly FpgaDeviceConfig _deviceConfig;

        private readonly FpgaOffsetsSetup _offsets;

        private readonly FpgaDeviceBandsSettingsSource _bandsSettingsSource;

        private static readonly float[] _phasesArray;
        private static readonly float[] _reversedPhasesArray;

        private readonly int _deviceConfigIndex;

        private readonly object _deviceCloseLockObject = new object();

        static FpgaDevice()
        {
            _phasesArray = new float[ushort.MaxValue + 1];
            _reversedPhasesArray = new float[ushort.MaxValue + 1];

            for (var angle = 0; angle < _phasesArray.Length; ++angle)
            {
                _phasesArray[angle] = Phases.PhaseMath.NormalizePhase(180f * angle / 8192 + 180);
                _reversedPhasesArray[angle] = Phases.PhaseMath.NormalizePhase(180 - 180f * angle / 8192);
            }
        }

        public FpgaDevice(FpgaDeviceBandsSettingsSource bandsSettingsSource, FpgaOffsetsSetup fpgaOffsetsSetup, int deviceConfigIndex)
        {
            _bandsSettingsSource = bandsSettingsSource;
            _offsets = fpgaOffsetsSetup;
            _deviceConfigIndex = deviceConfigIndex;

            _deviceConfig = Config.Instance.HardwareSettings.DeviceSettings[deviceConfigIndex];
            _deviceNumber = _deviceConfig.Id;
            ChannelsCount = _deviceConfig.ChannelsCount; 
            _inputBuffer = new byte[Constants.ReceiverSampleCount * 3 * ChannelsCount];

            IsOpened = false;
        }

        public bool StartFhssMode(FhssSetup setup)
        {
            if (!ApplyFhssSetup(setup))
                return false;

            InitFpga(ChannelMode.Fhss, setup.FftSize);
            IsFhssModeRunning = true;
            return true;
        }

        public bool StartFhssDurationMeasurement(FhssSetup setup)
        {
            if (!ApplyFhssSetup(setup))
                return false;

            InitFpga(ChannelMode.FhssDurationMeasurement, setup.FftSize);
            IsFhssModeRunning = true;
            return true;
        }

        private bool ApplyFhssSetup(FhssSetup setup)
        {
            if (ReadWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44) != 0)
            {
                return false;
            }

            // set masks and thresholds and receivers settings
            for (var i = 0; i < setup.Targets.Count; ++i)
            {
                var threshold = setup.GetFpgaDeviceThreshold(i);
                var offset = (uint)(_offsets.CmdTblOff + 0x58 + 4 * i);
                WriteWord(_offsets.CmdTblBar, offset, threshold);

                var mask = setup.GetMask(i);
                var maskOffset = _offsets.CmdTblOff + 0x40000 + 0x800 * i;
                for (var j = 0; j < mask.Length; ++j)
                {
                    WriteWord(_offsets.CmdTblBar, (uint)(maskOffset + j * 4), mask[j]);
                }

                /*/set receivers settings
                switch (i)
                {
                    case 0:
                        if (setup.ReceiverSettings[i].ShouldBeReversed)
                        {
                            flag = flag.SetBit(20, 0); // setting 20-bit to 0
                        }
                        else
                        {
                            flag = flag.SetBit(20, 1); // setting 20-bit to 1
                        }
                        break;
                    case 1:
                        if (setup.ReceiverSettings[i].ShouldBeReversed)
                        {
                            flag = flag.SetBit(21, 0); // setting 21-bit to 0
                        }
                        else
                        {
                            flag = flag.SetBit(21, 1); // setting 21-bit to 1
                        }
                        break;
                    case 2:
                        if (setup.ReceiverSettings[i].ShouldBeReversed)
                        {
                            flag = flag.SetBit(22, 0); // setting 22-bit to 0
                        }
                        else
                        {
                            flag = flag.SetBit(22, 1); // setting 22-bit to 1
                        }
                        break;
                    case 3:
                        if (setup.ReceiverSettings[i].ShouldBeReversed)
                        {
                            flag = flag.SetBit(23, 0); // setting 23-bit to 0
                        }
                        else
                        {
                            flag = flag.SetBit(23, 1); // setting 23-bit to 1
                        }
                        break;
                }*/
            }
            var flag = GetAdcFlag();
            if (setup.ReceiverSettings[0].ShouldBeReversed)
            {
                flag = flag.SetBit(23, 0); // setting 23-bit to 0
            }
            else
            {
                flag = flag.SetBit(23, 1);  // setting 23-bit to 1
            }
            var result = WriteWordOptimized(2, 0x20000 + 0x08, flag);

            // send load mask command
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44, (uint)AdcCommands.LoadMask);
            for (var i = 0; i < 20; i++)
            {
                if (ReadWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44) != 0)
                {
                    Thread.Sleep(10);
                }
                else
                {
                    break;
                }
            }

            // send load threshold command
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44, (uint)AdcCommands.LoadThreshold);
            for (var i = 0; i < 20; i++)
            {
                if (ReadWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44) != 0)
                {
                    Thread.Sleep(10);
                }
                else
                {
                    break;
                }
            }

            return true;
        }

        public bool StopFhssMode()
        {
            InitFpga(ChannelMode.Intelligence, FftResolution.N16384);
            IsFhssModeRunning = false;
            return true;
        }

        public int GetFhssPeakIndex(int channelIndex)
        {
            Contract.Assert(channelIndex >= 0 && channelIndex <= Constants.FhssChannelsCount);

            // reading amplitude and sample number combined value
            var value = ReadWord(0, (uint)(0x10000 + 0x4004 + 4 * channelIndex));
            var sampleIndex = (short)(value >> 16) & 0x3FFF;
            var amplitudeValue = (short)(value & 0x0000ffff);
            return sampleIndex;
        }

        public int GetScanIndex()
        {
            return (int)ReadWord(0, 0x10000 + 0x4000);
        }

        public int GetDriverVersion()
        {
            return (int)ReadWord(0, 0x10000 + 0x4014);
        }

        private void SetChannelReceivers(int deviceConfigIndex)
        {
            var skip = 0;
            for (var i = 0; i < deviceConfigIndex; ++i)
            {
                skip += Config.Instance.HardwareSettings.DeviceSettings[i].ChannelsCount;
            }
            _receiversSkipCount = skip;
        }

        ~FpgaDevice()
        {
            Close();
        }

        public void Open()
        {
            if (IsOpened)
                return;
            IsOpened = true;
            DeviceHandler = DevApi.pdevOpen((uint)_deviceNumber);
        }

        public bool Initialize()
        {
            SetChannelReceivers(_deviceConfigIndex);

            var bufferSize = (uint)_inputBuffer.Length;

            var config = new DmaConfigin
            {
                Status = CmdStatus.RebootReq,
                DmaEngType = (byte)DmaEngType.AxicdmaSimple,
                WorkMode = (byte)WorkMode.Poll,
                OpCode = (byte)OpCode.ReadDDR,
                EngOps = (uint)EngOptions.DataBufPcMem,
                PCIBar = _offsets.AxiPcieBar,
                PCIOff = _offsets.AxiPcieOff,
                DMABar = _offsets.AxiCdmaBar,
                DMAOff = _offsets.AxiCdmaOff,
                AxiBarSize = _offsets.AxiBarSize,
                DmaBufAlignment = 0x1000,
                DmaSimpleTransferMaxSize = bufferSize,
                DmaSGTransferMaxSize = bufferSize,
                DmaBufNumber = 0,
                BarForDataBuf = 1,
                FromAddr = 0xC0000000 + 0x20000,
                ToAddr = 0x80000000,
                Length = bufferSize,
            };

            if (!DevApi.InitialiseTransfer(DeviceHandler, ref config))
            {
                return false;
            }
            if (config.Status == CmdStatus.RebootReq)
            {
                return false;
            }
            if (!DevApi.pStartTransfer(DeviceHandler))
            {
                return false;
            }

            return true;
        }

        public bool InitFpga(ChannelMode channelMode, FftResolution fftSize)
        {
            MessageLogger.Log($"Init fpga: mode is {channelMode}, Fft size is {fftSize}");

            //отладочная информация через USB
            var isLogEnabled = (uint)(Config.Instance.HardwareSettings.EnableFpgaUsbLog ? 1 : 0);
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x54, isLogEnabled);

            //установка режима каналов
            if (channelMode != ChannelMode.FhssDurationMeasurement)
                WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x4C, (uint)channelMode);
            else
                WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x4C, (uint)ChannelMode.Fhss);

            //установка размера бпф
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44, (uint)fftSize);
            Thread.Sleep(1);
            if (channelMode == ChannelMode.FhssDurationMeasurement)
            {
                WriteWord(2, 0x20000 + 0x08, GetAdcFlag().SetBit(19, 1));
            }

            for (var i = 0; i < 20; i++)
            {
                if (ReadWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44) != 0)
                {
                    Thread.Sleep(10);
                }
                else
                {
                    break;
                }
            }
            return true;
        }

        public bool InitFpgaTacting(TactingMode mode)
        {
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44, (uint)mode);
            for (var i = 0; i < 20; i++)
            {
                if (ReadWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44) != 0)
                {
                    Thread.Sleep(10);
                }
                else
                {
                    break;
                }
            }

            return true;
        }

        public uint GetAdcFlag()
        {
            return ReadWord(2, 0x20000 + 0x08);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public bool StartAdc(uint adcFlag)
        {
            adcFlag = adcFlag & 0xFFFFFFDF;
            return WriteWordOptimized(2, 0x20000 + 0x08, adcFlag);
        }

        public void Close()
        {
            if (!Config.Instance.HardwareSettings.CloseFpgaDevicesOnAppClose)
            {
                return;
            }
            lock (_deviceCloseLockObject)
            {
                if (IsOpened)
                {
                    IsOpened = false;
                    DevApi.pStopTransfer(DeviceHandler);
                    DevApi.pResetTransfer(DeviceHandler);
                    DevApi.DevClose(DeviceHandler);
                }
            }
        }

        public bool Read(byte[] buffer)
        {
            if (!IsOpened)
            {
                MessageLogger.Error("Can't read from closed fpga device!");
                return false;
            }

            if (MessageLogger.IsFpgaLogEnabled)
            {
                MessageLogger.FpgaLog($"<< Read block: device number: {_deviceNumber}, block size: {buffer.Length}");
            }
            return DevApi.pReadBlock(DeviceHandler, buffer, (uint)buffer.Length);
        }

        public byte[] Read()
        {
            return Read(_inputBuffer) ? _inputBuffer : null;
        }

        public bool WaitData()
        {
            uint val = 0xFF;
            const uint fftReady = (uint)ProcessingStatus.FftReady;

            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44, 0);

            const int maxWaitIterationCount = 500_000;

            for (var i = 0; val != fftReady; ++i)// бесконечный цикл проверки флага готовности БПФ ???
            {
                val = ReadWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x48);
                if (i > maxWaitIterationCount)
                {
                    var flag = GetAdcFlag();
                    MessageLogger.Error($"No response from fpga device number {_deviceNumber}, while waiting FFT ready flag. ADC flag value is {flag}");
                    break;
                }
            }

            // сброс флага готовности БПФ
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x48, (uint)ProcessingStatus.NotFinished);
            return (val & fftReady) != 0;
        }

        public bool ReadData(IFpgaDataScan scan, IReadOnlyList<int> bandNumbers)
        {
            var now = DateTime.UtcNow;
            if (!Read(_inputBuffer))
            {
                return false;
            }

            for (var i = 0; i < ChannelsCount; ++i)
            {
                if (bandNumbers[i + _receiversSkipCount] == -1)
                {
                    continue;
                }
                var amplitudesOffset = _deviceConfig.ChannelSettings[i].AmplitudesOffset;
                var phasesOffset = _deviceConfig.ChannelSettings[i].PhasesOffset;
                
                var data = GetReceiverScan(
                    _inputBuffer, 
                    amplitudesOffset, 
                    phasesOffset, 
                    bandNumbers[i + _receiversSkipCount], 
                    now,
                    scan.ScanIndex);
                scan.SetScan(i + _receiversSkipCount, data);
            }
            return true;
        }

        public bool InitializeFpgaNet()
        {
            WriteWord(_offsets.CmdTblBar, _offsets.CmdTblOff + 0x44, (byte)AdcCommands.NetInit);
            return true;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void WriteWord(byte bar, uint offset, uint data)
        {
            if (!IsOpened)
            {
                MessageLogger.Error("Can't write to closed fpga device!");
                return;
            }

            if (MessageLogger.IsFpgaLogEnabled)
            {
                MessageLogger.FpgaLog($">> Write word: bar: {bar}, offset: {0x20000 + offset}, data: {data:X}");
            }
            var result = DevApi.pWriteDword(DeviceHandler, bar, offset, data);
            if (!result)
            {
                MessageLogger.Error("Can't write to fpga device!");
            }
        }

        /// <summary>
        /// WriteWord version without any logging, for best performance of adc
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private bool WriteWordOptimized(byte bar, uint offset, uint data)
        {
            if (!IsOpened)
            {
                MessageLogger.Error("Can't write to closed fpga device!");
                return false;
            }

            return DevApi.pWriteDword(DeviceHandler, bar, offset, data);
        }

        public uint ReadWord(byte bar, uint offset)
        {
            if (!IsOpened)
            {
                MessageLogger.Error("Can't read from closed fpga device!");
                return 0;
            }
            
            var commandResult = DevApi.pReadDword(DeviceHandler, bar, offset, out var result);
            if (!commandResult)
            {
                MessageLogger.Error("Can't read from fpga device!");
            }
            if (MessageLogger.IsFpgaLogEnabled)
            {
                MessageLogger.FpgaLog($"<< Read word: bar: {bar}, offset: {0x20000 + offset}, result: {result:X}");
            }
            return result;
        }

        /// <summary>
        /// This method is a hack! TODO: remove it when Petya adds hardware realization
        /// </summary>
        private void RemoveCentralPeak(float[] amplitudes)
        {
            //optimal value found out by Arseniy
            const int side = 7;
            const int centerIndex = Constants.BandSampleCount / 2;
            //todo : refactor
            for (var i = centerIndex - side; i <= centerIndex + side; ++i)
            {
                if (i == centerIndex - 7 || i == centerIndex - 6 || i == centerIndex + 6 || i == centerIndex + 7)
                    amplitudes[i] -= 5;

                if (i == centerIndex - 5 || i == centerIndex + 5)
                    amplitudes[i] -= 8;

                if (i == centerIndex - 4 || i == centerIndex - 3 || i == centerIndex - 2 || i == centerIndex + 2 || i == centerIndex + 3 || i == centerIndex + 4)
                    amplitudes[i] -= 10;

                if (i == centerIndex - 1 || i == centerIndex + 1)
                    amplitudes[i] -= 13;

                if (i == centerIndex)
                    amplitudes[i] -= 30;

                if (amplitudes[i] < Constants.ReceiverMinAmplitude)
                    amplitudes[i] = Constants.ReceiverMinAmplitude;
            }
        }

        private IReceiverScan GetReceiverScan(
            byte[] buffer, 
            int amplitudesOffset, 
            int phasesOffset, 
            int bandNumber, 
            DateTime creationTime,
            int scanIndex)
        {
            // input array consists of 2 or 4 channels placed by scheme: A1A2 D1D1 D2D2 A3A4 D3D3 D4D4, each letter is 16 Kb of data
            // so A1A2 - is 32 Kb of data, amplitudes for 2 channels, D1D1 - also 32 Kb of data, phases for 1 channel and so on.

            var amplitudes = new float[Constants.BandSampleCount];
            var phases = new float[Constants.BandSampleCount];

            const int firstPartStart = Constants.ReceiverSampleCount - Constants.BandSampleCount / 2;
            const int secondPartEnd = (Constants.BandSampleCount + 1) / 2;

            int k = 0, shift = 1;
            var phasesArray = _phasesArray;
            if (_bandsSettingsSource.Settings[bandNumber].ShouldBeReversed)
            {
                shift = -1;
                k = Constants.BandSampleCount - 1;
                phasesArray = _reversedPhasesArray;
            }

            for (int i = secondPartEnd - 1; i >= 0; --i)
            {
                var phaseIndex = buffer[phasesOffset + i * 2] + 256 * buffer[phasesOffset + i * 2 + 1];
                phases[k] = phasesArray[phaseIndex];
                amplitudes[k] = buffer[i * 2 + amplitudesOffset] + Constants.ReceiverMinAmplitude;
                k += shift;
            }

            for (int i = Constants.ReceiverSampleCount - 1; i >= firstPartStart; --i)
            {
                var phaseIndex = buffer[phasesOffset + i * 2] + 256 * buffer[phasesOffset + i * 2 + 1];
                phases[k] = phasesArray[phaseIndex];
                amplitudes[k] = buffer[i * 2 + amplitudesOffset] + Constants.ReceiverMinAmplitude;
                k += shift;
            }
            RemoveCentralPeak(amplitudes);

            //hack
            if (bandNumber == Constants.UsrpFirstBandNumber - 1
                && Config.Instance.BandSettings.ReceiverUsrpSetup == ReceiverUsrpSetup.Receiver3GhzUsrp6Ghz)
            {
                for (int i = 1638; i < amplitudes.Length; i++)
                    amplitudes[i] = Constants.ReceiverMinAmplitude;
            }

            return new ReceiverScan(amplitudes, phases, bandNumber, creationTime, scanIndex);
        }

        public void DropFhssMeasurementFlag()
        {
            var adcFlag = GetAdcFlag().SetBit(18, 0);
            WriteWord(2, 0x20000 + 0x08, adcFlag);
        }
    }
}
