﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using DspDataModel;
using DspDataModel.Hardware;

namespace HardwareLibrary.TransmitionChannels
{
    public class Rs232TransmitionChannel : ITransmissionChannel
    {
        public bool IsConnected => _port != null && _port.IsOpen;

        public string PortName { get; private set; }

        public int BaudRate { get; private set; }

        public event EventHandler<IReadOnlyList<byte>> OnDataRead;

        public int Timeout { get; set; }

        private SerialPort _port;

        public Rs232TransmitionChannel(int timeout = 500)
        {
            Timeout = timeout;
        }

        /// <param name="args">string PortName, int baudRate</param>
        public bool Connect(params object[] args)
        {
            if (args.Length < 2)
                return false;
            if (args[0] is string port)
            {
                PortName = port;
            }
            else
            {
                return false;
            }
            if (args[1] is int baudRate)
            {
                BaudRate = baudRate;
            }
            else
            {
                return false;
            }

            if (IsConnected)
            {
                Disconnect();
            }
            try
            {
                _port = new SerialPort(PortName, BaudRate, Parity.None, 8, StopBits.One)
                {
                    ReadTimeout = Timeout,
                    WriteTimeout = Timeout
                };
                _port.Open();
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex);
                return false;
            }
            return true;
        }

        public void Disconnect()
        {
            if (IsConnected)
            {
                _port.Close();
            }
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            _port.ReadExisting();
            _port.Write(buffer, offset, count);
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            var readCount = 0;
            while (readCount < count)
            {
                readCount += _port.Read(buffer, offset + readCount, count - readCount);
            }
            OnDataRead?.Invoke(this, new ArraySegment<byte>(buffer, offset, readCount));
            return readCount;
        }
    }
}
