﻿using System;
using System.Collections.Generic;
using System.Linq;
using Phases;
using Settings;

namespace Correlator.CalibrationCorrection
{
    internal static class CalibrationCorrectionAlgorithms
    {
        private const float MinSignalsDirectionDelta = 5;

        private const float MinSignalsDistanceKhz = 5000;

        private const float MaxRestoreSignalDistanceKhz = 120_000;

        private const int MaxRestoreSignalDirectionDelta = 40;

        private static IEnumerable<CalibrationCorrectionSignal> RestoreSignalsByFrequency(CalibrationCorrectionSignal leftSignal, CalibrationCorrectionSignal rightSignal)
        {
            var distance = rightSignal.FrequencyKhz - leftSignal.FrequencyKhz;
            var direction = leftSignal.Direction;

            var restoreSignalCount = (int)((distance + MinSignalsDistanceKhz - 1) / MinSignalsDistanceKhz) - 1;
            if (restoreSignalCount <= 0)
            {
                yield break;
            }
            var startFrequency = leftSignal.FrequencyKhz + MinSignalsDistanceKhz;
            for (var i = 0; i < restoreSignalCount; ++i)
            {
                var frequency = startFrequency + i * MinSignalsDistanceKhz;
                var phases = new float[Constants.PhasesDifferencesCount];
                var alpha = (frequency - leftSignal.FrequencyKhz) / distance;

                for (var j = 0; j < Constants.PhasesDifferencesCount; ++j)
                {
                    var leftPhase = leftSignal.Phases[j];
                    var rightPhase = rightSignal.Phases[j];
                    var delta = PhaseMath.SignAngle(leftPhase, rightPhase);
                    phases[j] = PhaseMath.NormalizePhase(leftPhase + delta * alpha);
                }

                yield return new CalibrationCorrectionSignal((int) frequency, direction, phases);
            }
        }

        public static void RestorePeaksWithAveragePhase(IReadOnlyList<CalibrationCorrectionSignal> signals)
        {
            const int minSignalCount = 3;
            const float maxPhaseDeviation = 30;

            if (signals == null || signals.Count < minSignalCount)
            {
                return;
            }

            for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
            {
                var phases = signals.Select(s => s.Phases[i]).ToArray();
                var averagePhase = PhaseMath.CalculatePhase(phases, 40);
                for (var j = 0; j < phases.Length; ++j)
                {
                    if (PhaseMath.Angle(phases[j], averagePhase) > maxPhaseDeviation)
                    {
                        signals[j].Phases[i] = FindNearestGoodPhase(phases, averagePhase, j);
                    }
                }
            }

            float FindNearestGoodPhase(float[] phases, float averagePhase, int index)
            {
                for (var distance = 1; distance < 10; ++distance)
                {
                    var i = index - distance;
                    if (i < 0)
                    {
                        i = 0;
                    }
                    if (PhaseMath.Angle(phases[i], averagePhase) < maxPhaseDeviation)
                    {
                        return phases[i];
                    }
                    i = index + distance;
                    if (i >= phases.Length)
                    {
                        i = phases.Length - 1;
                    }
                    if (PhaseMath.Angle(phases[i], averagePhase) < maxPhaseDeviation)
                    {
                        return phases[i];
                    }
                }

                return averagePhase;
            }
        }

        public static void RestorePeaksWithLineRegression(IReadOnlyList<CalibrationCorrectionSignal> signals ,int unrollBlockSizeKhz)
        {
            const int minSignalCount = 3;
            const float maxPhaseDeviation = 20;

            if (signals == null || signals.Count < minSignalCount)
            {
                return;
            }
            var function = GetRegressionFunction(signals, 1, unrollBlockSizeKhz);

            RestoreEdgeSignal(signals[0]);
            RestoreEdgeSignal(signals.Last());

            for (var i = 1; i < signals.Count - 1; ++i)
            {
                RestoreEdgeSignal(signals[i]);
            }

            void RestoreEdgeSignal(CalibrationCorrectionSignal signal)
            {
                var linePhases = function(signal.FrequencyKhz);
                for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
                {
                    if (PhaseMath.Angle(linePhases[i], signal.Phases[i]) > maxPhaseDeviation)
                    {
                        signal.Phases[i] = PhaseMath.NormalizePhase(linePhases[i]);
                    }
                }
            }
        }

        public static List<CalibrationCorrectionSignal> RestoreSignalsByFrequency(IReadOnlyList<CalibrationCorrectionSignal> inputSignals)
        {
            if (inputSignals.Count == 0)
            {
                return new List<CalibrationCorrectionSignal>();
            }
            var signals = inputSignals.ToList();
            FilterSinglePeaksByFrequency(signals);

            var result = new List<CalibrationCorrectionSignal> { signals[0] };
            for (var i = 1; i < signals.Count; ++i)
            {
                var prevSignal = signals[i - 1];
                var signal = signals[i];

                var distance = signal.FrequencyKhz - prevSignal.FrequencyKhz;
                if (distance > MinSignalsDistanceKhz && distance <= MaxRestoreSignalDistanceKhz)
                {
                    result.AddRange(RestoreSignalsByFrequency(prevSignal, signal));
                }
                result.Add(signal);
            }
            return result;
        }

        public static List<CalibrationCorrectionSignal> RestoreSignalsByDirection(IReadOnlyList<CalibrationCorrectionSignal> signals)
        {
            var result = new List<CalibrationCorrectionSignal> { signals[0] };
            for (var i = 1; i < signals.Count; ++i)
            {
                var prevSignal = signals[i - 1];
                var signal = signals[i];

                var distance = PhaseMath.Angle(signal.Direction, prevSignal.Direction);
                if (distance >= MinSignalsDirectionDelta && distance <= MaxRestoreSignalDirectionDelta)
                {
                    result.AddRange(RestoreSignalsByDirection(prevSignal, signal));
                }
                result.Add(signal);
            }
            FilterSinglePeaksByDirection(result);
            return result;
        }

        public static List<CalibrationCorrectionSignal> SignalsRegression(IReadOnlyList<CalibrationCorrectionSignal> inputSignals, float startFrequencyKhz, float endFrequencyKhz, int unrollBlockSizeKhz)
        {
            const int minSignalCount = 3;
            const float stepKhz = 1000;

            if (inputSignals == null || inputSignals.Count < minSignalCount)
            {
                return new List<CalibrationCorrectionSignal>();
            }

            var regression = GetRegressionFunction(inputSignals, 3, unrollBlockSizeKhz);
            var direction = inputSignals[0].Direction;
            var result = new List<CalibrationCorrectionSignal>();
            var signalsCount = (int)((endFrequencyKhz - startFrequencyKhz) / stepKhz);

            for (var i = 0; i < signalsCount; ++i)
            {
                var frequency = startFrequencyKhz + stepKhz * i;
                var phases = regression(frequency);
                result.Add(new CalibrationCorrectionSignal((int) frequency, direction, phases));
            }

            return result;
        }

        /// <summary>
        /// restores the edges of the ranges where are no signals
        /// </summary>
        public static IReadOnlyList<CalibrationCorrectionSignal> RestoreRangeEdges(IReadOnlyList<CalibrationCorrectionSignal> inputSignals, float startFrequencyKhz, float endFrequencyKhz, int unrollBlockSizeKhz)
        {
            const int minSignalCount = 3;
            const float maxRestoreWidthKhz = 200_000;
            const float step = MinSignalsDistanceKhz;
            const float minEdgeWidth = MinSignalsDistanceKhz;

            if (inputSignals == null || inputSignals.Count < minSignalCount)
            {
                return new List<CalibrationCorrectionSignal>();
            }

            var firstSignal = inputSignals[0];
            var lastSignal = inputSignals.Last();

            if (firstSignal.FrequencyKhz - startFrequencyKhz < minEdgeWidth &&
                endFrequencyKhz - lastSignal.FrequencyKhz < minEdgeWidth)
            {
                // it means edges contain enough signals
                return inputSignals;
            }

            var regression = GetRegressionFunction(inputSignals, 1, unrollBlockSizeKhz);
            var direction = inputSignals[0].Direction;
            var result = new List<CalibrationCorrectionSignal>();

            var firstFrequency = startFrequencyKhz + MinSignalsDistanceKhz / 2;
            for (var frequency = firstFrequency; frequency < firstSignal.FrequencyKhz; frequency += step)
            {
                if (firstSignal.FrequencyKhz - frequency < maxRestoreWidthKhz)
                {
                    var phases = regression(frequency);
                    result.Add(new CalibrationCorrectionSignal((int) frequency, direction, phases));
                }
            }
            result.AddRange(inputSignals);

            var lastFrequency = lastSignal.FrequencyKhz + MinSignalsDistanceKhz / 2;
            for (var frequency = lastFrequency; frequency < endFrequencyKhz; frequency += step)
            {
                if (frequency - lastSignal.FrequencyKhz < maxRestoreWidthKhz)
                {
                    var phases = regression(frequency);
                    result.Add(new CalibrationCorrectionSignal((int) frequency, direction, phases));
                }
            }

            return result;
        }

        /// <summary>
        /// returns a function that takes frequency in khz and produces phases
        /// </summary>
        private static Func<float, float[]> GetRegressionFunction(IReadOnlyList<CalibrationCorrectionSignal> inputSignals, int polynomPower, int blockSizeKhz)
        {
            const int minSignalCount = 3;

            if (inputSignals == null || inputSignals.Count < minSignalCount)
            {
                return _ => new float[Constants.PhasesDifferencesCount];
            }

            var signals = SmartUnroll(inputSignals, blockSizeKhz);
            var xPoints = signals
                .Select(s => (double)s.FrequencyKhz)
                .ToArray();

            var functions = new Func<double, double>[10];

            for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
            {
                var yPoints = signals
                    .Select(s => (double)s.Phases[i])
                    .ToArray();
                functions[i] = MathNet.Numerics.Fit.PolynomialFunc(xPoints, yPoints, polynomPower);
            }

            return frequency => functions
                .Select(f => PhaseMath.NormalizePhase((float) f(frequency)))
                .ToArray();
        }

        private static IEnumerable<CalibrationCorrectionSignal> RestoreSignalsByDirection(CalibrationCorrectionSignal leftSignal, CalibrationCorrectionSignal rightSignal)
        {
            var distance = PhaseMath.Angle(rightSignal.Direction, leftSignal.Direction);
            var restoreSignalCount = (int)((distance + MinSignalsDirectionDelta - 1) / MinSignalsDirectionDelta) - 1;
            if (restoreSignalCount <= 0)
            {
                yield break;
            }
            var startDirection = leftSignal.Direction + MinSignalsDirectionDelta;
            var frequency = leftSignal.FrequencyKhz;

            for (var i = 0; i < restoreSignalCount; ++i)
            {
                var direction = startDirection + i * MinSignalsDirectionDelta;
                var phases = new float[Constants.PhasesDifferencesCount];
                var alpha = (direction - leftSignal.Direction) / distance;

                for (var j = 0; j < Constants.PhasesDifferencesCount; ++j)
                {
                    var leftPhase = leftSignal.Phases[j];
                    var rightPhase = rightSignal.Phases[j];
                    var delta = PhaseMath.SignAngle(leftPhase, rightPhase);
                    phases[j] = PhaseMath.NormalizePhase(leftPhase + delta * alpha);
                }

                yield return new CalibrationCorrectionSignal(frequency, (int) direction, phases);
            }
        }

        private static void FilterSinglePeaksByFrequency(IList<CalibrationCorrectionSignal> signals)
        {
            for (var i = 2; i < signals.Count; ++i)
            {
                var prevSignal = signals[i - 2];
                var signal = signals[i - 1];
                var nextSignal = signals[i];

                if (signal.FrequencyKhz - prevSignal.FrequencyKhz > MinSignalsDistanceKhz ||
                    nextSignal.FrequencyKhz - signal.FrequencyKhz > MinSignalsDistanceKhz)
                {
                    continue;
                }

                UpdatePhasesIfNeeded(prevSignal, signal, nextSignal);
            }
        }

        private static void FilterSinglePeaksByDirection(IList<CalibrationCorrectionSignal> signals)
        {
            for (var i = 2; i < signals.Count; ++i)
            {
                var prevSignal = signals[i - 2];
                var signal = signals[i - 1];
                var nextSignal = signals[i];

                if (PhaseMath.Angle(signal.Direction, prevSignal.Direction) > MinSignalsDirectionDelta ||
                    PhaseMath.Angle(nextSignal.Direction, signal.Direction) > MinSignalsDirectionDelta)
                {
                    continue;
                }

                UpdatePhasesIfNeeded(prevSignal, signal, nextSignal);
            }
        }

        private static void UpdatePhasesIfNeeded(CalibrationCorrectionSignal prevSignal, CalibrationCorrectionSignal signal, CalibrationCorrectionSignal nextSignal)
        {
            const float phaseDeviationThreshold = 45;
            const float maxExtraDeviation = 5;

            for (var j = 0; j < Constants.PhasesDifferencesCount; ++j)
            {
                var prevPhase = prevSignal.Phases[j];
                var phase = signal.Phases[j];
                var nextPhase = nextSignal.Phases[j];

                var sector = PhaseMath.Angle(prevPhase, nextPhase);
                if (sector < phaseDeviationThreshold)
                {
                    if (PhaseMath.Angle(prevPhase, phase) > sector + maxExtraDeviation ||
                        PhaseMath.Angle(nextPhase, phase) > sector + maxExtraDeviation)
                    {
                        var delta = PhaseMath.SignAngle(prevPhase, nextPhase);
                        signal.Phases[j] = PhaseMath.NormalizePhase(prevPhase + delta * 0.5f);
                    }
                }
            }
        }

        private static float[] Unroll(IReadOnlyList<float> phases)
        {
            var result = new float[phases.Count];
            result[0] = phases[0];

            for (var i = 1; i < phases.Count; ++i)
            {
                var phase = phases[i];
                var prevPhase = phases[i - 1];
                var delta = PhaseMath.SignAngle(prevPhase, phase);
                result[i] = result[i - 1] + delta;
            }

            return result;
        }

        public static List<CalibrationCorrectionSignal> SmartUnroll(IReadOnlyList<CalibrationCorrectionSignal> signals, int blockSizeKhz)
        {
            var signalDistance = 0f;
            for (var i = 1; i < signals.Count; ++i)
            {
                signalDistance += signals[i].FrequencyKhz - signals[i - 1].FrequencyKhz;
            }
            signalDistance /= (signals.Count - 1);
            var blockSize = (int) (blockSizeKhz / signalDistance);

            var result = new List<CalibrationCorrectionSignal>(signals.Count);
            result.AddRange(signals.Select(t => new CalibrationCorrectionSignal(
                t.FrequencyKhz,
                t.Direction,
                new float[Constants.PhasesDifferencesCount]))
            );

            for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
            {
                var phases = signals.Select(s => s.Phases[i]).ToArray();
                var unrolledPhases = SmartUnroll(phases, blockSize);
                for (var j = 0; j < result.Count; ++j)
                {
                    result[j].Phases[i] = unrolledPhases[j];
                }
            }

            return result;
        }

        private static float[] SmartUnroll(IReadOnlyList<float> phases, int blockSize)
        {
            if (phases.Count == 0)
            {
                return Array.Empty<float>();
            }

            var result = new float[phases.Count];

            var profile = CreateUnrollProfile(phases, blockSize);

            for (var i = 0; i < phases.Count; ++i)
            {
                var profileIndex = i / blockSize;
                result[i] = phases[i] + profile[profileIndex] * 360;
            }

            // update profile edges
            for (var i = 1; i < profile.Length; ++i)
            {
                if (profile[i] == profile[i - 1])
                {
                    continue;
                }
                var startIndex = (i - 1) * blockSize;
                var blockEndIndex = i * blockSize;
                var endIndex = (i + 1) * blockSize;
                if (endIndex > phases.Count)
                {
                    endIndex = phases.Count;
                }
                for (var j = startIndex + 1; j < endIndex; ++j)
                {
                    var prevPhase = result[j - 1];
                    var phase = result[j];
                    var profileValue = j < blockEndIndex ? profile[i] : profile[i - 1];
                    var alternativePhase = phases[j] + profileValue * 360;
                    if (Math.Abs(prevPhase - alternativePhase) < Math.Abs(prevPhase - phase))
                    {
                        result[j] = alternativePhase;
                    }
                }
            }

            // restore some peaks
            for (var i = 1; i < result.Length; ++i)
            {
                var prevPhase = result[i - 1];
                var phase = result[i];
                var altPhase1 = phase - 360;
                var altPhase2 = phase + 360;
                var distance = Math.Abs(prevPhase - phase);
                var distance1 = Math.Abs(prevPhase - altPhase1);
                var distance2 = Math.Abs(prevPhase - altPhase2);

                if (distance1 < distance)
                {
                    result[i] = altPhase1;
                }
                else if (distance2 < distance)
                {
                    result[i] = altPhase2;
                }
            }

            return result;
        }

        private static int[] CreateUnrollProfile(IReadOnlyList<float> phases, int blockSize)
        {
            var profileLength = (phases.Count + blockSize - 1) / blockSize;
            var result = new int[profileLength];
            var averagedPhases = new float[profileLength];

            for (var i = 0; i < result.Length; ++i)
            {
                var startIndex = i * blockSize;
                var endIndex = (i + 1) * blockSize;
                if (endIndex >= phases.Count)
                {
                    endIndex = phases.Count;
                }
                var phasesToAverage = phases.Skip(startIndex).Take(endIndex - startIndex).ToArray();
                averagedPhases[i] = PhaseMath.CalculatePhase(phasesToAverage);
            }
            var unrolledPhases = Unroll(averagedPhases);
            for (var i = 0; i < result.Length; ++i)
            {
                if (unrolledPhases[i] >= 0)
                {
                    result[i] = (int)(unrolledPhases[i] / 360);
                }
                else
                {
                    result[i] = (int)((unrolledPhases[i] - 360) / 360);
                }
            }
            
            return result;
        }
    }
}
