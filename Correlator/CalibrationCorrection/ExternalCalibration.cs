﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;

namespace Correlator.CalibrationCorrection
{
    internal class ExternalCalibration
    {
        private readonly Dictionary<int, DirectionCalibration> _calibrations;

        public IReadOnlyDictionary<int, DirectionCalibration> Calibrations => _calibrations;

        public ExternalCalibration(IReadOnlyList<CalibrationCorrectionSignal> signals)
        {
            _calibrations = new Dictionary<int, DirectionCalibration>();
            var directions = signals.Select(s => s.Direction)
                .Distinct()
                .ToArray();

            var directionCalibrations = directions.Select(direction => new DirectionCalibration(signals.Where(s => s.Direction == direction), direction));
            foreach (var directionCalibration in directionCalibrations)
            {
                _calibrations.Add(directionCalibration.Direction, directionCalibration);
            }
        }

        public int[] GetAvailableDirections()
        {
            return _calibrations.Keys.ToArray();
        }

        public int[] GetAvailableFrequencies()
        {
            return _calibrations
                .SelectMany(kvp => kvp.Value.Signals)
                .Select(s => s.FrequencyKhz)
                .Distinct()
                .OrderBy(f => f)
                .ToArray();
        }

        public void DataRegression(IReadOnlyList<FrequencyRange> ranges, IReadOnlyList<FrequencyRange> antennaRanges, IReadOnlyList<int> unrollBlockSizes)
        {
            MessageLogger.Trace("External calibration regression");

            const float rangeIncreaseFactor = 0.15f;

            var newCalibrations = new List<DirectionCalibration>();
            var antennaRangesList = antennaRanges.ToList();

            foreach (var directionCalibration in Calibrations)
            {
                var direction = directionCalibration.Key;
                var signals = directionCalibration.Value.Signals.ToArray();

                var newSignals = new List<CalibrationCorrectionSignal>();
                foreach (var range in ranges)
                {
                    var antennaIndex = antennaRangesList.FindIndex(r => r.Contains(range.CentralFrequencyKhz));

                    // increase range a little bit to improve regressed connection parts
                    var increasedRange = IncreaseRange(range);
                    var rangeSignals = signals
                        .Where(s => increasedRange.Contains(s.FrequencyKhz))
                        .ToArray();

                    newSignals.AddRange(CalibrationCorrectionAlgorithms.SignalsRegression(
                        rangeSignals, range.StartFrequencyKhz, range.EndFrequencyKhz, unrollBlockSizes[antennaIndex]));
                }

                var newCalibration = new DirectionCalibration(newSignals, direction);
                newCalibrations.Add(newCalibration);
            }

            foreach (var calibration in newCalibrations)
            {
                UpdateDirectionCalibration(calibration);
            }

            FrequencyRange IncreaseRange(FrequencyRange range)
            {
                var antennaRange = antennaRanges.First(r => r.Contains(range.CentralFrequencyKhz));
                var startFrequency = range.StartFrequencyKhz - range.RangeSizeKhz * rangeIncreaseFactor;
                var endFrequency = range.EndFrequencyKhz + range.RangeSizeKhz * rangeIncreaseFactor;

                if (startFrequency < antennaRange.StartFrequencyKhz)
                {
                    startFrequency = antennaRange.StartFrequencyKhz;
                }
                if (endFrequency > antennaRange.EndFrequencyKhz)
                {
                    endFrequency = antennaRange.EndFrequencyKhz;
                }

                return new FrequencyRange(startFrequency, endFrequency);
            }
        }

        public void FrequencyRestore(IEnumerable<FrequencyRange> ranges)
        {
            MessageLogger.Trace("External calibration frequency restore");
            var newDirectionCalibrations = Calibrations.Values
                .Select(c => c.FrequencyRestore(ranges))
                .ToArray();

            foreach (var calibration in newDirectionCalibrations)
            {
                UpdateDirectionCalibration(calibration);
            }
        }

        public void AveragePhaseFrequencyRestore(IEnumerable<FrequencyRange> ranges)
        {
            MessageLogger.Trace("External calibration frequency restore huge peaks");
            foreach (var calibration in Calibrations.Values)
            {
                calibration.AveragePhaseFrequencyRestore(ranges);
            }
        }

        public void LineRegressionFrequencyRestore(IEnumerable<FrequencyRange> ranges, int blockSizeKhz)
        {
            MessageLogger.Trace("External calibration frequency restore huge peaks");
            foreach (var calibration in Calibrations.Values)
            {
                calibration.LineRegressionFrequencyRestore(ranges, blockSizeKhz);
            }
        }

        public void RestoreRangeEdges(IEnumerable<FrequencyRange> ranges, IReadOnlyList<int> blockSizesKhz)
        {
            MessageLogger.Trace("External calibration range edges restore");

            var newDirectionCalibrations = Calibrations.Values
                .Select((c, i) => c.RestoreRangeEdges(ranges, blockSizesKhz))
                .ToArray();

            foreach (var calibration in newDirectionCalibrations)
            {
                UpdateDirectionCalibration(calibration);
            }
        }

        public void DirectionRestore()
        {
            MessageLogger.Trace("External calibration direction restore");
            var frequencies = GetAvailableFrequencies();

            foreach (var frequency in frequencies)
            {
                var signals = GetSignalsByFrequency(frequency);
                var newSignals = CalibrationCorrectionAlgorithms.RestoreSignalsByDirection(signals);
                UpdateFrequencyCalibration(newSignals);
            }
        }

        private void UpdateDirectionCalibration(DirectionCalibration calibration)
        {
            _calibrations[calibration.Direction] = calibration;
        }

        private void UpdateFrequencyCalibration(IEnumerable<CalibrationCorrectionSignal> signals)
        {
            foreach (var signal in signals)
            {
                var directionCalibration = _calibrations[signal.Direction];
                var oldSignal = directionCalibration.Signals.FirstOrDefault(s => s.FrequencyKhz == signal.FrequencyKhz);
                if (oldSignal != null)
                {
                    directionCalibration.Signals.Remove(oldSignal);
                }
                directionCalibration.Signals.Add(signal);
            }
        }

        public IReadOnlyList<CalibrationCorrectionSignal> GetSignalsByFrequency(int frequencyKhz)
        {
            var result = new List<CalibrationCorrectionSignal>();

            var directions = GetAvailableDirections();

            foreach (var direction in directions)
            {
                var calibration = _calibrations[direction];
                var signal = calibration.Signals.FirstOrDefault(s => s.FrequencyKhz == frequencyKhz);
                if (signal != null)
                {
                    result.Add(signal);
                }
            }
            result.Sort((s1, s2) => s1.Direction.CompareTo(s2.Direction));
            return result;
        }
    }
}