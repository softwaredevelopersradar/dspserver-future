﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;

namespace Correlator.CalibrationCorrection
{
    internal class DirectionCalibration
    {
        public SortedSet<CalibrationCorrectionSignal> Signals { get; }

        public int Direction { get; }

        public DirectionCalibration(IEnumerable<CalibrationCorrectionSignal> signals, int direction)
        {
            Direction = direction;
            Signals = new SortedSet<CalibrationCorrectionSignal>(
                signals.Where(s => s.Direction == direction),
                new HelperComparer<CalibrationCorrectionSignal>((s1, s2) => s1.FrequencyKhz.CompareTo(s2.FrequencyKhz))
            );
        }

        internal DirectionCalibration FrequencyRestore(IEnumerable<FrequencyRange> frequencyRanges)
        {
            var newSignals = new List<CalibrationCorrectionSignal>();
            foreach (var range in frequencyRanges)
            {
                var rangeSignals = Signals
                    .Where(s => range.Contains(s.FrequencyKhz))
                    .ToArray();
                newSignals.AddRange(CalibrationCorrectionAlgorithms.RestoreSignalsByFrequency(rangeSignals));
            }
            return new DirectionCalibration(newSignals, Direction);
        }

        internal void AveragePhaseFrequencyRestore(IEnumerable<FrequencyRange> frequencyRanges)
        {
            foreach (var range in frequencyRanges)
            {
                var rangeSignals = Signals
                    .Where(s => range.Contains(s.FrequencyKhz))
                    .ToArray();
                CalibrationCorrectionAlgorithms.RestorePeaksWithAveragePhase(rangeSignals);
            }
        }

        internal void LineRegressionFrequencyRestore(IEnumerable<FrequencyRange> frequencyRanges, int unrollBlockSize)
        {
            foreach (var range in frequencyRanges)
            {
                var rangeSignals = Signals
                    .Where(s => range.Contains(s.FrequencyKhz))
                    .ToArray();
                CalibrationCorrectionAlgorithms.RestorePeaksWithLineRegression(rangeSignals, unrollBlockSize);
            }
        }

        internal DirectionCalibration RestoreRangeEdges(IEnumerable<FrequencyRange> frequencyRanges, IReadOnlyList<int> unrollBlockSizes)
        {
            var newSignals = new List<CalibrationCorrectionSignal>();
            var index = 0;
            foreach (var range in frequencyRanges)
            {
                var rangeSignals = Signals
                    .Where(s => range.Contains(s.FrequencyKhz))
                    .ToArray();
                newSignals.AddRange(CalibrationCorrectionAlgorithms.RestoreRangeEdges(rangeSignals, range.StartFrequencyKhz, range.EndFrequencyKhz, unrollBlockSizes[index++]));
            }
            return new DirectionCalibration(newSignals, Direction);
        }
    }
}