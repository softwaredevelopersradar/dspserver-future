﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using Phases;
using RadioTables;
using Settings;

namespace Correlator.CalibrationCorrection
{
    public static class CalibrationCorrectionGenerator
    {
        public static CorrectionTable GenerateTable(IReadOnlyList<CalibrationCorrectionSignal> inputSignals)
        {
            var externalCalibration = new ExternalCalibration(inputSignals);

            var antennaRanges = CreateAntennaRanges();
            var unrollRanges = new[] {20_000, 25_000, 100_000, 100_000};

            externalCalibration.FrequencyRestore(antennaRanges);

            // restore 3 and 4 antenna range
            for (var i = 2; i < antennaRanges.Count; ++i)
            {
                externalCalibration.LineRegressionFrequencyRestore(CreateRegressionRanges(antennaRanges[i], 250_000), unrollRanges[i]);
                externalCalibration.AveragePhaseFrequencyRestore(CreateRegressionRanges(antennaRanges[i], 150_000));
            }
            
            externalCalibration.DirectionRestore();
            externalCalibration.RestoreRangeEdges(antennaRanges, unrollRanges);

            var regressionRanges = CreateRegressionRanges(antennaRanges);
            externalCalibration.DataRegression(regressionRanges, antennaRanges, unrollRanges);

            return CreateCorrectionTable(externalCalibration);
        }

        private static CorrectionTable CreateCorrectionTable(ExternalCalibration externalCalibration)
        {
            var table = new CorrectionTableDto
            {
                CorrectionDifferences = new List<CorrectionFrequencyDifferences>()
            };
            var theoreticalTable = new TheoreticalTable();
            var frequencies = externalCalibration.GetAvailableFrequencies();

            foreach (var frequency in frequencies)
            {
                var signals = externalCalibration.GetSignalsByFrequency(frequency);
                table.CorrectionDifferences.Add(GenerateCorrectionFrequencyDifferences(theoreticalTable, signals));
            }
            
            var correctionTable = new CorrectionTable(table, Config.Instance.BandSettings.BandCount);
            correctionTable.Save("testSaved.bin");
            SubtractTheoreticalTable(table);
            return correctionTable;
        }

        private static void SubtractTheoreticalTable(CorrectionTableDto correctionTable)
        {
            var theoreticalTable = new TheoreticalTable();

            foreach (var difference in correctionTable.CorrectionDifferences)
            {
                var theoreticalPhaseArrays = theoreticalTable.GetPhases(difference.FrequencyKhz);
                for (var i = 0; i < difference.Phases.Length; i++)
                {
                    var phaseArray = difference.Phases[i];
                    var theoreticalPhaseArray = theoreticalPhaseArrays[i];

                    for (var j = 0; j < phaseArray.Phases.Length; j++)
                    {
                        var phase = phaseArray.Phases[j];
                        phaseArray.Phases[j] = PhaseMath.NormalizePhase(phase - theoreticalPhaseArray.Phases[j]);
                    }
                }
            }
        }

        private static List<FrequencyRange> CreateAntennaRanges()
        {
            var previousRangeMaxFrequency = Constants.FirstBandMinKhz;
            var antennaRanges = new List<FrequencyRange>();

            foreach (var antennaConfig in Config.Instance.TheoreticalTableSettings.AntennaTableSettings)
            {
                antennaRanges.Add(new FrequencyRange(previousRangeMaxFrequency, antennaConfig.MaxFrequencyKhz - 1));
                previousRangeMaxFrequency = antennaConfig.MaxFrequencyKhz;
            }

            return antennaRanges;
        }

        private static List<FrequencyRange> CreateRegressionRanges(IEnumerable<FrequencyRange> antennaRanges)
        {
            var ranges = new List<FrequencyRange>();
            var maxBandSizesForAntenna = new[] // max band size for each antenna range
            {
                60_000, 30_000, 200_000, 200_000
            };
            var index = 0;
            foreach (var antennaRange in antennaRanges)
            {
                var maxBandSizeKhz = maxBandSizesForAntenna[index++];
                if (index == maxBandSizesForAntenna.Length)
                {
                    index = maxBandSizesForAntenna.Length - 1;
                }
                ranges.AddRange(CreateRegressionRanges(antennaRange, maxBandSizeKhz));
            }

            return ranges;
        }

        private static List<FrequencyRange> CreateRegressionRanges(FrequencyRange antennaRange, int maxBandSizeKhz)
        {
            var ranges = new List<FrequencyRange>();

            var bandCount = (int)((antennaRange.RangeSizeKhz + maxBandSizeKhz - 1) / maxBandSizeKhz);
            var bandSize = (int)(antennaRange.RangeSizeKhz / bandCount);

            for (var i = 0; i < bandCount; ++i)
            {
                var start = antennaRange.StartFrequencyKhz + bandSize * i;
                var end = antennaRange.StartFrequencyKhz + bandSize * (i + 1);
                ranges.Add(new FrequencyRange(start, end));
            }

            return ranges;
        }

        private static CorrectionFrequencyDifferences GenerateCorrectionFrequencyDifferences(TheoreticalTable theoreticalTable, IReadOnlyList<CalibrationCorrectionSignal> signals)
        {
            var emptyPhaseArray = new PhaseArray(new float[Constants.PhasesDifferencesCount]);
            var phaseArrays = Enumerable.Repeat(emptyPhaseArray, Constants.DirectionsCount).ToArray();

            for (var i = 0; i < signals.Count; ++i)
            {
                var s1 = signals[i];
                var s2 = i == signals.Count - 1
                    ? signals[0]
                    : signals[i + 1];
                GenerateInterpolatedPhasesArrays(s1, s2, phaseArrays);
            }
            return new CorrectionFrequencyDifferences(signals[0].FrequencyKhz, phaseArrays);
        }

        private static void GenerateInterpolatedPhasesArrays(CalibrationCorrectionSignal s1, CalibrationCorrectionSignal s2, PhaseArray[] phaseArrays)
        {
            var count = PhaseMath.Angle(s1.Direction, s2.Direction);
            var offset = s1.Direction;

            for (var i = 0; i < count; ++i)
            {
                var alpha = 1f * i / count;
                var phases = GenerateInterpolatedPhases(s1.Phases, s2.Phases, alpha);
                phaseArrays[(offset + i) % phaseArrays.Length] = new PhaseArray(phases);
            }
        }

        private static float[] GenerateInterpolatedPhases(float[] phases1, float[] phases2, float alpha)
        {
            var phases = new float[Constants.PhasesDifferencesCount];
            for (var j = 0; j < phases.Length; ++j)
            {
                var phaseFactors = new[]
                {
                    new PhaseFactor(1 - alpha, PhaseMath.NormalizePhase(phases1[j])),
                    new PhaseFactor(alpha, PhaseMath.NormalizePhase(phases2[j]))
                };
                phases[j] = PhaseMath.AveragePhase(phaseFactors);
            }

            return phases;
        }
    }
}
