﻿using System;
using System.Linq;
using Correlator.CalibrationCorrection;
using DspDataModel;
using Phases;
using RadioTables;
using Settings;

namespace Correlator
{
    public class TheoreticalTable : ITheoreticalTable
    {
        private readonly TheoreticalTableDto _table;

        public int MinFrequencyKhz => _table.AntennaTables[0].MinFrequencyKhz;
        public int MaxFrequencyKhz => _table.AntennaTables[_table.AntennaTables.Count - 1].MaxFrequencyKhz;
        public int StepKhz => _table.StepKhz;

        public TheoreticalTable(TheoreticalTableDto table)
        {
            _table = table;
        }

        public TheoreticalTable()
        {
            _table = TheoreticalTableGenerator.GenerateTable();
        }

        public TheoreticalTable(TheoreticalTableConfig config)
        {
            _table = TheoreticalTableGenerator.GenerateTable(config);
        }

        public TheoreticalTable(string radioPathTableFilename)
        {
            _table = new TheoreticalTableDto();
            _table.LoadFromFile(radioPathTableFilename);
        }

        public void Save(string filename)
        {
            _table.SaveToFile(filename);
        }

        /// <summary>
        /// returns if adding was successful
        /// </summary>
        public bool AddCorrectionTable(string filename)
        { 
            try
            {
                var correctionTable = new CorrectionTable(
                    Config.Instance.CalibrationSettings.CalibrationCorrectionFilename, 
                    Config.Instance.BandSettings.BandCount);

                foreach (var antennaTable in _table.AntennaTables)
                {
                    UpdateAntennaTable(antennaTable, correctionTable);
                }
            }
            catch (Exception)
            {
                MessageLogger.Warning("Can't load calibration correction table!");
                return false;
            }

            return true;

            void UpdateAntennaTable(AntennaTableDto antennaTable, CorrectionTable correctionTable)
            {
                for (var index = 0; index < antennaTable.TheoreticalDifferences.Count; ++index)
                {
                    try
                    {
                        var frequency = antennaTable.MinFrequencyKhz + StepKhz * index;
                        if (frequency >= Config.Instance.BandSettings.LastBandMaxKhz)
                        {
                            return;
                        }
                        var correctionPhases = correctionTable.GetPhases(frequency);

                        if (correctionPhases == null)
                        {
                            // no calibration signals in this band, so just skip it
                            continue;
                        }
                        var phases = antennaTable.TheoreticalDifferences[index];
                        for (var angle = 0; angle < Constants.DirectionsCount; ++angle)
                        {
                            for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
                            {
                                phases[angle].Phases[i] =
                                    PhaseMath.NormalizePhase(phases[angle].Phases[i] + correctionPhases[angle].Phases[i]);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        MessageLogger.Error(e);
                    }
                }
            }
        }

        public VisualTable ToVisualTable()
        {
            return new VisualTable(
                phasesFunc: GetPhases,
                nearestFreqFunc: GetNearestPhases,
                amplitudeFunc: fr => new float[Constants.DfReceiversCount], 
                minFrequencyKhz: MinFrequencyKhz,
                maxFrequencyKhz: MaxFrequencyKhz,
                valuesCount: _table.AntennaTables.Sum(a => a.TheoreticalDifferences.Count)
            );
        }

        private float GetNearestPhases(float frequencyKhz)
        {
            var antenntaIndex = 0;
            for (; antenntaIndex < _table.AntennaTables.Count; ++antenntaIndex)
            {
                if (frequencyKhz <= _table.AntennaTables[antenntaIndex].MaxFrequencyKhz)
                    break;
            }
            var antennaTable = _table.AntennaTables[antenntaIndex];

            var index = (int)((frequencyKhz - antennaTable.MinFrequencyKhz) / StepKhz);
            if (index == antennaTable.TheoreticalDifferences.Count)
            {
                index--;
            }
            if (index < antennaTable.TheoreticalDifferences.Count)
            {
                return antennaTable.MinFrequencyKhz + StepKhz * index;
            }
            else
            {
                throw new ArgumentException("Frequency " + frequencyKhz + " KHz is too high. Correlation Table contains frequencies only to " + MaxFrequencyKhz + " KHz.");
            }
        }

        public PhaseArray[] GetPhases(float frequencyKhz)
        {
            var antenntaIndex = 0;
            for (; antenntaIndex < _table.AntennaTables.Count; ++antenntaIndex)
            {
                if (frequencyKhz <= _table.AntennaTables[antenntaIndex].MaxFrequencyKhz)
                    break;
            }
            var antennaTable = _table.AntennaTables[antenntaIndex];

            var index = (int) ((frequencyKhz - antennaTable.MinFrequencyKhz) / StepKhz);
            if (index == antennaTable.TheoreticalDifferences.Count)
            {
                index--;
            }
            if (index < antennaTable.TheoreticalDifferences.Count)
            {
                return antennaTable.TheoreticalDifferences[index];
            }
            else
            {
                throw new ArgumentException("Frequency " + frequencyKhz + " KHz is too high. Correlation Table contains frequencies only to " + MaxFrequencyKhz + " KHz.");
            }
        }
    }
}
