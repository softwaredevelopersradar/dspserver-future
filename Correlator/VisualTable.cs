﻿using System;
using RadioTables;

namespace Correlator
{
    public class VisualTable : IVisualTable
    {
        public int MinFrequencyKhz { get; }
        public int MaxFrequencyKhz { get; }

        public int CurrentAngle { get; set; }

        public int ValuesCount { get; }

        private readonly Func<float, PhaseArray[]> _phaseFunction;
        private readonly Func<float, float[]> _amplitudeFunction;
        private readonly Func<float, float> _nearestFrequencyFunction;

        public VisualTable(Func<float, PhaseArray[]> phasesFunc, Func<float, float[]> amplitudeFunc, Func<float, float> nearestFreqFunc, int minFrequencyKhz, int maxFrequencyKhz, int valuesCount)
        {
            _phaseFunction = phasesFunc;
            _amplitudeFunction = amplitudeFunc;
            _nearestFrequencyFunction = nearestFreqFunc;
            MinFrequencyKhz = minFrequencyKhz;
            MaxFrequencyKhz = maxFrequencyKhz;
            ValuesCount = valuesCount;
        }

        public float GetPhaseDeviations(int bandNumber)
        {
            return 0;
        }

        public float[] FrequencyToPhases(float frequencyKhz)
        {
            return _phaseFunction(frequencyKhz)[CurrentAngle].Phases;
        }

        public float[] FrequencyToAmplitudes(float frequencyKhz)
        {
            return _amplitudeFunction(frequencyKhz);
        }

        public float GetNearestTableFrequency(float frequency)
        {
            return _nearestFrequencyFunction(frequency);
        }
    }
}
