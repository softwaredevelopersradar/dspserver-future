﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Settings;
using DspDataModel;
using Phases;
using RadioTables;

namespace Correlator
{
    public static class TheoreticalTableGenerator
    {
        private static readonly float Sin36 = PhaseMath.Sin(36);
        private static readonly float Cos54 = PhaseMath.Cos(54);

        private static float[,] _a;
        private static float[,] _b;
        private static float[,] _d;
        private static float[,] _c;

        internal static float[][] Factors
        {
            get => _factors ?? (_factors = GetDirectionFactors());
            private set => _factors = value;
        }

        private static bool _areMultipliersGenerated = false;
        private static float[][] _factors;

        public static TheoreticalTableDto GenerateTable()
        {
            return GenerateTable(Config.Instance.TheoreticalTableSettings);
        }

        private static void GenerateMultipliers()
        {
            GenerateMultipliers(Config.Instance.TheoreticalTableSettings);
        }

        private static void GenerateMultipliers(TheoreticalTableConfig settings)
        {
            if (_areMultipliersGenerated)
            {
                return;
            }
            _areMultipliersGenerated = true;

            var radiusCount = settings.AntennaTableSettings.Length;
            _a = new float[radiusCount, 360];
            _b = new float[radiusCount, 360];
            _c = new float[radiusCount, 360];
            _d = new float[radiusCount, 360];

            for (var i = 0; i < radiusCount; ++i)
            {
                var radius = settings.AntennaTableSettings[i].Radius;
                var aMultiplier = -2 * radius * Sin36;
                var bcdMultipluer = 2 * radius * Cos54;

                for (var angle = 0; angle < 360; ++angle)
                {
                    _a[i, angle] = aMultiplier * PhaseMath.Sin(PhaseMath.NormalizePhase(36 - angle));
                    _b[i, angle] = bcdMultipluer * PhaseMath.Cos(PhaseMath.NormalizePhase(angle - 18)) - _a[i, angle];
                    _d[i, angle] = bcdMultipluer * PhaseMath.Sin(PhaseMath.NormalizePhase(72 - angle));
                    _c[i, angle] = bcdMultipluer * PhaseMath.Sin(angle) - _d[i, angle];
                }
            }

            Factors = GetDirectionFactors();
        }

        public static TheoreticalTableDto GenerateTable(TheoreticalTableConfig settings)
        {
            GenerateMultipliers(settings);
            var table = new TheoreticalTableDto(settings.StepKhz, new List<AntennaTableDto>());

            var prevMaxFrequency = Constants.FirstBandMinKhz;
            for (var i = 0; i < settings.AntennaTableSettings.Length; i++)
            {
                var setup = settings.AntennaTableSettings[i];
                var antennaTable = GenerateAntennaTable(prevMaxFrequency, setup.MaxFrequencyKhz, i,
                    settings.StepKhz);
                table.AntennaTables.Add(antennaTable);
                prevMaxFrequency = setup.MaxFrequencyKhz;
            }

            return table;
        }

        private static AntennaTableDto GenerateAntennaTable(int minFreq, int maxFreq, int radiusIndex, int stepKhz)
        {
            var count = (maxFreq - minFreq) / stepKhz + 1;
            var table = new AntennaTableDto(maxFreq, minFreq, new List<PhaseArray[]>());
            var phasesArrays = new PhaseArray[count][];

            Parallel.For(0, count, i =>
            {
                var freq = minFreq + i * stepKhz;
                var phases = new PhaseArray[360];
                for (var angle = 0; angle < 360; angle += Constants.DirectionStep)
                {
                    phases[angle] = new PhaseArray(GetPhases(radiusIndex, angle, freq));
                }
                phasesArrays[i] = phases;
            });
            table.TheoreticalDifferences.AddRange(phasesArrays);

            return table;
        }

        private static float[][] GetDirectionFactors()
        {
            var phases = new float[Constants.DirectionsCount, Constants.PhasesDifferencesCount];
            var factors = new float[Constants.DirectionsCount][];
            for (var i = 0; i < Constants.DirectionsCount; ++i)
            {
                factors[i] = new float[Constants.PhasesDifferencesCount];
            }

            for (var angle = 0; angle < Constants.DirectionsCount; ++angle)
            {
                var a = _a[0, angle];
                var b = _b[0, angle];
                var d = _d[0, angle];
                var c = _c[0, angle];

                phases[angle, 0] = -a;
                phases[angle, 1] = b;
                phases[angle, 2] = b + c + d;
                phases[angle, 3] = b + c;
                phases[angle, 4] = a + b;
                phases[angle, 5] = a + b + c + d;
                phases[angle, 6] = a + b + c;
                phases[angle, 7] = c + d;
                phases[angle, 8] = c;
                phases[angle, 9] = -d;
            }

            var maxFactor = 0f;
            // magic number that's found with a deep two-day research
            const float factorExtra = 0.23f;

            for (var angle = 0; angle < Constants.DirectionsCount; ++angle)
            {
                var factorSum = 0f;
                for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
                {
                    var p = Math.Abs(phases[angle, i]);
                    var factor = 1 / (p + factorExtra);

                    factors[angle][i] = factor;
                    factorSum += factor;
                    maxFactor = Math.Max(maxFactor, p);
                }
                for (var i = 0; i < Constants.PhasesDifferencesCount; ++i)
                {
                    factors[angle][i] /= factorSum;
                }
            }

            return factors;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float[] GetPhases(int radiusIndex, int angle, int freqKhz)
        {
            const float speedOfLight = 299792458f;
            var inversedLength = (freqKhz / speedOfLight) * 2 * 180 * -1000;
            var phases = new float[Constants.PhasesDifferencesCount];
            
            var a = _a[radiusIndex, angle];
            var b = _b[radiusIndex, angle];
            var d = _d[radiusIndex, angle];
            var c = _c[radiusIndex, angle];

            phases[0] = -1 * a * inversedLength;
            phases[1] = b * inversedLength;
            phases[2] = (b + c + d) * inversedLength;
            phases[3] = (b + c) * inversedLength;
            phases[4] = (a + b) * inversedLength;
            phases[5] = (a + b + c + d) * inversedLength;
            phases[6] = (a + b + c) * inversedLength;
            phases[7] = (c + d) * inversedLength;
            phases[8] = c * inversedLength;
            phases[9] = -1 * d * inversedLength;
            
            for (var j = 0; j < Constants.PhasesDifferencesCount; j++)
            {
                phases[j] = PhaseMath.NormalizePhase(phases[j]);
            }
            
            return phases;
        }
    }
}
