namespace RadioTables
{
	struct PhaseArray
	{
		float[10] Phases
	}

	struct AmplitudeArray
	{
		float[5] Amplitudes
	}

	class BandRadioPath
	{
		list<PhaseArray> RadioPath
		list<AmplitudeArray> Amplitudes
		float PhaseDeviation
	}

	class RadioPathTableDto
	{
		int MinFrequencyKhz
		int MaxFrequencyKhz
		list<BandRadioPath> BandRadioPathCalibrations
	}

	class AntennaTableDto
	{
		int MaxFrequencyKhz
		int MinFrequencyKhz
		list<PhaseArray[360]> TheoreticalDifferences
	}

	class TheoreticalTableDto
	{
		int StepKhz
		list<AntennaTableDto> AntennaTables
	}

	class CorrectionFrequencyDifferences
	{
		int FrequencyKhz
		PhaseArray[360] Phases
	}

	class CorrectionTableDto
	{
		list<CorrectionFrequencyDifferences> CorrectionDifferences
	}
}