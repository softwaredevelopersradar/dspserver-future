﻿namespace Correlator
{
    public interface IVisualTable
    {
        int MinFrequencyKhz { get; }
        int MaxFrequencyKhz { get; }
        int ValuesCount { get; }
        int CurrentAngle { get; set; }

        float GetPhaseDeviations(int bandNumber);
        float[] FrequencyToPhases(float frequencyKhz);
        float[] FrequencyToAmplitudes(float frequencyKhz);
        float GetNearestTableFrequency(float frequency);
    }
}
