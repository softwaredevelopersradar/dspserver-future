﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Settings;
using DspDataModel;

namespace Simulator
{
    public class FhssSignal
    {
        public int StartFrequencyKhz
        {
            get { return _startFrequencyKhz; }
            set
            {
                if (value < Constants.FirstBandMinKhz)
                {
                    value = Constants.FirstBandMinKhz;
                }
                _startFrequencyKhz = value; 
                UpdateSignals();
            }
        }

        public int EndFrequencyKhz
        {
            get { return _endFrequencyKhz; }
            set
            {
                if (value > Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    value = Config.Instance.BandSettings.LastBandMaxKhz;
                }
                _endFrequencyKhz = value;
                UpdateSignals();
            }
        }

        public int JumpsPerSecond
        {
            get { return _jumpsPerSecond; }
            set
            {
                _jumpsPerSecond = value; 
                UpdateSignals();

            }
        }

        public int StepKhz
        {
            get { return _stepKhz; }
            set
            {
                _stepKhz = value; 
                UpdateSignals();
            }
        }

        public int Direction
        {
            get { return _direction; }
            set
            {
                _direction = value; 
                UpdateSignals();
            }
        }

        public float Amplitude
        {
            get { return _amplitude; }
            set
            {
                _amplitude = value; 
                UpdateSignals();
            }
        }

        public SignalSimulator Simulator;

        public List<Signal> CurrentSignals;

        private readonly List<Signal> _signals;
        private int _startFrequencyKhz;
        private int _endFrequencyKhz;
        private int _stepKhz;
        private int _direction;

        private readonly Random _random;
        private readonly Stopwatch _stopwatch;
        private int _jumpsPerSecond;
        private float _amplitude;

        public FhssSignal()
        {
            _signals = new List<Signal>();
            _startFrequencyKhz = 30000;
            _endFrequencyKhz = 50000;
            _jumpsPerSecond = 100;
            _stepKhz = 25;
            _direction = 0;
            _amplitude = -65;
            _random = new Random((int)(DateTime.Now.Ticks ^ DateTime.Now.Millisecond));
            _stopwatch = Stopwatch.StartNew();
            CurrentSignals = new List<Signal>();
            UpdateSignals();
        }

        public void SetSimulator(SignalSimulator simulator)
        {
            Simulator = simulator;
            UpdateSignals();
        }

        public FhssSignal(int startFrequencyKhz, int endFrequencyKhz, int jumpsPerSecond, int stepKhz, int direction, float amplitude)
        {
            _signals = new List<Signal>();
            _startFrequencyKhz = startFrequencyKhz;
            _endFrequencyKhz = endFrequencyKhz;
            _jumpsPerSecond = jumpsPerSecond;
            _stepKhz = stepKhz;
            _direction = direction;
            _amplitude = amplitude;
            _random = new Random((int) (DateTime.Now.Ticks ^ DateTime.Now.Millisecond));
            _stopwatch = Stopwatch.StartNew();
            CurrentSignals = new List<Signal>();
            UpdateSignals();
        }

        public void Update()
        {
            _stopwatch.Stop();
            var signalsCount = (int) (0.001 * _stopwatch.Elapsed.TotalMilliseconds * JumpsPerSecond);
            if (signalsCount == 0)
            {
                signalsCount++;
            }
            else if (signalsCount > _signals.Count)
            {
                CurrentSignals = _signals;
                _stopwatch.Restart();
                return;
            }
            CurrentSignals = new List<Signal>(signalsCount);
            if (_signals.Count > 0)
            {
                for (var i = 0; i < signalsCount; ++i)
                {
                    var index = _random.Next(0, _signals.Count);
                    CurrentSignals.Add(_signals[index]);
                }
            }
            _stopwatch.Restart();
        }

        private void UpdateSignals()
        {
            _signals.Clear();
            for (var frequency = StartFrequencyKhz; frequency <= EndFrequencyKhz; frequency += StepKhz)
            {
                var signal = new Signal(frequency, 1, Direction, Amplitude);
                _signals.Add(signal);
                Simulator?.UpdateSignalPhases(signal);
            }
            Simulator?.UpdateFhssSignal(this);
        }
    }
}
