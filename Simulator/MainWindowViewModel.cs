﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Simulator.Annotations;

namespace Simulator
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private string _connectButtonText;
        private string _formTitle;
        public string SimulatorHost { get; set; }
        public int SimulatorPort { get; set; }

        public string ConnectButtonText
        {
            get => _connectButtonText;
            set
            {
                _connectButtonText = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("ConnectButtonText"));
            }
        }

        public string FormTitle
        {
            get => _formTitle;
            set
            {
                _formTitle = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("FormTitle"));
            }
        }

        public SignalSimulator SignalSimulator => Simulator.SignalSimulator;

        public SimulatorServer Server => Simulator.Server;

        public SimulatorCore Simulator { get; }

        public MainWindowViewModel()
        {
            Simulator = new SimulatorCore();
            SimulatorHost = Simulator.SimulatorHost;
            SimulatorPort = Simulator.SimulatorPort;

            Server.Server.StartEvent += RaiseServerConnectionEvent;
            Server.Server.StopEvent += RaiseServerConnectionEvent;
        }

        private void RaiseServerConnectionEvent(object sender, EventArgs e)
        {
            if (Server.IsWorking)
            {
                ConnectButtonText = "Stop";
                FormTitle = "Working";
            }
            else
            {
                ConnectButtonText = "Start";
                FormTitle = "Stopped";
            }   
        }

        public void StartServer()
        {
            if (Server.Start(SimulatorHost, SimulatorPort))
            {
                RaiseServerConnectionEvent(this, EventArgs.Empty);
            }
        }

        public async Task StopServer()
        {
            await Server.Stop();
            RaiseServerConnectionEvent(this, EventArgs.Empty);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
