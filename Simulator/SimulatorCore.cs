﻿using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Hardware;
using SimulatorProtocols;

namespace Simulator
{
    public class SimulatorCore
    {
        public SignalSimulator SignalSimulator { get; }

        public SimulatorServer Server { get; }

        public string SimulatorHost { get; }
        public int SimulatorPort { get; }

        public SimulatorCore(bool saveSimulatedSignals = true)
        {
            SimulatorHost = Config.Instance.SimulatorSettings.SimulatorHost;
            SimulatorPort = Config.Instance.SimulatorSettings.SimulatorPort;
            SignalSimulator = new SignalSimulator(SimulatorDo.Load(), saveSimulatedSignals);
            Server = new SimulatorServer();
            
            Server.DataRequestReadEvent += OnDataRequestRead;
            Server.SignalGeneratorRequestEvent += OnSignalGeneratorRequest;
            Server.ReceiversChannelRequestEvent += OnSetReceiversChannelRequest;
            Server.UsrpDataRequestReadEvent += OnUsrpDataRequestRead;
        }

        private void OnSetReceiversChannelRequest(object sender, ReceiverChannel channel)
        {
            SignalSimulator.Channel = channel;
        }

        private void OnSignalGeneratorRequest(object sender, SetGeneratorSignalRequest e)
        {
            if (e.IsActive)
            {
                SignalSimulator.SignalGeneratorSignal = new Signal(e.Frequency / 10, 1, 0, e.Level)
                {
                    Simulator = SignalSimulator
                };
                SignalSimulator.UpdateSignalPhases(SignalSimulator.SignalGeneratorSignal);
            }
            else
            {
                SignalSimulator.SignalGeneratorSignal = null;
            }
        }

        private void OnDataRequestRead(object sender, DataRequest e)
        {
            var bandNumbers = e.BandNumbers.Select(b => b != 255 ? b : (byte) 0).ToArray();
            var response = SignalSimulator.GetResponse(bandNumbers);
            Server.Send(response.GetBytes());
        }

        private void OnUsrpDataRequestRead(object sender, UsrpDataRequest e)
        {
            var band = e.BandNumber != 255 ? e.BandNumber : (byte)0;
            var amplification = e.Amplification >= 1 && e.Amplification <= 70 ? e.Amplification : (byte)70;
            var response = SignalSimulator.GetUsrpResponse(band, amplification);
            Server.Send(response.GetBytes());
        }

        public async Task Stop()
        {
            await Server.Stop();
        }
    }
}
